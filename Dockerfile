FROM python:3.12

WORKDIR /usr/docker/waktisanat

COPY . .

RUN make rebuild_data

CMD ["make", "start"]