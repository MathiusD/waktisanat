# WakTisanat

![pipeline](https://gitlab.com/MathiusD/waktisanat/badges/master/pipeline.svg)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/846f225909124d00a961828f85296ce1)](https://www.codacy.com/manual/MathiusD/waktisanat?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=MathiusD/waktisanat&amp;utm_campaign=Badge_Grade)
[![Discord Bots](https://top.gg/api/widget/status/699940231343767552.svg)](https://top.gg/bot/699940231343767552)
[![Discord](https://img.shields.io/discord/722850007354703922?color=blue&label=discord&logo=discord)](https://discord.gg/9sjdyspDdf)
[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/S6S023QRN)

## FR

WakTisanat est un bot discord en python basé sur la Librairie [Discord Py](https://github.com/Rapptz/discord.py) et motorisé par mon ORM [Wakdata](https://gitlab.com/MathiusD/wakdata) pour exploiter les données du jeu dans les 4 langues de ce dernier (Fr, En, Es, Pt).
Ce bot permet de rechercher des objets du jeu et d'obtenir de nombreuses informations telles que les effets de ces derniers, le plan éventuellement nécessaire ou les composants.
Il permet également de faire des demandes de craft à travers l'interface (Cela peut donc permettre de les soumettre aux artisans enregistrés).
[Waktisanat](https://waktisanat.waklab.fr) est également un site web connecté au bot afin de profiter du bot sans être sur discord.

### Démarrage rapide

Enchanté cher Douzien,
Voici comment configurer rapidement WakTisanat, que je vais détailler en 2 points. En premier lieu pour l'utilisateur dit `lambda`, puis pour un serveur.

__Pour un utilisateur `lambda` :__

* Afin de faire en sorte que votre expérience vous soit la plus agréable possible, je vous conseille de vous enregistrer via la commande `~register`.
* Si vous avez un compte, je vous conseille de choisir votre langue par défaut à travers la commande `~langAccount fr\en\es\pt`.
* Si vous avez l'habitude d'utiliser votre propre préfixe ou tout simplement que vous trouvez `~` insupportable, vous pouvez changer celui-ci à travers la commande `~prefix MonPrefixIncroyable`.

* __Besoin de fabriquer ?__

  * Vous pouvez rechercher un objet à travers diverses commandes de recherches (`~help Objet` pour avoir plus de détails sur celles-ci). Ici, on va prendre la commande de base pour exemple `~search La Bonne Pioche` pour chercher la fiche de la Bonne Pioche. Sur celle-ci vous pouvez afficher ou masquer le craft via l'émote 🛠️, vous pouvez afficher ou masquer les statistiques via l'émote 🗡️, soumettre une demande de craft via l'émote 🔎, voir les artisans pouvant répondre à votre demande grâce à l'émote 📒 ou voir tous les composants et sous composants du craft avec l'émote 📃.
  * Cependant, pour effectuer une requête, il vous faut enregistrer un avatar afin de permettre au bot de savoir sur quel serveur vous souhaiter effectuer le craft. Il vous suffit de faire `~addAvatar "NomDeVotreAvatar" VotreServeur`.
  * Vous cherchez tous les artisans d'un serveur, c'est possible à l'aide de la commande `~directory Serveur`.

* __Vous êtes artisan ?__

  * Au même titre que les personnes souhaitant fabriquer des objets, il vous faut enregistrer un avatar si vous souhaitez pouvoir être visible pour d'autres joueurs ayant potentiellement besoin de vos services. Une fois fait, il serait de bon ton d'enregistrer vos niveaux de métier à travers la commande `~setJobLvl 145` puis de selectionner le métier à définir dans la liste que le bot vous présentera.
  * Vous n'êtes pas n'importe quel artisan et vous possédez des plans ? Pas de souci, vous pouvez également les enregistrer à travers la commande `~pattern "NomDeVotreAvatar" "Le Seum"` (ici vous enregistrez le fait que vous possédez le plan pour craft `Le Seum`).
  * Vous souhaitez voir vos avatars afin de montrer vos plans et métiers à vos guildeux ? Pas de souci, il vous suffit de faire `~userAvatars` et le tour est joué.
  * Vous souhaitez que le bot vous prévienne en cas de demandes auxquelles vous pouvez répondre ? Vous pouvez modifier vos paramètres de notification afin de recevoir soit toutes les demandes (`~notify toutes`), les demandes qui nécessitent un plan spécifique (`~notify recettes-seulement`) ou aucune (`~notify aucune`) (c'est le niveau de notification par défaut). Peu importe votre niveau de notification, vous pourrez voir à tout moment à quelles demandes vous pouvez répondre à travers `~requestsAvailable`.

* __Vous souhaitez préparer votre nouvel équipement ?__

  * Vous pouvez voir le coût en composants de votre équipement à travers la commande `~getComponentsOfBuild`.
  * Vous pouvez a contrario comparer la différence de composants entre deux équipements à l'aide de la commande `~compareComponentsOfBuilds`.

__Pour un serveur :__

* Attention, les commandes suivantes nécessitent que vous aillez les permissions Administrateur sur le serveur (les mêmes permissions qui vous ont permis d'inviter Waktisanat à vrai dire).
* Vous souhaitez que le bot parle dans une langue définie sur votre serveur, il vous suffit de faire `~lang fr/en/es/pt`.
* Vous voulez définir un préfixe pour votre serveur car `~` est toujours aussi peu pratique. Vous pouvez le faire à travers `~prefix "NouveauPréfix"`.
* Avoir les notifications du bot est également possible. Pour cela il vous suffit de saisir la commande `~notifyChannel #VotreSalonDeNotif` et ainsi le bot vous préviendra des modifications apportées ou s'il redémarre car les données d'Ankama ont été mises à jour.

__Questions, remarques, problèmes ?__

* Vous avez un bug à signaler ? Dans ce cas, vous pouvez le remonter à l'aide de la commande `~addReport`.
* Une suggestion ? Vous pouvez la transmettre à travers `~addSuggest`.
* Une question sur le fonctionnement du bot ? Nous serons ravis de répondre à vos question sur le serveur de support (`~supportGuild` vous donnera le lien).

## EN

WakTisanat is a discord bot in python based on the [Discord Py library](https://github.com/Rapptz/discord.py) and powered by my ORM [Wakdata](https://gitlab.com/MathiusD/wakdata) to exploit the game data in the 4 languages of the latter (Fr, En, Es, Pt).
This bot allows you to search for objects in the game and get a lot of information such as the effects of the latter, the necessary plan or the components.
It also allows you to make craft requests through the interface (this can therefore allow you to submit them to registered craftsmen).
[Waktisanat](https://waktisanat.waklab.fr) is also a website connected to the bot in order to enjoy the bot without being in disagreement.

### QuickStart

Delighted dear Douzien,
Here is how to quickly configure WakTisanat, which I will detail in 2 points. First for the user called `lambda`, then for a server.

__For a `lambda` user :__

* In order to make your experience as favorable as possible, I advise you to register via the `~register` command.
* If you have an account I advise you to choose your default language through the `~langAccount fr\en\es\pt` command.
* If you are used to using your own prefix or simply find `~`unportable you can change it with the `~prefix MyUnbelievablePrefix` command.

* __Need to Craft ?__

  * You can search for an object through various search commands (`~help Object` for more details on these). Here we'll take the basic command for example `~search the Good Pickaxe's` for the Good Pickaxe's file. On it you can show or hide craft of item with emote 🛠️, you can show or hide stats of item with emote 🗡️, you can submit a craft request through the emote 🔎, see the craftsmen who can answer your request through the emote 📒 or see all the components and sub-components of the craft with the emote 📃.
  * However, to make a request, you need to register an avatar so that the bot knows on which server you want to make the craft. Just do `~addAvatar "NameOfYourAvatar" "YourServer"`.
  * If you are looking for all the craftsmen of a server, you can do it with the command `~directory Server`.

* __You're a craftsman?__

  * Just like people who want to make items, you need to register an avatar if you want to be visible to other players who may need your services. Once done, it would be a good idea to register your job levels with the command `~setJobLvl 145` and then select the job to define from the list the bot will present you with.
  * You are not just any craftsman and you have blueprints, no worries you can also register them through the command `~pattern "NameOfYourAvatar" "The Seum"` (Here you register the fact that you own the blueprint for crafting `The Seum`).
  * If you want to see your avatars to show your blueprints and trades to a guild no worries, just do `~userAvatars` and that's it.
  * Do you want the bot to warn you in case of a request you can answer? You can change your notification settings to receive either all requests (`~notify all`), requests that require a specific plan (`~notify recipes-only`) or none (`~notify none`) (This is the default notification level). No matter what level of notification you have, you will be able to see at any time which requests you can respond to through `~requestsAvailable`.

* __Do you want to prepare your new equipment ?__

  * You can see the component cost of your equipment through the `~getComponentsOfBuild` command.
  * You can compare the difference in components between two units with the `~compareComponentsOfBuilds` command.

__For a Server :__

* Be careful the following commands require that you have Administrator permissions on the server (the same permissions that allowed you to invite Waktisanat actually).
* You want the bot to speak in a language defined on your server, just do `~lang fr/en/es/pt`.
* You want to set a prefix for your server because `~` is always inconvenient. You can do it through `~prefix "New Prefix"`.
* It is also possible to get notifications from the bot for this purpose. Simply enter the command `~notifyChannel #YourNotificationLounge` and the bot will notify you of any changes made or if it restarts because Ankama's data has been updated.

__Questions, Comments, Problems?__

* You have a bug to report, in this case you can report it with the command `~addReport`
* A suggestion, you can transmit it through `~addSuggest`
* A question about how the bot works? We will be happy to answer your questions on the support server (`~supportGuild` will give you the link).

## ES

WakTisanat es un bot de discordia en pitón basado en la [biblioteca de Discordia Py](https://github.com/Rapptz/discord.py) y potenciado por mi ORM [Wakdata](https://gitlab.com/MathiusD/wakdata) para explotar los datos del juego en los 4 idiomas de este último (Fr, En, Es, Pt).
Este robot permite buscar objetos en el juego y obtener mucha información como los efectos de estos últimos, el plan necesario o los componentes.
También le permite hacer solicitudes de artesanía a través de la interfaz (por lo tanto, esto puede permitirle presentarlas a los artesanos registrados).
[Waktisanat](https://waktisanat.waklab.fr) es también un sitio web conectado al bot para poder disfrutar del bot sin estar en desacuerdo.

## PT

WakTisanat é uma discórdia Bot no pitão baseado no [Discórdia biblioteca de Py](https://github.com/Rapptz/discord.py) e acionado pelo meu ORM [Wakdata](https://gitlab.com/MathiusD/wakdata) para explorar os dados de jogo nas 4 línguas do último (Fr, En, Es, Pt).
Este Bot permite-lhe procurar objetos no jogo e adquirir muita informação como os efeitos do último, o plano necessário ou os componentes.
Também lhe permite fazer pedidos de ofício pela interface (isto, por isso, pode permitir-lhe submetê-los a artífices registrados).
[Waktisanat](https://waktisanat.waklab.fr) também é um site web unido a Bot para gostar de Bot sem estar na discordância.

## Auteur

Féry Mathieu (aka Mathius)
