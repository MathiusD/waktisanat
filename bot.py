import time
import logging
import cogs
import discord
import datetime
import json
import asyncio
import cdnwakfu
import subprocess
import traceback
import os
from own import own_sqlite, own_discord
from discord.ext import commands
from models import Guild, User, Avatar, Request, Issue, ItemPrice
from config import settings
from utils import Commit, Lang, Help, sendAll
from data.wakdata import latest
from cdnwakfu import import_data
import utils
import constants

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s',
                    filename='logs/%s.log' % (str(time.time())), level=settings.DISCORD_LOG)

logging.info("Verification Database")
if (own_sqlite.bdd_exist(settings.DISCORD_DB) != True):
    logging.info("Creating Database")
    own_sqlite.bdd_create(settings.DISCORD_DB)

logging.info("Connecting Database")
bdd = own_sqlite.connect(settings.DISCORD_DB)

guilds = Guild(bdd)
users = User(bdd)
avatars = Avatar(bdd, users)
requests = Request(bdd, avatars)
issues = Issue(bdd, users)
prices = ItemPrice(bdd, users)
datetime_fields = ("year", "month", "day", "hour", "minute", "second")


def determine_prefix(bot: commands.AutoShardedBot, message: discord.Message):
    prefix = users.fetchPrefixOfUser(message.author.id)
    if message.guild and not prefix:
        prefix = guilds.fetchPrefixOfServer(message.guild.id)
    return commands.when_mentioned_or(settings.DISCORD_PREFIX if not prefix else prefix)(bot, message)


class MyBot(commands.AutoShardedBot):
    async def setup_hook(self):
        await self.add_cog(cogs.ItemCogs(guilds, requests, prices))
        await self.add_cog(cogs.RequestCogs(guilds, requests, prices))
        await self.add_cog(cogs.BuilderCogs(guilds, requests, prices))
        await self.add_cog(cogs.AvatarCogs(guilds, avatars))
        await self.add_cog(cogs.UserCogs(users, guilds))
        await self.add_cog(cogs.GuildCogs(users, guilds))
        await self.add_cog(cogs.IssueCogs(users, guilds, issues))
        await self.add_cog(cogs.AdminCogs(users, guilds))

        def commandStartBy(bot: commands.AutoShardedBot, name: str):
            name = name.lower().strip()
            cmds = []
            if len(name) >= 3:
                for cmd in bot.commands:
                    if cmd.hidden is False:
                        cmdName = cmd.name.lower().strip()
                        if cmdName.startswith(name):
                            if cmdName == name:
                                return []
                            cmds.append(cmd.name)
                        for aliase in cmd.aliases:
                            aliaseName = aliase.lower().strip()
                            if aliaseName.startswith(name):
                                if aliaseName == name:
                                    return []
                                cmds.append(aliase)
            return cmds

        async def almost(msg: discord.Message):
            for prefix in determine_prefix(bot, msg):
                if msg.content.startswith(prefix):
                    cmd = msg.content[len(prefix):].split(" ")[0]
                    cmds = commandStartBy(bot, cmd)
                    if len(cmds) > 0:
                        ms = cdnwakfu.Text({
                            "fr": "Vous vouliez peut-être dire...\n",
                            "en": "Maybe you meant...\n",
                            "es": "Tal vez quisiste decir...\n",
                            "pt": "Talvez quis dizer...\n"
                        })
                        ctx = await bot.get_context(msg)
                        ms = "%s```" % ms.getByShortLang(
                            utils.Lang.getLang(ctx, users, guilds, False))
                        for cmd in cmds:
                            ms = "%s\t%s\n" % (ms, cmd)
                        ms = "%s```" % ms
                        return await own_discord.reponse_send(ctx, ms)
            return None

        self.add_listener(almost, 'on_message')

        dblpy = None
        if settings.DBL_TOKEN is not None:
            import dbl
            dblpy = dbl.DBLClient(self, settings.DBL_TOKEN)

        async def waitingStart(bot: commands.AutoShardedBot):
            while bot.is_ready() == False:
                await asyncio.sleep(5)

        async def checkVersion(bot: commands.AutoShardedBot, loop: bool = True, latest: dict = latest):
            while loop:
                version = cdnwakfu.get_version()
                if not latest or (version != None and latest["version"] != version):
                    print("Data Out of Date")
                    logging.info("Data Out of Date")

                    async def restart(ctx: commands.Context, lang: str):
                        msg = cdnwakfu.Text({
                            "fr": "Redémarrage du bot pour mettre à jour les Données (%s à %s)" % (latest["version"] if latest else "Données Manquantes", version),
                            "en": "Bot Restart for upgrade data version (%s to %s)" % (latest["version"] if latest else "Missing Data", version),
                            "es": "Bot Reinicio para la actualización de la versión de datos (%s a %s)" % (latest["version"] if latest else "Ausencia de datos", version),
                            "pt": "Reinício de Bot de versão de dados de upgrade (%s para %s)" % (latest["version"] if latest else "Ausência a dados", version)
                        })
                        await ctx.send(msg.getByShortLang(lang))
                    await waitingStart(bot)
                    await sendAll(bot, guilds, restart)
                    await bot.logout()
                    import_data()
                    print("Fetch and Rebuild")
                    await bot.close()
                    subprocess.run(["make", "start"])
                else:
                    await asyncio.sleep(settings.CHECK_TIME)

        async def checkIssues(bot: commands.AutoShardedBot, loop: bool = True):
            while loop:
                now = datetime.datetime.utcnow()
                time = {}
                regen_time = False
                if os.path.exists(settings.FILE_ISSUE):
                    try:
                        with open(settings.FILE_ISSUE) as file:
                            time = json.load(file)
                            regen_time = False
                    except json.JSONDecodeError:
                        regen_time = True
                else:
                    regen_time = True
                if regen_time:
                    for key in datetime_fields:
                        time[key] = getattr(now, key)
                modelIssues = issues.fetchAll()
                fetchIssues = utils.Issue.issuesAreUpdatedAfter(
                    modelIssues, time)
                now = datetime.datetime.utcnow()
                await waitingStart(bot)
                for issue in fetchIssues:
                    await utils.Issue.sendIssueUpdate(bot, issue, issues)
                for key in time.keys():
                    time[key] = getattr(now, key)
                with open(settings.FILE_ISSUE, "w") as file:
                    json.dump(time, file)
                await asyncio.sleep(settings.CHECK_TIME)

        async def checkRequest(bot: commands.AutoShardedBot, loop: bool = True):
            while loop:
                newIds = {
                    "craft": 0,
                    "component": 0,
                    "buy": 0
                }
                if os.path.exists(settings.REQUESTS_FILE):
                    try:
                        with open(settings.REQUESTS_FILE) as file:
                            ids = json.load(file)
                            await waitingStart(bot)
                            for req in requests.buyRequests.fetchAfterId(ids["buy"]):
                                await utils.requests.BuyRequest.sendPingsForRequest(bot, req, requests, prices)
                                ids["buy"] = req.id
                            for req in requests.componentRequests.fetchAfterId(ids["component"]):
                                await utils.requests.ComponentRequest.sendPingsForRequest(bot, req, requests, prices)
                                ids["component"] = req.id
                            for req in requests.craftRequests.fetchAfterId(ids["craft"]):
                                await utils.requests.CraftRequest.sendPingsForRequest(bot, req, requests, prices)
                                ids["craft"] = req.id
                            newIds = ids
                    except json.JSONDecodeError:
                        None
                with open(settings.REQUESTS_FILE, "w") as file:
                    json.dump(newIds, file)
                await asyncio.sleep(settings.CHECK_TIME)

        async def SendDevlog(bot: commands.AutoShardedBot):
            logging.info("Devlog process...")
            now = datetime.datetime.utcnow()
            time = {}
            regen_time = False
            if os.path.exists(settings.FILE_LAST_LAUNCH):
                try:
                    with open(settings.FILE_LAST_LAUNCH) as file:
                        time = json.load(file)
                        regen_time = False
                except json.JSONDecodeError:
                    regen_time = True
            else:
                regen_time = True
            if regen_time:
                for key in datetime_fields:
                    time[key] = getattr(now, key)
            commits = Commit.FetchCommit([{"key": "since", "value": datetime.datetime(
                time["year"], time["month"], time["day"], time["hour"], time["minute"], time["second"])}])
            if len(commits) > 0:
                await waitingStart(bot)

                async def devlog(ctx: commands.Context, lang: str):
                    await Commit.Devlog(ctx, commits, lang)
                await sendAll(bot, guilds, devlog)
                logging.info("Devlog are sending !")
            with open(settings.FILE_LAST_LAUNCH, "w") as file:
                for key in time.keys():
                    time[key] = getattr(now, key)
                json.dump(time, file)

        async def postGuild():
            if dblpy:
                await dblpy.post_guild_count()

        @bot.event
        async def on_guild_join(guild: discord.Guild):
            await postGuild()

        @bot.event
        async def on_guild_remove(guild: discord.Guild):
            await postGuild()

        @bot.event
        async def on_ready():
            act = discord.Game(settings.DISCORD_HELP)
            await bot.change_presence(activity=act)
            await postGuild()
            logging.info("Bot Launch.")
            print("Bot Up !")

        @bot.command(aliases=["aide", "ayudar", "ajudar"])
        async def help(ctx: commands.Context, arg: str = None):
            lang = Lang.getLang(ctx, users, guilds)
            await ctx.typing()
            msg = None
            if arg:
                cog = Help.extractCog(bot, arg, lang)
                cmd = Help.extractCmd(bot, arg)
                if cog:
                    msg = Help.createCog(cog, lang, user_prefix=ctx.prefix)
                elif cmd and cmd.hidden is False:
                    msg = Help.showCmd(cmd, lang, user_prefix=ctx.prefix)
                elif arg in cdnwakfu.Text.shortLangsAccepted():
                    msg = Help.globalHelp(ctx.bot, arg, user_prefix=ctx.prefix)
                else:
                    msg = None
            else:
                msg = Help.globalHelp(ctx.bot, lang, user_prefix=ctx.prefix)
            if msg:
                await own_discord.embed_send(ctx, own_discord.embed_create(msg))
            else:
                await own_discord.reponse_send(ctx, constants.not_found.getByShortLang(lang))

        @bot.command(aliases=["démarrageRapide", "comienzoRápido", "partidaRápida"])
        async def quickStart(ctx: commands.Context, lang: str = None):
            lang = Lang.checkLang(lang) if lang else Lang.getLang(
                ctx, users, guilds)
            lang = settings.DISCORD_LANG if lang is None else lang
            await ctx.typing()
            msg = constants.quickstart.getByShortLang(lang) if constants.quickstart.getByShortLang(
                lang) != "TODO" else constants.quickstart.getByShortLang(settings.DISCORD_LANG)
            await own_discord.embed_send(ctx, own_discord.embed_create(msg, constants.quickstart_title.getByShortLang(lang)), True)

        @bot.command(aliases=[])
        async def version(ctx: commands.Context):
            await ctx.send(latest["version"])

        if dblpy:
            @bot.command(aliases=["supportServeur", "gremioApoyo", "corporaçãoSuporte"])
            async def supportGuild(ctx: commands.Context):
                lang = Lang.getLang(ctx, users, guilds)
                await ctx.typing()
                data = await dblpy.get_bot_info()
                await ctx.send("https://discord.gg/%s" % data["support"] if "support" in data.keys() else constants.no_renseigned.getByShortLang(lang))

            @bot.command()
            async def invite(ctx: commands.Context):
                lang = Lang.getLang(ctx, users, guilds)
                await ctx.typing()
                data = await dblpy.get_bot_info()
                await ctx.send(data["invite"] if "invite" in data.keys() else constants.no_renseigned.getByShortLang(lang))

        async def commandError(ctx: commands.Context, exc: commands.CommandError):
            log = True
            if ctx.command:
                if isinstance(exc, commands.UserInputError):
                    log = False
                    await own_discord.reponse_send(ctx, constants.input_error.getByShortLang(Lang.getLang(ctx, users, guilds)))
                elif isinstance(exc, commands.MissingPermissions):
                    log = False
                    await own_discord.reponse_send(ctx, constants.missing_permissions.getByShortLang(Lang.getLang(ctx, users, guilds)))
                elif isinstance(exc, commands.NoPrivateMessage):
                    log = False
                    await own_discord.reponse_send(ctx, str(exc))
                else:
                    await own_discord.reponse_send(ctx, constants.unknown_error(ctx.prefix).getByShortLang(Lang.getLang(ctx, users, guilds)))
                if ctx.command.hidden is False:
                    if log is False:
                        await help(ctx, ctx.command.name)
            if log:
                if not isinstance(exc, commands.CommandNotFound):
                    logs = traceback.format_exception(
                        type(exc), exc, exc.__traceback__)
                    location = "For User %s#%s(%i)" % (
                        ctx.author.name, ctx.author.discriminator, ctx.author.id)
                    if ctx.command:
                        location = "In use %s %s" % (
                            ctx.command.name, location)
                    if ctx.guild:
                        location = "%s In Guild \"%s\"(%i)" % (
                            location, ctx.guild.name, ctx.guild.id)
                    logs.append(location)
                    logs.append("Call : %s" % ctx.message.content)
                    for line in logs:
                        print(line)
                        logging.warning(line)

        if settings.DISCORD_LOG == "DEBUG":
            async def checkTasks(bot: commands.AutoShardedBot):
                while True:
                    tasks = asyncio.all_tasks(bot.loop)
                    tasksFormat = ""
                    for task in tasks:
                        tasksFormat = "%s\t%s\n" % (tasksFormat, task)
                    logging.debug("%i tasks are launched :\n%s",
                                  len(tasks), tasksFormat)
                    await asyncio.sleep(10)
            asyncio.get_event_loop().create_task(checkTasks(self))

        self.on_command_error = commandError

        asyncio.get_event_loop().create_task(checkVersion(self))
        asyncio.get_event_loop().create_task(checkIssues(self))
        asyncio.get_event_loop().create_task(checkRequest(self))
        asyncio.get_event_loop().create_task(SendDevlog(self))


bot = MyBot(
    command_prefix=determine_prefix, help_command=None, case_insensitive=True, intents=discord.Intents.default())


bot.run(settings.DISCORD_TOKEN)
