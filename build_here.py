from own_install import install
from config import settings
import os, json, datetime

install(settings.GITLAB_DEPENDANCIES, settings.GITLAB_MODULE, settings.GITLAB_AUTHOR, settings.GITLAB_PROVIDER)
now = datetime.datetime.utcnow()
date = {
    "year":now.year,
    "month":now.month,
    "day":now.day,
    "hour":now.hour,
    "minute":now.minute,
    "second":now.second
}
if os.path.exists(settings.FILE_LAST_LAUNCH) == False:
    json.dump(date, open(settings.FILE_LAST_LAUNCH, "w"))
if os.path.exists(settings.FILE_ISSUE) == False:
    json.dump(date, open(settings.FILE_ISSUE, "w"))
if os.path.exists(settings.REQUESTS_FILE) == False:
    data = {
        "craft":0,
        "component":0,
        "buy":0
    }
    json.dump(data, open(settings.REQUESTS_FILE, "w"))