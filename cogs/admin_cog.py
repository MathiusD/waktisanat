from discord.ext import commands
from cogs import CogsDBBase
from cdnwakfu import Text
import utils
from own import own_discord
import constants


class AdminCogs(CogsDBBase):

    own_name = Text({
        "fr": "Admin",
        "en": "Admin",
        "es": "Admin",
        "pt": "Admin"
    })

    @commands.is_owner()
    @commands.dm_only()
    @commands.command(hidden=True)
    async def sendAll(self, ctx: commands.Context):
        lang = await super().init_command(ctx)
        data = {}
        canceled = False
        for lng in Text.shortLangs():
            if canceled is False:
                dit = {
                    "cancel": constants.cancel.getByShortLang(lang)
                }
                desc = Text({
                    "fr": "Veuillez saisir le message pour la communauté (%s) :" % lng,
                    "en": "Please enter the message for the community (%s):" % lng,
                    "es": "Por favor, introduzca el mensaje para la comunidad (%s):" % lng,
                    "pt": "Por favor entre na mensagem da comunidade (%s):" % lng
                })
                await own_discord.reponse_send(ctx, desc.getByShortLang(lang))
                dat = await own_discord.wait_input(ctx, own_discord.str_check, dit, 300.0)
                await ctx.typing()
                if not dat or dat == -1:
                    await own_discord.reponse_send(ctx, constants.canceled.getByShortLang(lang))
                    canceled = True
                else:
                    data[lng] = dat
        if canceled is False:
            msg = Text(data)

            async def send_msg(ctx: commands.Context, lang: str):
                await own_discord.reponse_send(ctx, msg.getByShortLang(lang))
            await utils.sendAll(ctx.bot, self.guilds, send_msg)
            await own_discord.reponse_send(ctx, constants.send.getByShortLang(lang))

    @commands.is_owner()
    @commands.dm_only()
    @commands.command(hidden=True)
    async def sendAllWithoutLang(self, ctx: commands.Context):
        lang = await super().init_command(ctx)
        data = {}
        canceled = False
        dit = {
            "cancel": constants.cancel.getByShortLang(lang)
        }
        desc = Text({
            "fr": "Veuillez saisir le message pour la communauté :",
            "en": "Please enter the message for the community :",
            "es": "Por favor, introduzca el mensaje para la comunidad :",
            "pt": "Por favor entre na mensagem da comunidade :"
        })
        await own_discord.reponse_send(ctx, desc.getByShortLang(lang))
        dat = await own_discord.wait_input(ctx, own_discord.str_check, dit, 300.0)
        await ctx.typing()
        if not dat or dat == -1:
            await own_discord.reponse_send(ctx, constants.canceled.getByShortLang(lang))
            canceled = True
        else:
            for lng in Text.shortLangs():
                data[lng] = dat
        if canceled is False:
            msg = Text(data)

            async def send_msg(ctx: commands.Context, lang: str):
                await own_discord.reponse_send(ctx, msg.getByShortLang(lang))
            await utils.sendAll(ctx.bot, self.guilds, send_msg)
            await own_discord.reponse_send(ctx, constants.send.getByShortLang(lang))

    @commands.is_owner()
    @commands.dm_only()
    @commands.command(hidden=True)
    async def stats(self, ctx: commands.Context):
        lang = await super().init_command(ctx)
        msg = "Latency : %f\n%i Guilds :\n" % (
            ctx.bot.latency, len(ctx.bot.guilds))
        config = 0
        lang = {
            "en": 0,
            "fr": 0,
            "es": 0,
            "sp": 0,
            "pt": 0
        }
        prefix = 0
        notify = 0
        for guild in ctx.bot.guilds:
            g = self.guilds.fetchGuildByDiscordId(guild.id)
            if g:
                config += 1
                if g.lang:
                    lang[g.lang] += 1
                if g.prefix:
                    prefix += 1
                if g.notify_chan:
                    notify += 1
        if config > 0:
            msg = "%s\t%i Guild with Configuration\n" % (msg, config)
            for l in Text.shortLangsAccepted():
                if lang[l] > 0:
                    msg = "%s\t%i Guild with Lang %s\n" % (msg, lang[l], l)
            if prefix > 0:
                msg = "%s\t%i Guild with Custom Prefix\n" % (msg, prefix)
            if notify > 0:
                msg = "%s\t%i Guild with Notification\n" % (msg, notify)
        users = self.users.model.fetchAll(self.users.db)
        if len(users) > 0:
            msg = "%s%i Users :\n" % (msg, len(users))
            lang = {
                "en": 0,
                "fr": 0,
                "es": 0,
                "sp": 0,
                "pt": 0
            }
            prefix = 0
            for user in users:
                if user.lang:
                    lang[user.lang] += 1
                if user.prefix:
                    prefix += 1
            for l in Text.shortLangsAccepted():
                if lang[l] > 0:
                    msg = "%s\t%i User with Lang %s\n" % (msg, lang[l], l)
            if prefix > 0:
                msg = "%s\t%i User with Custom Prefix\n" % (msg, prefix)
        embd = own_discord.embed_create(msg, "Stats")
        await own_discord.embed_send(ctx, embd)

    @commands.is_owner()
    @commands.dm_only()
    @commands.command(hidden=True)
    async def advancedStats(self, ctx: commands.Context):
        lang = await super().init_command(ctx)
        msg = "Latency : %f\nGuilds :\n" % ctx.bot.latency
        for guild in ctx.bot.guilds:
            g = self.guilds.fetchGuildByDiscordId(guild.id)
            line = "\t`%s`" % (guild.name)
            if g:
                if g.lang:
                    line = "%s (Lang : `%s`)" % (line, g.lang)
                if g.prefix:
                    line = "%s (Prefix : `%s`)" % (line, g.prefix)
                if g.notify_chan:
                    channel = guild.get_channel(g.notify_chan)
                    if channel:
                        line = "%s (With NotifyChan [In `%s`])" % (
                            line, channel.name)
            line = "%s [Id:%i]" % (line, guild.id)
            msg = "%s%s\n" % (msg, line)
        msg = "%sUsers :\n" % msg
        for user in self.users.model.fetchAll(self.users.db):
            line = "\t<@%i>" % (user.discord_id)
            if user.lang:
                line = "%s (Lang : `%s`)" % (line, user.lang)
            if user.prefix:
                line = "%s (Prefix : `%s`)" % (line, user.prefix)
            msg = "%s%s\n" % (msg, line)
        embd = own_discord.embed_create(msg, "Stats")
        await own_discord.embed_send(ctx, embd)

    @commands.is_owner()
    @commands.dm_only()
    @commands.command(hidden=True)
    async def debugGuild(self, ctx: commands.Context):
        lang = await super().init_command(ctx)
        await utils.Guild.display(ctx, self.guilds.model.fetchAll(self.guilds.db), lang)
