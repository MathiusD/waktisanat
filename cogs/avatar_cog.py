from discord.ext import commands
from cogs import CogsDBAvatar
from own import own_discord
import constants
from data import wakdata
from cdnwakfu import Recipe, Text, RecipePattern
import utils


class AvatarCogs(CogsDBAvatar):

    own_name = Text({
        "fr": "Avatar",
        "en": "Avatar",
        "es": "Avatar",
        "pt": "Avatar"
    })

    @commands.command(aliases=["ajouterAvatar", "añadaAvatar", "acrescenteAvatar"])
    async def addAvatar(self, ctx: commands.Context, name: str, server: str = None, lvl: int = None):
        lang = await super().init_command(ctx)
        if await utils.Account.haveAccount(ctx, self.users, lang):
            if not server:
                server = await utils.Server.select(ctx, lang)
            server = await utils.Server.checkServer(ctx, server, lang)
            if server != None:
                if lvl:
                    lvl = utils.Lvl.checkAvatarLvl(lvl)
                    if not lvl:
                        await own_discord.reponse_send(ctx, constants.lvl_avatar_incorrect_creating.getByShortLang(lang))
                if self.avatars.addAvatar(ctx.author.id, name, utils.Server.indexOfServerElement(server), lvl):
                    await own_discord.reponse_send(ctx, constants.avatar_register.getByShortLang(lang))
                else:
                    await own_discord.reponse_send(ctx, constants.avatar_not_available.getByShortLang(lang))

    @commands.command(aliases=["renommerAvatar", "renombrarElAvatar", "renomearOAvatar"])
    async def renameAvatar(self, ctx: commands.Context, *, name: str = None):
        lang = await super().init_command(ctx)
        argDict = self.extractArg(name)
        name = argDict["arg"]
        lang = argDict["lang"] if argDict["lang"] != None else lang
        avatarSelected = None
        if not name:
            data = self.avatars.fetchAvatarsByDiscordId(ctx.author.id)
            if data:
                avatarSelected = await utils.Avatar.select(ctx, data, lang, self.avatars)
            else:
                await own_discord.reponse_send(ctx, constants.no_avatar.getByShortLang(lang))
        else:
            dat = self.users.fetchUserByName(name)
            if dat:
                data = self.avatars.fetchAvatarsByDiscordId(dat.discord_id)
                if data:
                    avatarSelected = await utils.Avatar.select(ctx, data, lang, self.avatars)
                else:
                    await own_discord.reponse_send(ctx, constants.no_avatar.getByShortLang(lang))
            else:
                await own_discord.reponse_send(ctx, constants.user_not_found.getByShortLang(lang))
        if avatarSelected is not None:
            dit = {
                "cancel": constants.cancel.getByShortLang(lang)
            }
            newName = Text({
                "fr": "Entrez le Nouveau Nom ci-dessous :",
                "en": "Enter the New Name below :",
                "es": "Introduce el nuevo nombre a continuación :",
                "pt": "Insira o novo nome abaixo :"
            })
            await own_discord.reponse_send(ctx, newName.getByShortLang(lang))
            newName = await own_discord.wait_input(ctx, own_discord.str_check, dit, 120.0)
            await ctx.typing()
            if not newName or newName == -1 or newName == avatarSelected.name:
                await own_discord.reponse_send(ctx, constants.canceled.getByShortLang(lang))
            else:
                if self.avatars.renameAvatar(avatarSelected.id, newName) is not None:
                    await own_discord.reponse_send(ctx, constants.avatar_register.getByShortLang(lang))
                else:
                    await own_discord.reponse_send(ctx, constants.avatar_not_available.getByShortLang(lang))

    @commands.command(aliases=["avatarUtilisateur", "avataresUsuario", "avataresUsuário"])
    async def userAvatars(self, ctx: commands.Context, *, name: str = None):
        lang = await super().init_command(ctx)
        argDict = self.extractArg(name)
        name = argDict["arg"]
        lang = argDict["lang"] if argDict["lang"] != None else lang
        if not name:
            data = self.avatars.fetchAvatarsByDiscordId(ctx.author.id)
            if data:
                await utils.Avatar.display(ctx, data, lang, self.avatars)
            else:
                await own_discord.reponse_send(ctx, constants.no_avatar.getByShortLang(lang))
        else:
            dat = self.users.fetchUserByName(name)
            if dat:
                data = self.avatars.fetchAvatarsByDiscordId(dat.discord_id)
                if data:
                    await utils.Avatar.display(ctx, data, lang, self.avatars)
                else:
                    await own_discord.reponse_send(ctx, constants.no_avatar.getByShortLang(lang))
            else:
                await own_discord.reponse_send(ctx, constants.user_not_found.getByShortLang(lang))

    @commands.command(aliases=["rechercherAvatar", "busqueAvatares", "procureAvatares"])
    async def searchAvatars(self, ctx: commands.Context, *, name: str):
        lang = await super().init_command(ctx)
        argDict = self.extractArg(name)
        name = argDict["arg"]
        lang = argDict["lang"] if argDict["lang"] != None else lang
        data = self.avatars.fetchAvatarsByName(name)
        await utils.Avatar.display(ctx, data, lang, self.avatars)

    @commands.command(aliases=["avatarPouvantFabriquer", "avatarCapazDeHacer", "avatarCapazDeCriação"])
    async def avatarCanCraft(self, ctx: commands.Context, *, name: str):
        lang = await super().init_command(ctx)
        argDict = self.extractArg(name)
        name = argDict["arg"]
        lang = argDict["lang"] if argDict["lang"] != None else lang
        items = await utils.ItemInteract.getItems(name, lang)
        item = await utils.ItemInteract.select(ctx, items, lang)
        if item:
            recipes = await Recipe.asyncFindRecipeByItemResult(wakdata.latest, item)
            recipe = await utils.Recipe.select(ctx, recipes, lang)
            if recipe:
                await utils.Avatar.showAvatarCanCraftRecipe(ctx, recipe, self.avatars, lang)

    @commands.command(aliases=["favori", "favorito"])
    async def favorite(self, ctx: commands.Context, *, name: str = None):
        lang = await super().init_command(ctx)
        argDict = self.extractArg(name)
        name = argDict["arg"]
        lang = argDict["lang"] if argDict["lang"] != None else lang
        if not name:
            favs = self.avatars.fetchFavsofAvatar(ctx.author.id)
            avts = []
            for fav in favs:
                avt = self.avatars.fetchAvatarsById(fav.favTarget)
                if avt:
                    avts.append(avt)
            if len(avts) > 0:
                await utils.Avatar.display(ctx, avts, lang, self.avatars)
            else:
                await own_discord.reponse_send(ctx, constants.no_favs.getByShortLang(lang))
        else:
            avts = self.avatars.fetchAvatarsByName(name)
            avt = await utils.Avatar.select(ctx, avts, lang, self.avatars)
            if avt:
                if self.avatars.addFav(ctx.author.id, avt.id) is not None:
                    await own_discord.reponse_send(ctx, " %s (%s)" % (constants.fav_add.getByShortLang(lang), avt.name))
                else:
                    dit = {
                        "cancel": constants.cancel.getByShortLang(lang)
                    }
                    await own_discord.reponse_send(ctx, constants.fav_suppress_request.getByShortLang(lang))
                    response = await own_discord.wait_input(ctx, own_discord.str_check, dit, 120.0)
                    if response and response != -1:
                        if response == constants.confirm.getByShortLang(lang):
                            self.avatars.removeFav(ctx.author.id, avt.id)
                            await own_discord.reponse_send(ctx, constants.fav_suppress.getByShortLang(lang))
                        else:
                            await own_discord.reponse_send(ctx, constants.bad_argument.getByShortLang(lang))
                    else:
                        await own_discord.reponse_send(ctx, constants.canceled.getByShortLang(lang))

    @commands.command(aliases=["annuaire", "directorio", "diretório"])
    async def directory(self, ctx: commands.Context, server: str = None, *, job: str = None):
        await self.directoryWithLevel(ctx, server, job)

    @commands.command()
    async def shortDirectory(self, ctx: commands.Context, server: str = None, *, job: str = None):
        await self.directoryWithLevel(ctx, server, job, -1)

    async def directoryWithLevel(self, ctx: commands.Context, server: str = None, job: str = None, level: int = None):
        lang = await super().init_command(ctx)
        if not server:
            server = await utils.Server.select(ctx, lang)
        server = await utils.Server.checkServer(ctx, server, lang)
        if server != None:
            data = []
            found = True
            if job:
                raw_job = utils.Job.checkJob(job, lang)
                if len(raw_job) == 1:
                    job = raw_job[0].id
                    if level is None:
                        bypass = Text({
                            "fr": "Aucune",
                            "en": "None",
                            "es": "Nada",
                            "pt": "Nada"
                        })
                        error_msg = Text({
                            "fr": "Entrer un nombre (`%s` pour ne pas en spécifier)" % bypass.fr,
                            "en": "Please enter number (`%s` for not specify)" % bypass.en,
                            "es": "Por favor entre en el número (`%s` para no especifican)" % bypass.es,
                            "pt": "Por favor entre no número (`%s` para não especificam)" % bypass.pt
                        })
                        dit = {
                            "cancel": bypass.getByShortLang(lang),
                            "max": 200,
                            "min": 0,
                            "error_msg": error_msg.getByShortLang(lang)
                        }
                        value = Text({
                            "fr": "Entrez le Niveau Recherché ci-dessous : (`%s` pour ne pas en spécifier)" % bypass.fr,
                            "en": "Enter the level you are looking for below : (`%s` for not specify)" % bypass.en,
                            "es": "Introduce el nivel que estás buscando abajo : (`%s` para no especifican)" % bypass.es,
                            "pt": "Insira o nível que você está procurando abaixo : (`%s` para não especificam)" % bypass.pt
                        })
                        await own_discord.reponse_send(ctx, value.getByShortLang(lang))
                        level = await own_discord.wait_input(ctx, own_discord.int_check, dit, 120.0)
                        await ctx.typing()
                    data = self.avatars.fetchAvatarsByServerElementAndJob(
                        server, job, level if level is not None and level != -1 else None)
                else:
                    found = False
                    await own_discord.reponse_send(ctx, constants.job_not_exist.getByShortLang(lang))
                    await utils.Job.sendJobs(ctx, lang)
            else:
                data = self.avatars.fetchAvatarsByServerElement(server)
            if len(data) > 0:
                await utils.Avatar.display(ctx, data, lang, self.avatars, job)
            else:
                if found == True:
                    await own_discord.reponse_send(ctx, constants.not_found.getByShortLang(lang))

    @commands.command(aliases=["supprimerAvatar", "quiteAvatar", "retireAvatar"])
    async def removeAvatar(self, ctx: commands.Context, *, name: str = None):
        lang = await super().init_command(ctx)
        argDict = self.extractArg(name)
        name = argDict["arg"]
        lang = argDict["lang"] if argDict["lang"] != None else lang
        if not name:
            data = self.avatars.fetchAvatarsByDiscordId(ctx.author.id)
            avatar = await utils.Avatar.select(ctx, data, lang, self.users)
            name = avatar.name if avatar else None
        if name:
            name = name if await utils.Avatar.haveAvatar(ctx, self.avatars, name, lang) else None
        if name:
            await utils.Avatar.remove(ctx, name, lang, self.avatars)

    @commands.command(aliases=["getJobLvl", "niveauMétier", "consigaNivelTrabajo", "adquiraNívelEmprego"])
    async def getJobLevel(self, ctx: commands.Context, name: str = None, job: str = None):
        lang = await super().init_command(ctx)
        avatar = await utils.Avatar.extractAvatar(ctx, self.avatars, name, lang)
        if avatar:
            if not job:
                raw_job = await utils.Job.selectJobs(ctx, lang)
            else:
                raw_job = utils.Job.checkJob(job, lang)
                if raw_job and isinstance(raw_job, list) is True:
                    if len(raw_job) == 1:
                        raw_job = raw_job[0]
                    else:
                        raw_job = None
                if raw_job is None:
                    await own_discord.reponse_send(ctx, constants.job_not_exist.getByShortLang(lang))
                    await utils.Job.sendJobs(ctx, lang)
            if raw_job:
                job = raw_job.id
                data = self.avatars.fetchJobLvlByNameAndJob(
                    ctx.author.id, avatar.name, job)
                if data:
                    await own_discord.reponse_send(ctx, "%s:%i" % (utils.Job.extractJob(job, lang), data.lvl))
                else:
                    await own_discord.reponse_send(ctx, constants.no_renseigned.getByShortLang(lang))

    @commands.command(aliases=["setJobLvl", "modifierNiveauMétier", "juegoNivelTrabajo", "jogoNívelEmprego"])
    async def setJobLevel(self, ctx: commands.Context, lvl: int, *, job_name: str = None):
        lang = await super().init_command(ctx)
        avatar = await utils.Avatar.extractAvatar(ctx, self.avatars, None, lang)
        if avatar:
            if not job_name:
                raw_job = await utils.Job.selectJobs(ctx, lang)
            else:
                raw_job = utils.Job.checkJob(job_name, lang)
                if raw_job and isinstance(raw_job, list) is True:
                    if len(raw_job) == 1:
                        raw_job = raw_job[0]
                    else:
                        raw_job = None
                if raw_job is None:
                    await own_discord.reponse_send(ctx, constants.job_not_exist.getByShortLang(lang))
                    await utils.Job.sendJobs(ctx, lang)
            if raw_job:
                job = raw_job.id
                lvl = utils.Lvl.checkJobLvl(lvl)
                if not lvl:
                    await own_discord.reponse_send(ctx, constants.job_lvl_incorrect.getByShortLang(lang))
                else:
                    if self.avatars.setJob(ctx.author.id, avatar.name, job, lvl):
                        await own_discord.reponse_send(
                            ctx,
                            "%s (%s:%s)" % (constants.job_set.getByShortLang(
                                lang), raw_job.title.getByShortLang(lang), lvl)
                        )
                    else:
                        await own_discord.reponse_send(ctx, constants.job_not_set.getByShortLang(lang))

    @commands.command(aliases=[constants.lvl.fr, constants.lvl.es, constants.lvl.pt])
    async def level(self, ctx: commands.Context, name: str = None, lvl: int = None):
        lang = await super().init_command(ctx)
        avatar = await utils.Avatar.extractAvatar(ctx, self.avatars, name, lang)
        if avatar:
            if lvl != None:
                lvl = utils.Lvl.checkAvatarLvl(lvl)
                if not lvl:
                    await own_discord.reponse_send(ctx, constants.lvl_avatar_incorrect.getByShortLang(lang))
                else:
                    if self.avatars.setLvl(ctx.author.id, avatar.name, lvl):
                        await own_discord.reponse_send(ctx, constants.lvl_avatar_set.getByShortLang(lang))
                    else:
                        await own_discord.reponse_send(ctx, constants.lvl_avatar_not_set.getByShortLang(lang))
            else:
                if avatar.lvl:
                    await own_discord.reponse_send(ctx, "%i" % avatar.lvl)
                else:
                    await own_discord.reponse_send(ctx, constants.no_renseigned.getByShortLang(lang))

    @commands.command(aliases=["plans", "patrón", "padrão"])
    async def pattern(self, ctx: commands.Context, name: str = None, *, name_pattern: str = None):
        lang = await super().init_command(ctx)
        avatar = await utils.Avatar.extractAvatar(ctx, self.avatars, name, lang)
        if avatar:
            if name_pattern:
                patterns = []
                ite = []
                items = await Recipe.asyncFindRecipeByItemResultName(wakdata.latest, name_pattern, lang)
                for item in items:
                    item.fetchData(wakdata.latest, 2)
                    if item.recipePattern != None:
                        if not item.recipePattern.id in patterns:
                            patterns.append(item.recipePattern.id)
                            ite.append(item.recipeResults[0].item)
                item = await utils.ItemInteract.select(ctx, ite, lang)
                if item:
                    pattern = await RecipePattern.asyncFindRecipePattern(wakdata.latest["recipePattern"], patterns[ite.index(item)])
                    if self.avatars.addPattern(ctx.author.id, avatar.name, pattern.id):
                        await own_discord.reponse_send(ctx, " %s (%s)" % (constants.pattern_add.getByShortLang(lang), pattern.name.getByShortLang(lang)))
                    else:
                        dit = {
                            "cancel": constants.cancel.getByShortLang(lang)
                        }
                        await own_discord.reponse_send(ctx, constants.pattern_suppress_request.getByShortLang(lang))
                        response = await own_discord.wait_input(ctx, own_discord.str_check, dit, 120.0)
                        if response and response != -1:
                            if response == constants.confirm.getByShortLang(lang):
                                self.avatars.removePattern(
                                    ctx.author.id, avatar.name, pattern.id)
                                await own_discord.reponse_send(ctx, constants.pattern_suppress.getByShortLang(lang))
                            else:
                                await own_discord.reponse_send(ctx, constants.bad_argument.getByShortLang(lang))
                        else:
                            await own_discord.reponse_send(ctx, constants.canceled.getByShortLang(lang))
            else:
                msg = utils.Pattern.extractRecipeAvatar(
                    avatar.id, self.avatars, lang)
                if len(msg) > 0:
                    await own_discord.embed_send(ctx, own_discord.embed_create(msg))
                else:
                    await own_discord.reponse_send(ctx, constants.no_pattern_register.getByShortLang(lang))

    @commands.command(aliases=["plansManquants", "modeloAusente"])
    async def missingPattern(self, ctx: commands.Context, *, name: str = None):
        lang = await super().init_command(ctx)
        argDict = self.extractArg(name)
        name = argDict["arg"]
        lang = argDict["lang"] if argDict["lang"] != None else lang
        avatar = await utils.Avatar.extractAvatar(ctx, self.avatars, name, lang)
        if avatar:
            msg = utils.Pattern.extractMissingRecipeAvatar(
                avatar.id, self.avatars, lang)
            if len(msg) > 0:
                await own_discord.embed_send(ctx, own_discord.embed_create(msg))
            else:
                await own_discord.reponse_send(ctx, constants.no_pattern_missing.getByShortLang(lang))
