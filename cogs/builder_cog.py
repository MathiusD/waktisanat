import utils.builders as builders
from cogs import CogsDBItem
from discord.ext import commands
from cdnwakfu import Text
from own import own_discord

class BuilderCogs(CogsDBItem):

    own_name = Text({
        "fr":"Builders",
        "en":"Builders",
        "es":"Builders",
        "pt":"Builders"
    })

    @commands.command()
    async def getCraftkfuLink(self, ctx:commands.Context, code:str):
        lang = await super().init_command(ctx, True)
        if lang:
            build = builders.BuildInteract.isUriForApi(code)
            if not build:
                builder = await builders.BuildInteract.select(ctx, lang)
                if builder:
                    build = builder.getBuild(code)
            if build:
                await build.api.sendCraftkfuLink(build, ctx)

    @commands.command()
    async def getComponentsOfBuild(self, ctx:commands.Context, code:str):
        lang = await super().init_command(ctx, True)
        if lang:
            build = builders.BuildInteract.isUriForApi(code)
            if not build:
                builder = await builders.BuildInteract.select(ctx, lang)
                if builder:
                    build = builder.getBuild(code)
            if build:
                await build.api.sendAllComponent(build, lang, ctx)

    @commands.command()
    async def compareComponentsOfBuilds(self, ctx:commands.Context, firstCode:str, secondCode:str):
        lang = await super().init_command(ctx, True)
        if lang:
            firstBuild = builders.BuildInteract.isUriForApi(firstCode)
            if not firstBuild:
                if len(builders.builders) > 1:
                    request = Text({
                        "fr":"Veuillez selectionner le builder employé pour le premier code",
                        "en":"Please choose the builder used for the first code",
                        "es":"Por favor elija al constructor usado para el primer código",
                        "pt":"Por favor escolha o construtor usado para o primeiro código"
                    })
                    await own_discord.reponse_send(ctx, request.getByShortLang(lang))
                firstBuilder = await builders.BuildInteract.select(ctx, lang)
                firstBuild = firstBuilder.getBuild(firstCode) if firstBuilder else None
            if firstBuild:
                secondBuild = builders.BuildInteract.isUriForApi(secondCode)
                if not secondBuild:
                    if len(builders.builders) > 1:
                        request = Text({
                            "fr":"Veuillez selectionner le builder employé pour le second code",
                            "en":"Please choose the builder used for the second code",
                            "es":"Por favor elija al constructor usado para el segundo código",
                            "pt":"Por favor escolha o construtor usado para o segundo código"
                        })
                        await own_discord.reponse_send(ctx, request.getByShortLang(lang))
                    secondBuilder = await builders.BuildInteract.select(ctx, lang)
                    secondBuild = secondBuilder.getBuild(secondCode) if secondBuilder else None
            if secondBuild:
                await firstBuild.api.sendCompareComponents(firstBuild, secondBuild, lang, ctx)

    @commands.command()
    async def showItemInBuild(self, ctx:commands.Context, code:str):
        lang = await super().init_command(ctx, True)
        if lang:
            build = builders.BuildInteract.isUriForApi(code)
            if not build:
                builder = await builders.BuildInteract.select(ctx, lang)
                if builder:
                    build = builder.getBuild(code)
            if build:
                await build.api.showBuild(build, lang, ctx, self.requests, self.prices)