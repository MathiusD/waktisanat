from discord.ext import commands
from models import User, Guild, Avatar, Request, ItemPrice
from config.settings import DISCORD_LANG
from cdnwakfu import Text
import utils
import re


class CogsDBBase(commands.Cog):

    own_name = "Base"

    def __init__(self, users: User, guilds: Guild):
        self.users = users
        self.guilds = guilds

    async def init_command(self, ctx: commands.Context, with_Choice: bool = False):
        await ctx.typing()
        lang = utils.Lang.getLang(ctx, self.users, self.guilds, with_Choice)
        if lang is None:
            lang = await utils.Lang.select(ctx, DISCORD_LANG)
            if lang is None:
                return None
            else:
                lang = lang.nameAttr
        return lang

    @staticmethod
    def extractArg(arg: str):
        langRegexSection = ""
        for lang in Text.langs:
            for short in lang.aliases:
                langRegexSection = "%s%s%s" % (
                    langRegexSection, "|" if len(langRegexSection) > 0 else "", short)
            langRegexSection = "%s%s%s" % (langRegexSection, "|" if len(
                langRegexSection) > 0 else "", lang.nameAttr)
        regex = "(\"|`|').*(\"|`|')( (%s))?" % langRegexSection
        if arg != None and re.match(regex, arg):
            splited = arg.split("\"")
            return {
                "arg": splited[1],
                "lang": utils.Lang.checkLang(splited[len(splited)-1].split(" ")[1]) if re.match(regex[:-1], arg) else None
            }
        else:
            return {"arg": arg, "lang": None}


class CogsDBAvatar(CogsDBBase):

    own_name = "Avatar"

    def __init__(self, guilds: Guild, avatars: Avatar):
        self.avatars = avatars
        super().__init__(avatars.users, guilds)

    async def avatarSelection(self, ctx: commands.Context, name: str = None, lang: str = None):
        lang = await self.init_command(ctx) if lang is None else lang
        avatar = await utils.Avatar.extractAvatar(ctx, self.avatars, name, lang)
        if avatar:
            return avatar
        return None


class CogsDBRequest(CogsDBAvatar):

    own_name = "Request"

    def __init__(self, guilds: Guild, requests: Request):
        self.requests = requests
        self.craftRequests = requests.craftRequests
        self.componentRequest = requests.componentRequests
        self.buyRequests = requests.buyRequests
        super().__init__(guilds, requests.avatars)


class CogsDBItem(CogsDBRequest):

    own_name = "Item"

    def __init__(self, guilds: Guild, requests: Request, prices: ItemPrice):
        self.prices = prices
        super().__init__(guilds, requests)
