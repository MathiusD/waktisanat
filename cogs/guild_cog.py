from cogs import CogsDBBase
from own import own_discord
from own.own_sqlite.models import Model
from discord.ext import commands
import constants
from config import settings
import utils, discord
from cdnwakfu import Text

class GuildCogs(CogsDBBase):

    own_name = Text({
        "fr":"Serveur",
        "en":"Guild",
        "es":"Gremio",
        "pt":"Corporação"
    })

    @commands.command(aliases=["langue"])
    @commands.guild_only()
    @commands.has_guild_permissions(administrator=True)
    async def lang(self, ctx:commands.Context, lang:str = None):
        lang_ = await super().init_command(ctx)
        if not lang:
            data = self.guilds.fetchLangOfServer(ctx.guild.id)
            if data:
                msg = await own_discord.reponse_send(ctx, data)
                guild = self.guilds.fetchGuildByDiscordId(ctx.guild.id)
                if guild and guild.lang:
                    if await own_discord.wait_react(ctx, msg, "❌") is True:
                        if self.guilds.removeOptionsofGuild(ctx.guild.id, "lang"):
                            msg = await own_discord.reponse_send(ctx, constants.lang_suppress.getByShortLang(lang_))
                        else:
                            await own_discord.reponse_send(ctx, constants.error_occured.getByShortLang(lang_))
            else:
                await own_discord.reponse_send(ctx, constants.no_renseigned.getByShortLang(lang_))
        else:
            append = False
            lang = await utils.Lang.checkLangWithSet(ctx, lang, self.users, self.guilds)
            data = self.guilds.updateGuild(ctx.guild.id, lang, verbose=True)
            if not data:
                self.guilds.addGuild(ctx.guild.id, lang)
                append = True
            if Model.checkInsertUpdateMany(data) == True or append == True:
                await own_discord.reponse_send(ctx, constants.lang_defined.getByShortLang(lang))
            else:
                await own_discord.reponse_send(ctx, constants.lang_update_error.getByShortLang(lang))

    @commands.command(aliases=["salonNotifications", "notifiqueCanal"])
    @commands.guild_only()
    @commands.has_guild_permissions(administrator=True)
    async def notifyChannel(self, ctx:commands.Context, chan:discord.TextChannel=None):
        lang = await super().init_command(ctx)
        if not chan:
            data = self.guilds.fetchGuildByDiscordId(ctx.guild.id)
            if data and data.notify_chan:
                msg = await own_discord.reponse_send(ctx, "<#%i>" % data.notify_chan)
                if await own_discord.wait_react(ctx, msg, "❌") is True:
                    if self.guilds.removeOptionsofGuild(ctx.guild.id, "notify_chan"):
                        await own_discord.reponse_send(ctx, constants.notification_disable.getByShortLang(lang))
                    else:
                        await own_discord.reponse_send(ctx, constants.error_occured.getByShortLang(lang))
            else:
                await own_discord.reponse_send(ctx, constants.no_renseigned.getByShortLang(lang))
        else:
            append = False
            data = self.guilds.updateGuild(ctx.guild.id, chan=chan.id, verbose=True)
            if not data:
                self.guilds.addGuild(ctx.guild.id, chan=chan.id)
                append = True
            if Model.checkInsertUpdateMany(data) == True or append == True:
                await own_discord.reponse_send(ctx, constants.chan_defined.getByShortLang(lang))
            else:
                await own_discord.reponse_send(ctx, constants.error_occured.getByShortLang(lang))

    @commands.command(aliases=["préfixe", "prefijo", "prefixo"])
    @commands.guild_only()
    @commands.has_guild_permissions(administrator=True)
    async def prefix(self, ctx:commands.Context, prefix:str=None):
        lang = await super().init_command(ctx)
        if not prefix:
            data = self.guilds.fetchGuildByDiscordId(ctx.guild.id)
            if data:
                msg = await own_discord.reponse_send(ctx, data.prefix if data.prefix else constants.no_prefix_defined(settings.DISCORD_PREFIX).getByShortLang(lang))
                if data.prefix:
                    if await own_discord.wait_react(ctx, msg, "❌") is True:
                        if self.guilds.removeOptionsofGuild(ctx.guild.id, "prefix"):
                            await own_discord.reponse_send(ctx, constants.prefix_suppress.getByShortLang(lang))
                        else:
                            await own_discord.reponse_send(ctx, constants.error_occured.getByShortLang(lang))
            else:
                await own_discord.reponse_send(ctx, constants.no_renseigned.getByShortLang(lang))
        else:
            append = False
            data = self.guilds.updateGuild(ctx.guild.id, prefix=prefix, verbose=True)
            if not data:
                self.guilds.addGuild(ctx.guild.id, prefix=prefix)
                append = True
            if Model.checkInsertUpdateMany(data) == True or append == True:
                await own_discord.reponse_send(ctx, constants.prefix_defined.getByShortLang(lang))
            else:
                await own_discord.reponse_send(ctx, constants.error_occured.getByShortLang(lang))