from discord.ext import commands
from utils import Suggest, Report, Issue, RepoInteract, repos, all_repos, read_all_repos
from own import own_discord
from cogs import CogsDBBase
import constants
from cdnwakfu import Text
import models
from models import User, Guild

class IssueCogs(CogsDBBase):

    own_name = Text({
        "fr":"Tickets",
        "en":"Issue",
        "es":"Cuestión",
        "pt":"Questão"
    })

    def __init__(self, users:User, guilds:Guild, issues:models.Issue):
        self.issues = issues
        self.notes = issues.notes
        super().__init__(users, guilds)

    @commands.command(aliases=["voirSesTickets", "mostrarPropiasCuestiones", "mostrarPrópriasQuestões"])
    async def showOwnIssues(self, ctx:commands.Context):
        lang = await super().init_command(ctx, True)
        if lang:
            user = self.users.fetchUserByDiscordId(ctx.author.id)
            if user:
                modelIssues = self.issues.fetchIssueOfUser(ctx.author.id)
                issues = Issue.FetchIssuesByModelData(modelIssues)
                await Issue.display(ctx, issues, lang, self.issues)
            else:
                await own_discord.reponse_send(ctx, constants.account_required.getByShortLang(lang))


    @commands.command(aliases=["ajouterSuggestion", "añadaSugieren", "acrescenteSugerem"])
    async def addSuggest(self, ctx:commands.Context):
        lang = await super().init_command(ctx, True)
        if lang:
            await Suggest.add(ctx, lang, self.issues)

    @commands.command(aliases=["reporterErreur", "añadaInforme", "acrescenteRelatório"])
    async def addReport(self, ctx:commands.Context):
        lang = await super().init_command(ctx, True)
        if lang:
            await Report.add(ctx, lang, self.issues)

    @commands.command(aliases=["suggestions", "sugiere", "sugere"])
    async def suggests(self, ctx:commands.Context, opened:bool = None):
        lang = await super().init_command(ctx, True)
        if lang:
            repo = await RepoInteract.select(ctx, lang, read_all_repos)
            if repo:
                if repo == all_repos:
                    suggests = []
                    for rep in repos:
                        if opened:
                            temp = Suggest.fetchAllWithState(rep, opened)
                        else:
                            temp = Suggest.fetchAll(rep)
                        for suggest in temp:
                            suggests.append(suggest)
                else:
                    if opened:
                        suggests = Suggest.fetchAllWithState(repo, opened)
                    else:
                        suggests = Suggest.fetchAll(repo)
                if suggests and len(suggests) > 0:
                    suggest = await Issue.display(ctx, suggests, lang, self.issues)
                else:
                    await own_discord.reponse_send(ctx, constants.no_suggest.getByShortLang(lang))

    @commands.command(aliases=["erreurs", "informes", "relatórios"])
    async def reports(self, ctx:commands.Context, opened:bool = None):
        lang = await super().init_command(ctx, True)
        if lang:
            repo = await RepoInteract.select(ctx, lang, read_all_repos)
            if repo:
                if repo == all_repos:
                    reports = []
                    for rep in repos:
                        if opened:
                            temp = Report.fetchAllWithState(rep, opened)
                        else:
                            temp = Report.fetchAllWithState(rep)
                        for report in temp:
                            reports.append(report)
                else:
                    if opened:
                        reports = Report.fetchAllWithState(repo, opened)
                    else:
                        reports = Report.fetchAllWithState(repo)
                if reports and len(reports) > 0:
                    report = await Issue.display(ctx, reports, lang, self.issues)
                else:
                    await own_discord.reponse_send(ctx, constants.no_report_open.getByShortLang(lang))