from discord.ext import commands
from cogs import CogsDBItem
from data import wakdata
import constants
from own import own_discord
import utils
import cdnwakfu


class ItemCogs(CogsDBItem):

    own_name = cdnwakfu.Text({
        "fr": "Objet",
        "en": "Item",
        "es": "Artículo",
        "pt": "Item"
    })

    @commands.command(aliases=["rechercher", "buscar"])
    async def search(self, ctx: commands.Context, *, name: str):
        lang = await super().init_command(ctx, True)
        argDict = self.extractArg(name)
        name = argDict["arg"]
        lang = argDict["lang"] if argDict["lang"] != None else lang
        item = None
        if lang:
            items = await utils.ItemInteract.getItems(name, lang)
            item = await utils.ItemInteract.select(ctx, items, lang)
            if item:
                await utils.ItemInteract.send(ctx, item, lang, self.requests, self.prices)

    @commands.command(aliases=["rechercherObjetFabriquéAPartirDe", "busqueArtedelArtículoCon", "procureOfíciodeItemcom"])
    async def searchItemCraftWith(self, ctx: commands.Context, *, name: str):
        lang = await super().init_command(ctx, True)
        argDict = self.extractArg(name)
        name = argDict["arg"]
        lang = argDict["lang"] if argDict["lang"] != None else lang
        item = None
        if lang:
            items = await utils.ItemInteract.getItems(name, lang)
            item = await utils.ItemInteract.select(ctx, items, lang)
            if item:
                wait = await own_discord.reponse_send(ctx, constants.processing.getByShortLang(lang))
                await ctx.typing()
                recipes = await cdnwakfu.Recipe.asyncFindRecipeByItemIngredientId(wakdata.latest, item._id)
                items_child = []
                for recipe in recipes:
                    recipe.fetchData(wakdata.latest, 2)
                    for ite in recipe.recipeResults:
                        items_child.append(ite.item)
                await wait.delete()
                item_child = await utils.ItemInteract.select(ctx, items_child, lang)
                if item_child:
                    await ctx.typing()
                    await utils.ItemInteract.send(ctx, item_child, lang, self.requests, self.prices)

    @commands.command(aliases=["rechercherObjetAvecCritères", "busqueArtículoConCriterios", "procureItemComCritérios"])
    async def searchItemWithCriteria(self, ctx: commands.Context):
        lang = await super().init_command(ctx, True)
        if lang:
            data = await utils.filters.Filter.select(ctx, utils.filters.filters, lang)
            criterias = []
            while data and data != True:
                criterias.append(data)
                data = await utils.filters.Filter.select(ctx, utils.filters.filters, lang)
            if data == True:
                if len(criterias) > 0:
                    msg = "%s :\n%s" % (constants.request.getByShortLang(
                        lang), utils.filters.Filter.createList(criterias, lang, False))
                    await own_discord.embed_send(ctx, own_discord.embed_create(msg))
                    await ctx.typing()
                    items = utils.filters.Search(criterias).search()
                    item = await utils.ItemInteract.select(ctx, items, lang)
                    if item:
                        await utils.ItemInteract.send(ctx, item, lang, self.requests, self.prices)
                else:
                    await own_discord.reponse_send(ctx, constants.request_empty.getByShortLang(lang))
