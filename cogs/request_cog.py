from cogs import CogsDBItem
from discord.ext import commands
import utils, cdnwakfu

class RequestCogs(CogsDBItem):

    own_name = cdnwakfu.Text({
        "fr":"Requêtes",
        "en":"Request",
        "es":"Solicitud",
        "pt":"Pedido"
    })

    @commands.command(aliases=["soumettreRequête", "añadaSolicitud", "acrescentePedido"])
    async def addRequest(self, ctx:commands.Context, name:str, avatar_name:str = None):
        lang = await super().init_command(ctx)
        avatar = await super().avatarSelection(ctx, avatar_name, lang)
        if avatar:
            requestManager = await utils.requests.RequestInteract().select(ctx, lang)
            if requestManager:
                await requestManager.addRequestWithSearch(ctx, avatar, lang, self.requests, name)

    @commands.command(aliases=["requêtesAvatar", "avatarSolicitudes" ,"avatarPedidos"])
    async def requestsAvatar(self, ctx:commands.Context, *, name:str = None):
        lang = await super().init_command(ctx)
        avatar = await super().avatarSelection(ctx, name, lang)
        if avatar:
            requestManager = await utils.requests.RequestInteract().select(ctx, lang)
            if requestManager:
                await requestManager.requestsAvatar(ctx, avatar, lang, self.requests, self.prices)

    @commands.command(aliases=["requêtesDisponibles", "solicitudesDisponibles", "pedidosDisponíveis"])
    async def requestsAvailable(self, ctx:commands.Context, *, name:str = None):
        lang = await super().init_command(ctx)
        avatar = await super().avatarSelection(ctx, name, lang)
        if avatar:
            requestManager = await utils.requests.RequestInteract().select(ctx, lang)
            if requestManager:
                await requestManager.requestsAvailable(ctx, avatar, lang, self.requests, self.prices)

    @commands.command(aliases=["supprimerToutesRequêtes", "quiteTodaSolicitud", "retireTodoPedido"])
    async def removeAllRequest(self, ctx:commands.Context, *, avatar_name:str = None):
        lang = await super().init_command(ctx)
        avatar = await super().avatarSelection(ctx, avatar_name, lang)
        if avatar:
            requestManager = await utils.requests.RequestInteract().select(ctx, lang)
            if requestManager:
                await requestManager.removeAllRequest(ctx, avatar, lang, self.requests)