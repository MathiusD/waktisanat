from discord.ext import commands
from own import own_discord
from cogs import CogsDBBase
import constants
from config import settings
import utils
from cdnwakfu import Text

class UserCogs(CogsDBBase):

    own_name = Text({
        "fr":"Utilisateur",
        "en":"User",
        "es":"Usuario",
        "pt":"Usuário"
    })

    @commands.command(aliases=["enregistrer", "registro"])
    async def register(self, ctx:commands.Context, name:str = None, lang:str = None, notify:str = None, prefix:str = None):
        lang_ = await super().init_command(ctx)
        if not name:
            name = ctx.author.name
        if not lang:
            lang = lang_
        else:
            lang = await utils.Lang.checkLangWithSet(ctx, lang, self.users, self.guilds)
        if not notify:
            notify = utils.Notify.default
        else:
            notify = await utils.Notify.checkNotify(ctx, notify, lang)
        user = self.users.addUser(name , ctx.author.id, lang, notify, prefix)
        if user:
            await own_discord.reponse_send(ctx, constants.account_created.getByShortLang(lang))
        else:
            await own_discord.reponse_send(ctx, constants.account_already_exist.getByShortLang(lang))

    @staticmethod
    async def sendUpdate(ctx:commands.Context, updated:bool, lang:str):
        if updated is True:
            await own_discord.reponse_send(ctx, constants.account_update.getByShortLang(lang))
        else:
            await own_discord.reponse_send(ctx, constants.account_error_update.getByShortLang(lang))

    @commands.command(aliases=["préfixeCompte", "cuentaPrefijo", "contaPrefixo"])
    async def prefixAccount(self, ctx:commands.Context, prefix:str = None):
        lang = await super().init_command(ctx)
        user = self.users.fetchUserByDiscordId(ctx.author.id)
        if user:
            if prefix:
                result = self.users.updateUserPrefix(ctx.author.id, prefix)
                await self.__class__.sendUpdate(ctx, result, lang)
            else:
                await own_discord.reponse_send(ctx, user.prefix if user.prefix else constants.no_prefix_defined(settings.DISCORD_PREFIX).getByShortLang(lang))
        else:
            if prefix:
                await own_discord.reponse_send(ctx, constants.account_not_found_created.getByShortLang(lang))
                await self.register(ctx, prefix=prefix)
            else:
                await own_discord.reponse_send(ctx, constants.no_account.getByShortLang(lang))

    @commands.command(aliases=["langueCompte", "cuentaLang", "contaLang"])
    async def langAccount(self, ctx:commands.Context, lang:str = None):
        lang_ = await super().init_command(ctx)
        user = self.users.fetchUserByDiscordId(ctx.author.id)
        if user:
            if lang:
                lang = await utils.Lang.checkLangWithSet(ctx, lang, self.users, self.guilds)
                result = self.users.updateUserLang(ctx.author.id, lang)
                await self.__class__.sendUpdate(ctx, result, lang)
            else:
                await own_discord.reponse_send(ctx, user.lang)
        else:
            if lang:
                await own_discord.reponse_send(ctx, constants.account_not_found_created.getByShortLang(lang))
                await self.register(ctx, lang=lang)
            else:
                await own_discord.reponse_send(ctx, constants.no_account.getByShortLang(lang_))

    @commands.command(aliases=["nomCompte", "nombreCuenta", "nomeConta"])
    async def nameAccount(self, ctx:commands.Context, name:str = None):
        lang = await super().init_command(ctx)
        user = self.users.fetchUserByDiscordId(ctx.author.id)
        if user:
            if name:
                result = self.users.updateUserName(ctx.author.id, name)
                await self.__class__.sendUpdate(ctx, result, lang)
            else:
                await own_discord.reponse_send(ctx, user.name)
        else:
            if name:
                await own_discord.reponse_send(ctx, constants.account_not_found_created.getByShortLang(lang))
                await self.register(ctx, name=name)
            else:
                await own_discord.reponse_send(ctx, constants.no_account.getByShortLang(lang))

    @commands.command(aliases=["notification", "informar", "notificar"])
    async def notify(self, ctx:commands.Context, notify:str = None):
        lang = await super().init_command(ctx)
        user = self.users.fetchUserByDiscordId(ctx.author.id)
        if user:
            if notify:
                notify = await utils.Notify.checkNotify(ctx, notify, lang)
                result = self.users.updateUserNotify(ctx.author.id, notify)
                await self.__class__.sendUpdate(ctx, result, lang)
            else:
                await own_discord.reponse_send(ctx, utils.Notify.extractNotify(user.notify, lang))
        else:
            if notify:
                await own_discord.reponse_send(ctx, constants.account_not_found_created.getByShortLang(lang))
                await self.register(ctx, notify=notify)
            else:
                await own_discord.reponse_send(ctx, constants.no_account.getByShortLang(lang))

    @commands.command()
    async def unregister(self, ctx:commands.Context):
        await utils.User.remove(ctx, await super().init_command(ctx), self.users)