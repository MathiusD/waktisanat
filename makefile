default_target: deploy

start:
	@echo "Launch Bot"
	@python3 bot.py

launch: fetch start

deploy: rebuild launch

build:
	@echo "Fetch scripts"
	@pip install --upgrade pip
	@pip install -r requirements.txt
	@git clone https://gitlab.com/MathiusD/waktisanat-script
	@mv ./waktisanat-script/*.sh .
	@rm -rf waktisanat-script
	@sh build.sh

fetch:
	@sh fetch.sh

clear_log:
	@rm logs/*.log

clear:
	@echo "Clear Repo"
	@rm -rf cdnwakfu config_wakdata fetch.py data/wakdata building own own_install.py models build.sh fetch.sh config/settings_wakdata.py data/__init__.py

rebuild: clear update build

rebuild_data : rebuild fetch

update:
	@git pull

docker_update: update
	@docker-compose build

docker_force_update: update
	@docker-compose build --no-cache

docker_up: docker_update docker_start

docker_start:
	@docker-compose up -d

docker_down:
	@docker-compose down

docker_restart: docker_down docker_start

docker_reload: docker_force_update docker_restart