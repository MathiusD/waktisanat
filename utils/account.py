from discord.ext import commands
from models import User
from own import own_discord
import constants

class Account:

    @staticmethod
    async def haveAccount(ctx:commands.Context, users:User, lang:str):
        if users.fetchUserByDiscordId(ctx.author.id):
            return True
        else:
            await own_discord.reponse_send(ctx, constants.account_required.getByShortLang(lang))
            return False