from own import own_discord
from own.own_sqlite.models import ModelData
from discord.ext import commands
from cdnwakfu import Text, Recipe
import constants
from config import settings
import utils
import models
import discord
import asyncio


class Avatar(utils.Interact):

    @classmethod
    async def sendChoices(cls, ctx: commands.Context, avatar: list, lang: str, users: models.User, avatars: models.Avatar = None, jobIdToDisplay: int = None):
        msg = "%s\n%s" % (constants.select.getByShortLang(
            lang), cls.createList(avatar, lang, users, avatars, jobIdToDisplay))
        return await own_discord.embed_send(ctx, own_discord.embed_create(msg))

    @classmethod
    def createList(cls, avatar: list, lang: str, users: models.User, avatars: models.Avatar = None, jobIdToDisplay: int = None):
        compt = 1
        msg = ""
        for dat in avatar:
            msg = "%s%i%s" % (msg, compt, cls.rowAvatar(dat, users, lang, avatars, jobIdToDisplay))
            compt += 1
        return msg

    @staticmethod
    def rowAvatar(avatar: ModelData, users: models.User, lang: str, avatars: models.Avatar = None, jobIdToDisplay: int = None):
        name = avatar.name
        server = utils.Server.extractServerName(avatar.server)
        msg = "-%s (%s : %s)" % (name,
                                 constants.server.getByShortLang(lang), server)
        if avatar.lvl:
            msg = "%s(%s : %i)" % (
                msg, constants.lvl.getByShortLang(lang), avatar.lvl)
        if jobIdToDisplay and avatars:
            job = avatars.fetchJobLvlByAvatarAndJob(avatar.id, jobIdToDisplay)
            if job:
                msg = "%s(%s : %i)" % (
                    msg, utils.Job.extractJob(jobIdToDisplay, lang), job.lvl)
        msg = "%s[%s : <@%i>]\n" % (msg, constants.user.getByShortLang(
            lang), users.fetchUserById(avatar.id_user).discord_id)
        return msg

    @classmethod
    async def send(cls, ctx: commands.Context, avatar: ModelData, lang: str, avatars: models.Avatar):
        embd = cls.createMsg(avatar, lang, avatars)
        if embd:
            msg = await own_discord.embed_send(ctx, embd)
            user = avatars.users.fetchUserByDiscordId(ctx.author.id)
            if user and avatar.id_user == user.id:
                asyncio.get_event_loop().create_task(
                    cls.removeReact(ctx, msg, avatar, avatars, lang))
            asyncio.get_event_loop().create_task(
                cls.fav(ctx, msg, avatar, avatars, lang))
            asyncio.get_event_loop().create_task(
                cls.updateProfile(ctx, avatar, msg, lang, avatars))
            return msg

    @classmethod
    async def updateProfile(cls, ctx: commands.Context, avatar: ModelData, msg: discord.Message, lang: str, avatars: models.Avatar, with_craft: bool = False):
        try:
            if with_craft is False:
                result = await own_discord.wait_react(ctx, msg, "📃")
            else:
                result = await own_discord.wait_react_remove(ctx, msg, "📃")
            if result is True:
                embd = cls.createMsg(avatar, lang, avatars,
                                     with_recipe=not with_craft)
                if embd:
                    await own_discord.embed_update(ctx, msg, embd)
                    asyncio.get_event_loop().create_task(cls.updateProfile(
                        ctx, avatar, msg, lang, avatars, not with_craft))
        except Exception as exc:
            await ctx.bot.on_command_error(ctx, exc)

    @staticmethod
    async def fav(ctx: commands.Context, msg: discord.Message, avatar: ModelData, avatars: models.Avatar, lang: str):
        try:
            if avatars.fetchFavofAvatarWithId(ctx.author.id, avatar.id):
                if await own_discord.wait_react(ctx, msg, "➖") is True:
                    await ctx.typing()
                    avatars.removeFav(ctx.author.id, avatar.id)
                    await own_discord.reponse_send(ctx, constants.fav_suppress.getByShortLang(lang))
            else:
                if await own_discord.wait_react(ctx, msg, "➕") is True:
                    await ctx.typing()
                    avatars.addFav(ctx.author.id, avatar.id)
                    await own_discord.reponse_send(ctx, " %s (%s)" % (constants.fav_add.getByShortLang(lang), avatar.name))
        except Exception as exc:
            await ctx.bot.on_command_error(ctx, exc)

    @classmethod
    async def removeReact(cls, ctx: commands.Context, msg: discord.Message, avatar: ModelData, avatars: models.Avatar, lang: str):
        try:
            if await own_discord.wait_react(ctx, msg, "❌") is True:
                await ctx.typing()
                await cls.remove(ctx, avatar.name, lang, avatars)
        except Exception as exc:
            await ctx.bot.on_command_error(ctx, exc)

    @staticmethod
    async def remove(ctx: commands.Context, name: str, lang: str, avatars: models.Avatar):
        dit = {
            "cancel": constants.cancel.getByShortLang(lang)
        }
        confirm_req = Text({
            "fr": "Confirmez le nom de l'avatar à supprimer (`%s`) ci-dessous :" % name,
            "en": "Confirm the name of the avatar to be deleted (`%s`) below :" % name,
            "es": "Confirmar el nombre del avatar para suprimirse (`%s`) abajo :" % name,
            "pt": "Confirmar o nome do avatar a eliminar-se (`%s`) abaixo :" % name
        })
        await own_discord.reponse_send(ctx, confirm_req.getByShortLang(lang))
        confirm = await own_discord.wait_input(ctx, own_discord.str_check, dit, 120.0)
        await ctx.typing()
        if not confirm or confirm == -1 or confirm.lower().strip() != name.lower().strip():
            await own_discord.reponse_send(ctx, constants.canceled.getByShortLang(lang))
        else:
            avt = avatars.fetchAvatarsByDiscordIdAndName(ctx.author.id, name)
            if avt:
                allDb = models.AllDb(avatars.db)
                if allDb.removeAvatar(avt):
                    await own_discord.reponse_send(ctx, constants.avatar_deleted.getByShortLang(lang))
                else:
                    await own_discord.reponse_send(ctx, constants.error_occured.getByShortLang(lang))
            else:
                await own_discord.reponse_send(ctx, constants.not_found.getByShortLang(lang))

    @staticmethod
    def extractAvatarCanCraftRecipe(recipe: Recipe, avatars: models.Avatar, server: str = None):
        srv = utils.Server.extractServer(utils.Server.indexOfServer(server))
        return avatars.extractAvatarCanCraftRecipe(recipe, srv)

    @classmethod
    async def showAvatarCanCraftRecipe(cls, ctx: commands.Context, recipe: Recipe, avatars: models.Avatar, lang: str, with_server: bool = True):
        if with_server:
            server = await utils.Server.select(ctx, lang)
        if (with_server and server) or not with_server:
            msg = await own_discord.reponse_send(ctx, constants.processing.getByShortLang(lang))
            fAvatars = cls.extractAvatarCanCraftRecipe(recipe, avatars, server)
            await msg.delete()
            avatar = await cls.select(ctx, fAvatars, lang, avatars.users)
            if avatar:
                await cls.send(ctx, avatar, lang, avatars)

    @staticmethod
    def createMsg(avatar: ModelData, lang: str, avatars: models.Avatar, with_recipe: bool = False):
        msg = "%s:\n" % avatar.name
        msg = "%s\t%s : %s\n" % (msg, constants.server.getByShortLang(
            lang), utils.Server.extractServerName(avatar.server))
        if avatar.lvl:
            msg = "%s\t%s : %i\n" % (
                msg, constants.lvl.getByShortLang(lang), avatar.lvl)
        user = avatars.users.fetchUserById(avatar.id_user)
        msg = "%s\t%s : %s(<@%i>)\n" % (msg,
                                        constants.user.getByShortLang(lang), user.name, user.discord_id)
        for cat in utils.Job.extractJobs():
            job = avatars.fetchJobLvlByAvatarAndJob(avatar.id, cat.id)
            if job:
                msg = "%s\t%s : %i\n" % (
                    msg, utils.Job.extractJob(cat.id, lang), job.lvl)
        if with_recipe:
            msg_pat = utils.Pattern.extractRecipeAvatar(
                avatar.id, avatars, lang)
            msg_not_pat = utils.Pattern.extractMissingRecipeAvatar(
                avatar.id, avatars, lang)
            if msg_pat != "":
                if msg_not_pat == "":
                    msg = "%s\n%s" % (
                        msg, constants.have_all_recipe.getByShortLang(lang))
                elif len(msg_not_pat) < len(msg_pat):
                    msg = "%s\n%s\n%s" % (
                        msg, constants.have_all_recipe_without.getByShortLang(lang), msg_not_pat)
                else:
                    msg = "%s\n%s :\n%s" % (
                        msg, constants.recipes.getByShortLang(lang), msg_pat)
            else:
                msg = "%s\n%s" % (
                    msg, constants.have_not_recipe.getByShortLang(lang))
        msg = "%s\n%s/avatar/%i" % (msg, settings.HOST, avatar.id)
        return own_discord.embed_create(msg)

    @classmethod
    async def display(cls, ctx: commands.Context, data: list, lang: str, avatars: models.Avatar, jobIdToDisplay: int = None):
        data = await cls.select(ctx, data, lang, avatars.users, avatars, jobIdToDisplay)
        if data:
            return await cls.send(ctx, data, lang, avatars)
        return None

    @classmethod
    async def select(cls, ctx: commands.Context, data: list, lang: str, users: models.User, avatars: models.Avatar = None, jobIdToDisplay: int = None):
        if len(data) > 0:
            if len(data) > 1:
                await cls.sendChoices(ctx, data, lang, users, avatars, jobIdToDisplay)
                error_msg = Text({
                    "en": "Entrer un nombre (1 à %i) [Ou %s pour annuler la recherche]" % (len(data), constants.cancel.en),
                    "fr": "Please enter number (1 to %i) [Or %s for cancel search]" % (len(data), constants.cancel.fr),
                    "es": "Por favor entre en el número (1 a %i) [O %s para anulan la búsqueda]" % (len(data), constants.cancel.es),
                    "pt": "Por favor entre no número (1 a %i) [Ou %s para cancelam a pesquisa]" % (len(data), constants.cancel.pt)
                })
                dit = {
                    "cancel": constants.cancel.getByShortLang(lang),
                    "max": len(data) + 1,
                    "min": 0,
                    "error_msg": error_msg.getByShortLang(lang)
                }
                value = await own_discord.wait_input(ctx, own_discord.int_check, dit)
                await ctx.typing()
                if not value or value == -1:
                    await own_discord.reponse_send(ctx, constants.canceled.getByShortLang(lang))
                else:
                    return data[value - 1]
            else:
                return data[0]
        else:
            await own_discord.reponse_send(ctx, constants.not_found.getByShortLang(lang))
        return None

    @classmethod
    async def extractAvatar(cls, ctx: commands.Context, avatars: models.Avatar, name: str, lang: str):
        if not name:
            avatar = None
            data = avatars.fetchAvatarsByDiscordId(ctx.author.id)
            if data:
                avatar = await utils.Avatar.select(ctx, data, lang, avatars.users)
                if avatar:
                    avatar = avatar
        else:
            avatar = avatars.fetchAvatarsByDiscordIdAndName(ctx.author.id, name) if await cls.haveAvatar(ctx, avatars, name, lang) else None
        return avatar

    @staticmethod
    async def haveAvatar(ctx: commands.Context, avatar: models.Avatar, name: str, lang: str):
        if await utils.Account.haveAccount(ctx, avatar.users, lang):
            if avatar.fetchAvatarsByDiscordIdAndName(ctx.author.id, name):
                return True
            else:
                await own_discord.reponse_send(ctx, constants.no_avatar_with_given_name.getByShortLang(lang))
        return False
