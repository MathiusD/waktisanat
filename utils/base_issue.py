from own import own_discord
from utils import Issue, RepoInteract, Repo, default_repo
from discord.ext import commands
from cdnwakfu import Text
import models

class BaseIssue:

    label = Text({
        "en":"Base Issue",
        "fr":"Issue De Base",
        "es":"Edición básica",
        "pt":"Questão baseada"
    })

    @classmethod
    async def add(cls, ctx:commands.Context, lang:str, issues:models.Issue):
        out = None
        msg = Text({
            "fr":"Soumission d'un(e) %s." % cls.label.getByShortLang(lang),
            "en":"Submission of one %s." % cls.label.getByShortLang(lang),
            "es":"Sumisión de un %s." % cls.label.getByShortLang(lang),
            "pt":"Submissão de um %s." % cls.label.getByShortLang(lang)
        })
        await own_discord.reponse_send(ctx, msg.getByShortLang(lang))
        repo = await RepoInteract.select(ctx, lang)
        if repo:
            issue = await Issue.SubmitIssue(ctx, [cls.label.getByShortLang("en")], lang, repo)
            if issue:
                out = issues.addOwnerOfIssue(ctx.author.id, issue["iid"], repo.repo)
        return out

    @classmethod
    def fetchAll(cls, repo:Repo = default_repo):
        return Issue.FetchIssue([{"key":"labels","value":cls.label.getByShortLang("en")}], repo)

    @classmethod
    def fetchAllWithState(cls, repo:Repo = default_repo, opened:bool = True):
        args = [
            {"key":"labels","value":cls.label.getByShortLang("en")},
            {"key":"state","value":"opened"}
        ]
        if opened is not True:
            args[1]["value"] = "closed"
        return Issue.FetchIssue(args, repo)