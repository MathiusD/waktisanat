import constants
from discord.ext import commands
from own import own_discord
from data import wakdata
from utils import ItemInteract
from concurrent.futures.thread import ThreadPoolExecutor
import models
import asyncio
import cdnwakfu


class Api(cdnwakfu.Api):

    ThreadExec = ThreadPoolExecutor(20)

    @classmethod
    async def getCompareComponentsMsg(cls, firstBuild: cdnwakfu.Build, secondBuild: cdnwakfu.Build, lang: str):
        firstLink = "%s%s" % (firstBuild.api.website(), firstBuild.code)
        if firstBuild.code == secondBuild.code and firstBuild.api == secondBuild.api:
            ms = cdnwakfu.Text({
                "fr": "Vous comparez 2 fois le même build (%s)" % firstLink,
                "en": "You compare twice the same build (%s)" % firstLink,
                "es": "Comparas dos veces la misma construcción (%s)" % firstLink,
                "pt": "Compara-se duas vezes o mesmo constroem (%s)" % firstLink
            })
            msg = ms.getByShortLang(lang)
        else:
            secondLink = "%s%s" % (secondBuild.api.website(), secondBuild.code)
            ms = cdnwakfu.Text({
                "fr": "Différence entre le Build (%s) et le Build (%s) :\n" % (firstLink, secondLink),
                "en": "Difference between Build (%s) and Build (%s) :\n" % (firstLink, secondLink),
                "es": "Diferencia entre Build (%s) y Build (%s) :\n" % (firstLink, secondLink),
                "pt": "A diferença entre constrói (%s) e constrói (%s) :\n" % (firstLink, secondLink)
            })
            msg = ms.getByShortLang(lang)
            compare = await asyncio.get_event_loop().run_in_executor(cls.ThreadExec, cls.compareComponents, firstBuild, secondBuild, wakdata.latest)
            if len(compare) > 0:
                for component in compare:
                    msg = "%s\t`%i` x %s\n" % (
                        msg, component.quantity, component.item.title.getByShortLang(lang))
            else:
                ms = cdnwakfu.Text({
                    "fr": "Il n'y a aucune différence de composants entre les deux builds",
                    "en": "There is no difference of components between both builds",
                    "es": "No hay diferencia de componentes entre ambos construye",
                    "pt": "Não há diferença de componentes entre ambos constrói"
                })
                msg = "%s%s" % (msg, ms.getByShortLang(lang))
        return msg

    @classmethod
    async def sendCompareComponents(cls, firstBuild: cdnwakfu.Build, secondBuild: cdnwakfu.Build, lang: str, ctx: commands.Context):
        msg = await own_discord.reponse_send(ctx, constants.processing.getByShortLang(lang))
        await ctx.typing()
        content = await cls.getCompareComponentsMsg(firstBuild, secondBuild, lang)
        await own_discord.embed_update(ctx, msg, own_discord.embed_create(content))
        await msg.edit(content="")

    @classmethod
    async def getComponentsMsg(cls, build: cdnwakfu.Build, lang: str):
        link = "%s%s" % (cls.website(), build.code)
        ms = cdnwakfu.Text({
            "fr": "Build (%s)\nComposé par :\n" % link,
            "en": "Build (%s)\nComposed by :\n" % link,
            "es": "Build (%s)\nFormado por :\n" % link,
            "pt": "Build (%s)\nComposto por :\n" % link
        })
        msg = ms.getByShortLang(lang)
        components = await asyncio.get_event_loop().run_in_executor(cls.ThreadExec, cls.getComponents, build, wakdata.latest)
        if len(components) > 0:
            for component in components:
                msg = "%s\t`%i` x %s\n" % (
                    msg, component.quantity, component.item.title.getByShortLang(lang))
        else:
            ms = cdnwakfu.Text({
                "fr": "Aucun composants",
                "en": "No components",
                "es": "No requiere ningunos componentes",
                "pt": "Nenhum componente"
            })
            msg = "%s\t%s" % (msg, ms.getByShortLang(lang))
        return msg

    @classmethod
    async def sendAllComponent(cls, build: cdnwakfu.Build, lang: str, ctx: commands.Context):
        msg = await own_discord.reponse_send(ctx, constants.processing.getByShortLang(lang))
        await ctx.typing()
        content = await cls.getComponentsMsg(build, lang)
        await own_discord.embed_update(ctx, msg, own_discord.embed_create(content))
        await msg.edit(content="")

    @classmethod
    async def showBuild(cls, build: cdnwakfu.Build, lang: str, ctx: commands.Context, requests: models.Request, prices: models.ItemPrice):
        await ItemInteract.display(ctx, cls.getItems(build, wakdata.latest), lang, requests, prices)

    @classmethod
    async def sendCraftkfuLink(cls, build: cdnwakfu.Build, ctx: commands.Context):
        await ctx.typing()
        link = cls.getCraftkfuLink(build, wakdata.latest)
        await own_discord.reponse_send(ctx, link)
