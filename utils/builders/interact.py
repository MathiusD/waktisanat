from utils import Interact
from discord.ext import commands
from ..builders import Api, builders

class BuildInteract(Interact):

    @staticmethod
    def createMsg(builder:Api, lang:str):
        return builder.website()

    @staticmethod
    def createList(builders:list, lang:str):
        msg = ""
        compt = 1
        for builder in builders:
            msg = "%s%i - %s\n" % (msg, compt, builder.provider)
            compt += 1
        return msg

    @staticmethod
    def isUriForApi(url:str):
        for builder in builders:
            build = builder.extractBuildFromUrl(url)
            if build:
                return build
        return None

    @classmethod
    async def select(cls, ctx:commands.Context, lang:str):
        return await super().select(ctx, builders, lang)

    @classmethod
    async def display(cls, ctx:commands.Context, lang:str):
        return await super().display(ctx, builders, lang)