from ..builders import Api
import cdnwakfu
from data.wakdata import latest

class SlummpApi(cdnwakfu.SlummpApi, Api):

    @classmethod
    def getEquipement(cls, build:cdnwakfu.Build, latest:dict = latest):
        return cdnwakfu.SlummpApi.getEquipement(build, latest)

    @classmethod
    def getSublimation(cls, build:cdnwakfu.Build, latest:dict = latest):
        return cdnwakfu.SlummpApi.getSublimation(build, latest)