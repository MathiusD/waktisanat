from ..builders import Api
import cdnwakfu
from data.wakdata import latest

class ZenithApi(cdnwakfu.ZenithApi, Api):

    @classmethod
    def getEquipement(cls, build:cdnwakfu.Build, latest:dict = latest):
        return cdnwakfu.ZenithApi.getEquipement(build, latest)

    @classmethod
    def getSublimation(cls, build:cdnwakfu.Build, latest:dict = latest):
        return cdnwakfu.ZenithApi.getSublimation(build, latest)