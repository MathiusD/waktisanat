from discord.ext import commands
from own import own_discord, own_gitlab
from config import settings
from utils import Interact
import constants

class Commit(Interact):

    @staticmethod
    def FetchCommit(args=[]):
        commits = own_gitlab.Commit.fetchCommit(settings.GITLAB_PROTOCOL, settings.GITLAB_PROVIDER, settings.GITLAB_AUTHOR, settings.GITLAB_REPO, settings.GITLAB_TOKEN, args)
        web = own_gitlab.Commit.fetchCommit(settings.GITLAB_PROTOCOL, settings.GITLAB_PROVIDER, settings.GITLAB_AUTHOR, settings.GITLAB_WEB_REPO, settings.GITLAB_TOKEN, args)
        data = own_gitlab.Commit.fetchCommit(settings.GITLAB_PROTOCOL, settings.GITLAB_PROVIDER, settings.GITLAB_AUTHOR, settings.GITLAB_DATA_REPO, settings.GITLAB_TOKEN, args)
        models = own_gitlab.Commit.fetchCommit(settings.GITLAB_PROTOCOL, settings.GITLAB_PROVIDER, settings.GITLAB_AUTHOR, settings.GITLAB_MODELS_REPO, settings.GITLAB_TOKEN, args)
        script = own_gitlab.Commit.fetchCommit(settings.GITLAB_PROTOCOL, settings.GITLAB_PROVIDER, settings.GITLAB_AUTHOR, settings.GITLAB_SCRIPT_REPO, settings.GITLAB_TOKEN, args)
        for commit in web:
            commits.append(commit)
        for commit in data:
            commits.append(commit)
        for commit in models:
            commits.append(commit)
        for commit in script:
            commits.append(commit)
        for repo in settings.GITLAB_DEPENDANCIES:
            data = own_gitlab.Commit.fetchCommit(settings.GITLAB_PROTOCOL, settings.GITLAB_PROVIDER, settings.GITLAB_AUTHOR, "%s%s" % (settings.GITLAB_MODULE, repo), settings.GITLAB_TOKEN, args)
            if data:
                for commit in data:
                    commits.append(commit)
        return commits

    @staticmethod
    def createMsg(commit:dict, lang:str):
        msg = "%s : %i\n" % (constants.issue.getByShortLang(lang), commit["iid"])
        msg = "%s%s : %s\n" % (msg, constants.desc.getByShortLang(lang), commit["description"])
        msg = "%s%s : %s\n" % (msg, constants.state.getByShortLang(lang), commit["state"])
        msg = "%s%s : %s\n" % (msg, constants.submit.getByShortLang(lang), commit["created_at"])
        if commit["state"] == "closed":
            msg = "%s%s : %s\n" % (msg, constants.closed.getByShortLang(lang), commit["closed_at"])
        msg = "%s%s : %s\n" % (msg, constants.link.getByShortLang(lang), commit["web_url"])
        return own_discord.embed_create(msg, commit["title"])

    @staticmethod
    def createList(commits:list, lang:str, indiced=True):
        compt = 1
        msg = ""
        for issue in commits:
            new_repo = False
            index = commits.index(issue) - 1 if commits.index(issue) > 0 else None
            if index is not None:
                new_repo = True if issue["web_url"].split('/')[4] != commits[index]["web_url"].split('/')[4] else False
            if (index is None) or (new_repo == True):
                msg = "%s%s:\n" % (msg, issue["web_url"].split('/')[4])
            msg = "%s%i" % (msg, compt) if indiced == True else msg
            msg = "%s - %s (%s)\n" % (msg, issue["title"], issue["author_name"])
            compt += 1
        return msg

    @classmethod
    async def Devlog(cls, ctx:commands.Context, commits:list, lang:str):
        if len(commits) > 0:
            msgContent = "Devlog :\n%s" % cls.createList(commits, lang, False)
            msgs = await own_discord.embed_send(ctx, own_discord.embed_create(msgContent), True)
            await own_discord.publish(msgs)