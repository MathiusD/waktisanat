from utils import Interact

class EquipementItemType(Interact):

    @classmethod
    def createList(cls, equipementItemTypes:list, lang:str):
        compt = 1
        msg = ""
        for equipementItemType in equipementItemTypes:
            msg = "%s%i - %s\n" % (msg, compt, equipementItemType.title.getByShortLang(lang))
            compt += 1
        return msg