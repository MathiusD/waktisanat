from .search import Search
from .filter import Filter
from .compare_filter import CompareFilter
from .actionFilters import *
from .level_filter import LevelFilter
from .equipment_item_type_filter import EquipmentItemTypeFilter
filters = [
    LevelFilter(),
    EquipmentItemTypeFilter(),
    Pa(),
    PaDown(),
    Pm(),
    PmDown(),
    Po(),
    PoDown(),
    Controle(),
    Pw(),
    PwDown(),
    CriticalHit(),
    CriticalHitDown(),
    Block(),
    BlockDown(),
    WillPower(),
    MonoElemMastery(),
    BiElemMastery(),
    TriElemMastery(),
    MonoElemResitance(),
    BiElemResitance(),
    TriElemResitance(),
    HealingMastering(),
    ElementalMastering(),
    FireMastering(),
    EarthMastering(),
    WaterMastering(),
    AirMastering(),
    ElementalMasteringDown(),
    FireMasteringDown(),
    CriticalMastering(),
    RearMastering(),
    RearMasteringDown(),
    AreaMastering(),
    SingleTargetMastering(),
    MeleeMastering(),
    DistanceMastering(),
    BerserkMastering(),
    CriticalMasteringDown(),
    DistanceMasteringDown(),
    BerserkMasteringDown(),
    RearResistance(),
    ElementalResistance(),
    FireResistance(),
    EarthResistance(),
    WaterResistance(),
    AirResistance(),
    FireResistanceDown(),
    EarthResistanceDown(),
    WaterResistanceDown(),
    ElementalResistanceDown(),
    CriticalResistance(),
    CriticalResistanceDown(),
    Initiative(),
    InitiativeDown(),
    Lock(),
    LockDown(),
    Dodge(),
    DodgeDown(),
    Hp(),
    HpDown(),
    Initiative(),
    InitiativeDown(),
    Prospecting(),
    Wisdom(),
    WisdomDown(),
    KitSkill()
]