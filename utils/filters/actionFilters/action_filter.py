from cdnwakfu import Action, ActionCriteria
from data.wakdata import latest
from ...filters import CompareFilter

class ActionFilter(CompareFilter):

    def __init__(self, action:int=None, argLen:int=None, requestValue:list=None, requestOperator:list=None, value:int=None, operator:str=None):
        super().__init__(ActionCriteria(action, argLen=argLen, requestValue=requestValue, requestOperator=requestOperator), value, operator)

    def showExemple(self, lang:str, actions:list = latest["actions"]):
        params = ([0] * (self.criteria.argLen*2)) if self.criteria.argLen else []
        if self.criteria.requestValue and params:
            for val in self.criteria.requestValue:
                index = self.criteria.requestValue.index(val)
                if not self.criteria.requestOperator or self.criteria.requestOperator[index] == "=":
                    params[(index*2)+1] = 1
                    params[index*2] = val
        text = Action.findAction(actions, self.criteria.action).adaptingDescription(params, 0)
        return text.getByShortLang(lang).replace("0", "X")