from ..actionFilters import ActionFilter

class ElemVar(ActionFilter):

    def __init__(self, action:int, requestValue:list=None, requestOperator:list=None, value:int=None, operator:str=None):
        super().__init__(action, argLen=2, requestValue=requestValue, requestOperator=requestOperator, value=value, operator=operator)

class MonoElem(ElemVar):

    def __init__(self, action:int, requestValue:int=None, requestOperator:str=None):
        super().__init__(action, requestValue=[requestValue, 1], requestOperator=[requestOperator, "="], value=requestValue, operator=requestOperator)

class BiElem(ElemVar):

    def __init__(self, action:int, requestValue:int=None, requestOperator:str=None):
        super().__init__(action, requestValue=[requestValue, 2], requestOperator=[requestOperator, "="], value=requestValue, operator=requestOperator)

class TriElem(ElemVar):

    def __init__(self, action:int, requestValue:int=None, requestOperator:str=None):
        super().__init__(action, requestValue=[requestValue, 3], requestOperator=[requestOperator, "="], value=requestValue, operator=requestOperator)

class MonoElemMastery(MonoElem):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(1068, value, operator)

class BiElemMastery(BiElem):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(1068, value, operator)

class TriElemMastery(TriElem):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(1068, value, operator)

class MonoElemResitance(MonoElem):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(1069, value, operator)

class BiElemResitance(BiElem):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(1069, value, operator)

class TriElemResitance(TriElem):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(1069, value, operator)
