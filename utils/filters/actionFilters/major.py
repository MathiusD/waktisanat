from ..actionFilters import ActionFilter

class Pa(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(31, requestValue=[value], requestOperator=[operator])

class PaDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(56, requestValue=[value], requestOperator=[operator])

class Pm(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(41, requestValue=[value], requestOperator=[operator])

class PmDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(57, requestValue=[value], requestOperator=[operator])

class Po(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(160, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class PoDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(161, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class Controle(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(184, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class Pw(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(191, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class PwDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(194, requestValue=[value], requestOperator=[operator], value=value, operator=operator)
