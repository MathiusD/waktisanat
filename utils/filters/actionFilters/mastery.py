from ..actionFilters import ActionFilter

class HealingMastering(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(26, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class ElementalMastering(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(120, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class FireMastering(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(122, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class EarthMastering(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(123, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class WaterMastering(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(124, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class AirMastering(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(125, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class ElementalMasteringDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(130, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class FireMasteringDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(132, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class CriticalMastering(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(149, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class RearMastering(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(180, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class RearMasteringDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(181, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class AreaMastering(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(1050, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class SingleTargetMastering(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(1051, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class MeleeMastering(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(1052, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class DistanceMastering(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(1053, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class BerserkMastering(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(1055, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class CriticalMasteringDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(1056, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class DistanceMasteringDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(1060, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class BerserkMasteringDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(1061, requestValue=[value], requestOperator=[operator], value=value, operator=operator)
