from ..actionFilters import ActionFilter

class CriticalHit(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(150, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class CriticalHitDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(168, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class WillPower(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(177, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class Block(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(875, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class BlockDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(876, requestValue=[value], requestOperator=[operator], value=value, operator=operator)
