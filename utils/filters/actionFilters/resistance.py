from ..actionFilters import ActionFilter

class RearResistance(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(71, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class ElementalResistance(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(80, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class FireResistance(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(82, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class EarthResistance(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(84, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class WaterResistance(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(83, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class AirResistance(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(85, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class FireResistanceDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(97, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class EarthResistanceDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(96, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class WaterResistanceDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(98, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class ElementalResistanceDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(100, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class CriticalResistance(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(988, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class CriticalResistanceDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(1063, requestValue=[value], requestOperator=[operator], value=value, operator=operator)
