from ..actionFilters import ActionFilter

class Initiative(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(171, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class InitiativeDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(172, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class Lock(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(173, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class LockDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(174, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class Dodge(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(175, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class DodgeDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(176, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class Hp(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(20, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class HpDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(21, requestValue=[value], requestOperator=[operator], value=value, operator=operator)