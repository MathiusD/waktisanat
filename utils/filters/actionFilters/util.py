from ..actionFilters import ActionFilter

class Prospecting(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(162, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class Wisdom(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(166, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class WisdomDown(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(167, requestValue=[value], requestOperator=[operator], value=value, operator=operator)

class KitSkill(ActionFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(234, requestValue=[value], requestOperator=[operator], value=value, operator=operator)
