from cdnwakfu import Text, ComparatorStr, Criteria
from own import own_discord
from discord.ext import commands
from ..filters import Filter
import abc


class CompareFilter(Filter):

    def __init__(self, criteria: Criteria = None, value: int = None, operator: str = None):
        self.operator = ComparatorStr(operator).operator
        self.value = value
        super().__init__(criteria)

    @abc.abstractmethod
    def showExemple(self, lang: str):
        return ""

    def createRow(self, lang: str):
        temp = self.showExemple(lang)
        tem = self.operator if self.operator else ""
        tem = "%s %i" % (tem, self.value) if self.value else None
        return temp.replace("X", tem) if tem else temp

    async def waitAttr(self, ctx: commands.Context, lang: str):
        bypass = Text({
            "fr": "Aucune",
            "en": "None",
            "es": "Nada",
            "pt": "Nada"
        })
        error_msg = Text({
            "fr": "Entrer un nombre (`%s` pour ne pas en spécifier)" % bypass.fr,
            "en": "Please enter number (`%s` for not specify)" % bypass.en,
            "es": "Por favor entre en el número (`%s` para no especifican)" % bypass.es,
            "pt": "Por favor entre no número (`%s` para não especificam)" % bypass.pt
        })
        dit = {
            "cancel": bypass.getByShortLang(lang),
            "max": 40000,
            "min": -1,
            "error_msg": error_msg.getByShortLang(lang)
        }
        value = Text({
            "fr": "Entrez la Valeur Recherchée ci-dessous : (`%s` pour ne pas en spécifier)" % bypass.fr,
            "en": "Enter Search Value here : (`%s` for not specify)" % bypass.en,
            "es": "Entre en el Valor de Búsqueda aquí : (`%s` para no especifican)" % bypass.es,
            "pt": "Entre no Valor de Pesquisa aqui : (`%s` para não especificam)" % bypass.pt
        })
        await own_discord.reponse_send(ctx, value.getByShortLang(lang))
        val = await own_discord.wait_input(ctx, own_discord.int_check, dit, 120.0)
        await ctx.typing()
        if not val or val == -1:
            return self
        else:
            operate = Text({
                "fr": "Entrez l'opérateur de recherche ci-dessous : (`%s` pour ne pas en spécifier)" % bypass.fr,
                "en": "Enter Search Operator here : (`%s` for not specify)" % bypass.en,
                "es": "Entre en el Operador de Búsqueda aquí: (`%s` para no especifican)" % bypass.es,
                "pt": "Entre no Operador de Pesquisa aqui : (`%s` para não especificam)" % bypass.pt
            })
            await own_discord.reponse_send(ctx, operate.getByShortLang(lang))
            ope = await own_discord.wait_input(ctx, own_discord.str_check, dit, 120.0)
            await ctx.typing()
            return self.__class__(value=val) if not ope or ope == -1 else self.__class__(value=val, operator=ope)
