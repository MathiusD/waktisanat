from data.wakdata import latest
from discord.ext import commands
from ..filters import Filter
from ..equipment_item_type import EquipementItemType
import cdnwakfu

class EquipmentItemTypeFilter(Filter):

    def __init__(self, value:cdnwakfu.EquipementItemType=None):
        self.value = value
        super().__init__(cdnwakfu.EquipementItemTypeCriteria(value.definition.id if value else None))

    def createRow(self, lang:str):
        row = cdnwakfu.Text({
            "fr":"Type d'Equipement",
            "en":"Equipment Type",
            "es":"Tipo de equipo",
            "pt":"Tipo de equipo"
        })
        row = row.getByShortLang(lang)
        row = "%s (%s)" % (row, self.value.title.getByShortLang(lang)) if self.value else row
        return row

    async def waitAttr(self, ctx:commands.Context, lang:str):
        types = []
        for typ in latest['equipmentItemTypes']:
            types.append(cdnwakfu.EquipementItemType(typ))
        data = await EquipementItemType.select(ctx, types, lang)
        return self.__class__(data) if data else None