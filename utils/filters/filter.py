from cdnwakfu import Criteria, Text
from own import own_discord
import constants
from discord.ext import commands
import copy
import abc


class Filter(abc.ABC):

    def __init__(self, criteria: Criteria):
        self.criteria = criteria

    @classmethod
    async def sendChoices(cls, ctx: commands.Context, filters: list, lang: str):
        msg = "%s\n%s" % (constants.select.getByShortLang(
            lang), cls.createList(filters, lang))
        return await own_discord.embed_send(ctx, own_discord.embed_create(msg))

    @abc.abstractmethod
    def createRow(self, lang: str):
        return ""

    @staticmethod
    def createList(filters: list, lang: str, withSubmit: bool = True):
        compt = 1
        msg = ""
        for filter in filters:
            msg = "%s%i - `%s`\n" % (msg, compt, filter.createRow(lang))
            compt += 1
        if withSubmit:
            msg = "%s%i - `%s`" % (msg, compt,
                                   constants.execute_search.getByShortLang(lang))
        return msg

    async def waitAttr(self, ctx: commands.Context, lang: str):
        return copy.deepcopy(self)

    @classmethod
    async def select(cls, ctx: commands.Context, filters: list, lang: str):
        if len(filters) > 0:
            if len(filters) > 1:
                await cls.sendChoices(ctx, filters, lang)
                error_msg = Text({
                    "en": "Entrer un nombre (1 à %i) [Ou %s pour annuler la recherche]" % (len(filters), constants.cancel.en),
                    "fr": "Please enter number (1 to %i) [Or %s for cancel search]" % (len(filters), constants.cancel.fr),
                    "es": "Por favor entre en el número (1 a %i) [O %s para anulan la búsqueda]" % (len(filters), constants.cancel.es),
                    "pt": "Por favor entre no número (1 a %i) [Ou %s para cancelam a pesquisa]" % (len(filters), constants.cancel.pt)
                })
                dit = {
                    "cancel": constants.cancel.getByShortLang(lang),
                    "max": len(filters) + 2,
                    "min": 0,
                    "error_msg": error_msg.getByShortLang(lang)
                }
                value = await own_discord.wait_input(ctx, own_discord.int_check, dit, (len(own_discord.embed_create(cls.createList(filters, lang))) * 60.0))
                await ctx.typing()
                if not value or value == -1:
                    await own_discord.reponse_send(ctx, constants.canceled.getByShortLang(lang))
                else:
                    if value > len(filters):
                        return True
                    return await filters[value - 1].waitAttr(ctx, lang)
            else:
                return await filters[0].waitAttr(ctx, lang)
        else:
            await own_discord.reponse_send(ctx, constants.not_found.getByShortLang(lang))
        return None
