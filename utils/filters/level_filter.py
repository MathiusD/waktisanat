from cdnwakfu import ComparatorStr, LevelCriteria
import constants
from ..filters import CompareFilter

class LevelFilter(CompareFilter):

    def __init__(self, value:int=None, operator:str=None):
        super().__init__(LevelCriteria(value, ComparatorStr(operator)), value, operator)

    def showExemple(self, lang:str):
        return "%s X" % constants.lvl.getByShortLang(lang)