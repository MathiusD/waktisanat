from cdnwakfu import Item
from data.wakdata import latest

class Search:

    def __init__(self, filters:list = []):
        self.criterias = []
        for filter in filters:
            self.criterias.append(filter.criteria)

    def search(self, GameData:dict=latest):
        return Item.findItemsWithCriterias(GameData, self.criterias)

    @staticmethod
    def searchWithSearchs(searchs:list, GameData:dict=latest):
        criterias = []
        for search in searchs:
            for criteria in search.criterias:
                criterias.append(criteria)
        Search(criterias).search(GameData)