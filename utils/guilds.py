from utils.interact import Interact
from discord.ext import commands
from own.own_sqlite.models import ModelData
from own.own_sqlite.fields import Field
from own import own_discord
import constants
from cdnwakfu import Text


class Guild(Interact):

    @staticmethod
    async def createMsg(ctx: commands.Context, guild: ModelData, lang: str):
        g = ctx.bot.get_guild(guild.Guild)
        if g:
            msg = g.name
            if guild.lang:
                msg = "%s\n\tLang : `%s`" % (msg, guild.lang)
            if guild.prefix:
                msg = "%s\n\tPrefix : `%s`" % (msg, guild.prefix)
            if guild.notify_chan:
                channel = g.get_channel(guild.notify_chan)
                msg = "%s\n\tWith NotifyChan [In `%s`]" % (msg, channel.name)
            msg = "%s\n\tMax Members : %i" % (msg, g.member_count)
            link = await own_discord.inviteFromGuild(g, ctx.bot, "For Debug %s" % (ctx.bot.user.name))
            if link:
                msg = "%s\n\tInvitation Link : %s" % (msg, link.url)
            msg = "%s\n\tId:%i" % (msg, g.id)
            embed = own_discord.embed_create(msg, g.name, g.icon)
            return embed
        return None

    @staticmethod
    def createList(ctx: commands.Context, guilds: list, lang: str):
        msg = ""
        compt = 1
        for guild in guilds:
            g = ctx.bot.get_guild(guild.Guild)
            if g:
                msg = "%s%i - %s\n" % (msg, compt, g.name)
            compt += 1
        return msg

    @classmethod
    async def sendChoices(cls, ctx: commands.Context, guilds: list, lang: str):
        msg = "%s\n%s" % (constants.select.getByShortLang(
            lang), cls.createList(ctx, guilds, lang))
        return await own_discord.embed_send(ctx, own_discord.embed_create(msg))

    @classmethod
    async def send(cls, ctx: commands.Context, guild: ModelData, lang: str):
        embd = await cls.createMsg(ctx, guild, lang)
        if embd:
            return await own_discord.embed_send(ctx, embd)

    @staticmethod
    def checkGuilds(ctx: commands.Context, guilds: list):
        nguilds = []
        nguildsid = []
        for guild in guilds:
            g = ctx.bot.get_guild(guild.Guild)
            if g:
                nguilds.append(guild)
                nguildsid.append(guild.Guild)
        for guild in ctx.bot.guilds:
            if guild.id not in nguildsid:
                nguilds.append(
                    ModelData(
                        [
                            Field("id", int, "INT"),
                            Field("Guild", int, "INT"),
                            Field("lang", str, "TEXT"),
                            Field("prefix", str, "TEXT"),
                            Field("notify_chan", int, "INT")
                        ],
                        [
                            None,
                            guild.id,
                            None,
                            None,
                            None
                        ]
                    )
                )
        return nguilds

    @classmethod
    async def select(cls, ctx: commands.Context, guilds: list, lang: str):
        guilds = cls.checkGuilds(ctx, guilds)
        if len(guilds) > 0:
            if len(guilds) > 1:
                await cls.sendChoices(ctx, guilds, lang)
                error_msg = Text({
                    "fr": "Entrer un nombre (1 à %i) [Ou %s pour annuler la recherche]" % (len(guilds), constants.cancel.fr),
                    "en": "Please enter number (1 to %i) [Or %s for cancel search]" % (len(guilds), constants.cancel.en),
                    "es": "Por favor entre en el número (1 a %i) [O %s para anulan la búsqueda]" % (len(guilds), constants.cancel.es),
                    "pt": "Por favor entre no número (1 a %i) [Ou %s para cancelam a pesquisa]" % (len(guilds), constants.cancel.pt)
                })
                dit = {
                    "cancel": constants.cancel.getByShortLang(lang),
                    "max": len(guilds) + 1,
                    "min": 0,
                    "error_msg": error_msg.getByShortLang(lang)
                }
                value = await own_discord.wait_input(ctx, own_discord.int_check, dit, (len(own_discord.embed_create(cls.createList(ctx, guilds, lang))) * 60.0))
                await ctx.typing()
                if not value or value == -1:
                    await own_discord.reponse_send(ctx, constants.canceled.getByShortLang(lang))
                else:
                    return guilds[value - 1]
            else:
                return guilds[0]
        else:
            await own_discord.reponse_send(ctx, constants.not_found.getByShortLang(lang))
        return None
