from discord.ext import commands
import constants
from config import settings

class Help:

    @staticmethod
    def nbCmdNotHiddenInCog(cog:commands.Cog):
        compt = 0
        for cmd in cog.get_commands():
            if cmd.hidden is False:
                compt += 1
        return compt

    @staticmethod
    def startField(cmd:commands.Command, prefix:str = ""):
        msg = "%s%s" % (prefix, cmd.name)
        if len(cmd.aliases) > 0:
            msg = "%s [" % (msg)
        for aliase in cmd.aliases:
            if cmd.aliases.index(aliase) > 0:
                msg = "%s, " % msg
            msg = "%s%s" % (msg, aliase)
        if len(cmd.aliases) > 0:
            msg = "%s]" % (msg)
        return msg

    @staticmethod
    def getCmdDesc(cmd:commands.Command, lang:str, user_prefix:str = settings.DISCORD_PREFIX):
        if cmd.name in constants.discord_desc(user_prefix).keys():
            lang = lang if constants.discord_desc(user_prefix)[cmd.name].getByShortLang(lang) != "" else settings.DISCORD_LANG
            msg = " : %s" % (constants.discord_desc(user_prefix)[cmd.name].getByShortLang(lang))
        else:
            msg = " : %s" % (constants.discord_desc(user_prefix)["default"].getByShortLang(lang))
        return msg

    @classmethod
    def createField(cls, cmd:commands.Command, lang:str, prefix:str = "", user_prefix:str = settings.DISCORD_PREFIX):
        msg = cls.startField(cmd, prefix)
        msg = "%s%s" % (msg, cls.getCmdDesc(cmd, lang, user_prefix).split('\n')[0])
        return msg

    @classmethod
    def showCmd(cls, cmd:commands.Command, lang:str, prefix:str = "", user_prefix:str = settings.DISCORD_PREFIX):
        return "%s%s" % (cls.startField(cmd, prefix), cls.getCmdDesc(cmd, lang, user_prefix))

    @classmethod
    def createCog(cls, cog:commands.Cog, lang:str, prefix = "", user_prefix:str = settings.DISCORD_PREFIX):
        msg = "%s%s:\n" % (prefix, cog.own_name.getByShortLang(lang))
        for cmd in cog.get_commands():
            if cmd.hidden is False:
                msg = "%s%s\n" % (msg, cls.createField(cmd, lang, "%s\t" % prefix, user_prefix=user_prefix))
        return msg if cls.nbCmdNotHiddenInCog(cog) > 0 else None

    @classmethod
    def globalHelp(cls, bot:commands.AutoShardedBot, lang:str, user_prefix:str = settings.DISCORD_PREFIX):
        msg = "%s\n\n" % constants.discord_desc(user_prefix)["global"].getByShortLang(lang)
        for cog in bot.cogs:
            cog_text = cls.createCog(bot.get_cog(cog), lang, user_prefix=user_prefix)
            if cog_text:
                msg = "%s%s\n" % (msg, cog_text)
        msg = "%s%s:\n" % (msg, constants.discord_desc(user_prefix)["no_cat"].getByShortLang(lang))
        for cmd in bot.commands:
            if not cmd.cog and cmd.hidden is False:
                msg = "%s%s\n" % (msg, cls.createField(cmd, lang, "\t", user_prefix=user_prefix))
        return msg

    @staticmethod
    def extractCog(bot:commands.AutoShardedBot, name:str, lang:str):
        for cog in bot.cogs:
            if bot.get_cog(cog).own_name.getByShortLang(lang).lower().strip() == name.lower().strip():
                return bot.get_cog(cog)
        return None

    @staticmethod
    def extractCmd(bot:commands.AutoShardedBot, name:str):
        name = name.lower().strip()
        for cmd in bot.commands:
            if name == cmd.name.lower().strip():
                return cmd
            for aliase in cmd.aliases:
                if name == aliase.lower().strip():
                    return cmd
        return None

