from own import own_discord
from discord.ext import commands
from cdnwakfu import Text
import constants
import abc


class Interact:

    @classmethod
    async def send(cls, ctx: commands.Context, obj: object, lang: str):
        embd = cls.createMsg(obj, lang)
        if embd:
            return await own_discord.embed_send(ctx, embd)

    @staticmethod
    @abc.abstractmethod
    def createMsg(obj: object, lang: str):
        pass

    @classmethod
    async def sendChoices(cls, ctx: commands.Context, objs: list, lang: str):
        msg = "%s\n%s" % (constants.select.getByShortLang(
            lang), cls.createList(objs, lang))
        return await own_discord.embed_send(ctx, own_discord.embed_create(msg))

    @staticmethod
    @abc.abstractmethod
    def createList(objs: list, lang: str):
        pass

    @classmethod
    async def select(cls, ctx: commands.Context, objs: list, lang: str):
        if len(objs) > 0:
            if len(objs) > 1:
                await cls.sendChoices(ctx, objs, lang)
                error_msg = Text({
                    "fr": "Entrer un nombre (1 à %i) [Ou %s pour annuler la recherche]" % (len(objs), constants.cancel.fr),
                    "en": "Please enter number (1 to %i) [Or %s for cancel search]" % (len(objs), constants.cancel.en),
                    "es": "Por favor entre en el número (1 a %i) [O %s para anulan la búsqueda]" % (len(objs), constants.cancel.es),
                    "pt": "Por favor entre no número (1 a %i) [Ou %s para cancelam a pesquisa]" % (len(objs), constants.cancel.pt)
                })
                dit = {
                    "cancel": constants.cancel.getByShortLang(lang),
                    "max": len(objs) + 1,
                    "min": 0,
                    "error_msg": error_msg.getByShortLang(lang)
                }
                value = await own_discord.wait_input(ctx, own_discord.int_check, dit, (len(own_discord.embed_create(cls.createList(objs, lang))) * 60.0))
                await ctx.typing()
                if not value or value == -1:
                    await own_discord.reponse_send(ctx, constants.canceled.getByShortLang(lang))
                else:
                    return objs[value - 1]
            else:
                return objs[0]
        else:
            await own_discord.reponse_send(ctx, constants.not_found.getByShortLang(lang))
        return None

    @classmethod
    async def display(cls, ctx: commands.Context, data: list, lang: str):
        data = await cls.select(ctx, data, lang)
        if data:
            return await cls.send(ctx, data, lang)
        return None
