from discord.ext import commands
from own import own_discord, own_gitlab
from config import settings
from utils import Interact, Repo, default_repo, User, repos
from cdnwakfu import Text
import constants
from own.own_sqlite.models import ModelData
import models
import discord
import datetime
import copy
import asyncio


class Issue(Interact):

    @staticmethod
    async def WaitingIssue(ctx: commands.Context, lang: str):
        dit = {
            "cancel": constants.cancel.getByShortLang(lang)
        }
        title = Text({
            "fr": "Entrez le Titre ci-dessous :",
            "en": "Enter Title here :",
            "es": "Entrar en el Título aquí: ",
            "pt": "Entrar no Título aqui: "
        })
        await own_discord.reponse_send(ctx, title.getByShortLang(lang))
        title = await own_discord.wait_input(ctx, own_discord.str_check, dit, 120.0)
        await ctx.typing()
        if not title or title == -1:
            await own_discord.reponse_send(ctx, constants.canceled.getByShortLang(lang))
            return None
        desc = Text({
            "fr": "Entrez la Description ci-dessous :",
            "en": "Enter Description here :",
            "es": "Entrar en la Descripción aquí: ",
            "pt": "Entrar na Descrição aqui:"
        })
        await own_discord.reponse_send(ctx, desc.getByShortLang(lang))
        desc = await own_discord.wait_input(ctx, own_discord.str_check, dit, 300.0)
        await ctx.typing()
        if not desc or desc == -1:
            await own_discord.reponse_send(ctx, constants.canceled.getByShortLang(lang))
            return None
        by = Text({
            "fr": "Par",
            "en": "By",
            "es": "Por",
            "pt": "De"
        })
        return {"title": title, "desc": "%s\n - %s %s#%s" % (desc, by.getByShortLang(lang), ctx.author.name, ctx.author.discriminator)}

    @staticmethod
    async def WaitingBody(ctx: commands.Context, lang: str):
        dit = {
            "cancel": constants.cancel.getByShortLang(lang)
        }
        desc = Text({
            "fr": "Entrez le message ci-dessous :",
            "en": "Enter Message here :",
            "es": "Entrar el mensaje aquí: ",
            "pt": "Entrar na mensagem aqui:"
        })
        await own_discord.reponse_send(ctx, desc.getByShortLang(lang))
        desc = await own_discord.wait_input(ctx, own_discord.str_check, dit, 300.0)
        await ctx.typing()
        if not desc or desc == -1:
            await own_discord.reponse_send(ctx, constants.canceled.getByShortLang(lang))
            return None
        by = Text({
            "fr": "Par",
            "en": "By",
            "es": "Por",
            "pt": "De"
        })
        return "%s\n - %s %s#%s" % (desc, by.getByShortLang(lang), ctx.author.name, ctx.author.discriminator)

    @classmethod
    async def SubmitIssue(cls, ctx: commands.Context, labels: list, lang: str, repo: Repo):
        data = await cls.WaitingIssue(ctx, lang)
        if data:
            dat = own_gitlab.Issue.submitIssue(
                repo.protocol, repo.provider, repo.author, repo.repo, repo.token, data["title"], data["desc"], labels)
            if dat:
                msg = constants.issue_submit
            else:
                msg = constants.issue_not_submit
            await own_discord.reponse_send(ctx, msg.getByShortLang(lang))
            return dat
        return None

    @classmethod
    async def SubmitNote(cls, ctx: commands.Context, iid: int, lang: str, repo: Repo, notes: models.NoteIssue):
        data = await cls.WaitingBody(ctx, lang)
        if data:
            dat = own_gitlab.Note.submitNote(
                repo.protocol, repo.provider, repo.author, repo.repo, iid, repo.token, data)
            if dat:
                notes.addOwnerOfNote(ctx.author.id, iid, repo.repo, dat["id"])
                msg = constants.issue_submit
            else:
                msg = constants.issue_not_submit
            await own_discord.reponse_send(ctx, msg.getByShortLang(lang))
            return dat
        return None

    @staticmethod
    def FetchIssue(args=[], repo: Repo = default_repo):
        return own_gitlab.Issue.fetchIssues(repo.protocol, repo.provider, repo.author, repo.repo, repo.token, args)

    @staticmethod
    def createMsg(issue: dict, lang: str):
        msg = "%s : %i\n" % (
            constants.issue.getByShortLang(lang), issue["iid"])
        msg = "%s%s : %s\n" % (
            msg, constants.desc.getByShortLang(lang), issue["description"])
        msg = "%s%s : %s\n" % (
            msg, constants.state.getByShortLang(lang), issue["state"])
        msg = "%s%s : %s\n" % (
            msg, constants.submit.getByShortLang(lang), issue["created_at"])
        if issue["state"] == "closed":
            msg = "%s%s : %s\n" % (
                msg, constants.closed.getByShortLang(lang), issue["closed_at"])
        msg = "%s%s : %s\n" % (
            msg, constants.link.getByShortLang(lang), issue["web_url"])
        if len(issue["notes"]) > 0:
            msg = "%s%s :\n" % (msg, constants.comments.getByShortLang(lang))
            for note in issue["notes"]:
                msg = "%s\t%s : %s\n" % (
                    msg, note["author"]["username"], note["body"])
        return own_discord.embed_create(msg, issue["title"])

    @staticmethod
    def createList(issues: list, lang: str):
        compt = 1
        msg = ""
        for issue in issues:
            msg = "%s%i - %s" % (msg, compt, issue["title"])
            if len(issue["labels"]) > 1:
                msg = "%s[" % msg
                for label in issue["labels"]:
                    msg = "%s%s, " % (msg, label)
                msg = "%s]" % msg[:-2]
            msg = "%s (%s : %s)\n" % (
                msg, constants.state.getByShortLang(lang), issue["state"])
            compt += 1
        return msg

    @staticmethod
    def getRepoOfIssue(issue: dict):
        repoStr = issue["web_url"].split("/")[4]
        repo = Repo.foundRepoObjectOfTarget(repoStr, repos)
        return repo

    @classmethod
    async def send(cls, ctx: commands.Context, issue: dict, lang: str, modIssues: models.Issue):
        msg = await super().send(ctx, issue, lang)
        if msg:
            repo = cls.getRepoOfIssue(issue)
            asyncio.get_event_loop().create_task(cls.addNote(
                ctx, msg, issue["iid"], repo, modIssues, lang))
            asyncio.get_event_loop().create_task(
                cls.edit(ctx, msg, issue["iid"], repo, modIssues, lang))
            asyncio.get_event_loop().create_task(cls.remove(
                ctx, msg, issue["iid"], repo, modIssues, lang))
        return msg

    @staticmethod
    def checkIsOwner(ctx: commands.Context, iid: int, repo: Repo, modIssues: models.Issue):
        issue = modIssues.fetchOwnerOf(iid, repo.repo)
        if issue:
            user = modIssues.users.fetchUserById(issue.owner)
            if user and user.discord_id == ctx.author.id:
                return True
        return False

    @classmethod
    async def edit(cls, ctx: commands.Context, msg: discord.Message, iid: int, repo: Repo, modIssues: models.Issue, lang: str):
        try:
            if cls.checkIsOwner(ctx, iid, repo, modIssues):
                if await own_discord.wait_react(ctx, msg, "📝") is True:
                    data = await cls.WaitingIssue(ctx, lang)
                    if data:
                        if own_gitlab.Issue.editIssue(repo.protocol, repo.provider, repo.author, repo.repo, repo.token, iid, data["title"], data['desc']):
                            await own_discord.reponse_send(ctx, constants.issue_updated.getByShortLang(lang))
                        else:
                            await own_discord.reponse_send(ctx, constants.error_occured.getByShortLang(lang))
        except Exception as exc:
            await ctx.bot.on_command_error(ctx, exc)

    @classmethod
    async def remove(cls, ctx: commands.Context, msg: discord.Message, iid: int, repo: Repo, modIssues: models.Issue, lang: str):
        try:
            if cls.checkIsOwner(ctx, iid, repo, modIssues):
                issue = modIssues.fetchOwnerOf(iid, repo.repo)
                if await own_discord.wait_react(ctx, msg, "❌") is True:
                    if own_gitlab.Issue.removeIssue(repo.protocol, repo.provider, repo.author, repo.repo, repo.token, iid) and modIssues.removeIssue(issue.id):
                        await own_discord.reponse_send(ctx, constants.issue_removed.getByShortLang(lang))
                    else:
                        await own_discord.reponse_send(ctx, constants.error_occured.getByShortLang(lang))
        except Exception as exc:
            await ctx.bot.on_command_error(ctx, exc)

    @classmethod
    async def addNote(cls, ctx: commands.Context, msg: discord.Message, iid: int, repo: Repo, modIssues: models.Issue, lang: str):
        try:
            if await own_discord.wait_react(ctx, msg, "💬") is True:
                await cls.SubmitNote(ctx, iid, lang, repo, modIssues.notes)
        except Exception as exc:
            await ctx.bot.on_command_error(ctx, exc)

    @classmethod
    async def display(cls, ctx: commands.Context, issues: list, lang: str, modIssues: models.Issue):
        issue = await cls.select(ctx, issues, lang)
        if issue:
            return await cls.send(ctx, issue, lang, modIssues)

    @classmethod
    def FetchIssueByModelData(cls, modelIssue: ModelData, args: list = []):
        issue = None
        if modelIssue:
            repo = Repo.foundRepoObjectOfTarget(modelIssue.repo, repos)
            newArgs = copy.deepcopy(args)
            newArgs.append(
                {
                    "key": "iids[]",
                    "value": modelIssue.iid
                }
            )
            issue = cls.FetchIssue(newArgs, repo)
            issue = issue[0] if issue is not None and len(issue) == 1 else None
        return issue

    @classmethod
    def FetchIssuesByModelData(cls, modelIssues: list, args: list = []):
        issues = []
        for modelIssue in modelIssues:
            issue = cls.FetchIssueByModelData(modelIssue, args)
            if issue:
                issues.append(issue)
        return issues

    @classmethod
    def issuesAreUpdatedAfter(cls, modelIssues: list, time: dict):
        issues = []
        for modelIssue in modelIssues:
            issue = cls.issueAreUpdatedAfter(modelIssue, time)
            if issue:
                issues.append(issue)
        return issues

    @classmethod
    def issueAreUpdatedAfter(cls, modelIssue: ModelData, time: dict):
        issue = None
        if modelIssue:
            args = [
                {
                    "key": "updated_after",
                    "value": datetime.datetime(time["year"], time["month"], time["day"], time["hour"], time["minute"], time["second"])
                }
            ]
            issue = cls.FetchIssueByModelData(modelIssue, args)
        return issue

    @classmethod
    async def sendIssueUpdate(cls, bot: commands.AutoShardedBot, issue: dict, issues: models.Issue):
        repo = cls.getRepoOfIssue(issue)
        rawUsers = []
        modelIssue = issues.fetchOwnerOf(issue["iid"], repo.repo)
        if modelIssue:
            rawUsers.append(modelIssue.owner)
        notes = issues.notes.fetchNoteOfIssue(repo.repo, issue["iid"])
        for note in notes:
            if note.owner not in rawUsers:
                rawUsers.append(note.owner)
        for rawUser in rawUsers:
            user = issues.users.fetchUserById(rawUser)
            if user:
                data = {
                    "issue": issue,
                    "issues": issues
                }

                async def sending(ctx: commands.Context, lang: str, data: dict = {}):
                    await own_discord.reponse_send(ctx, constants.issue_updated.getByShortLang(lang))
                    await cls.send(ctx, data["issue"], lang, data["issues"])
                await User.execCoroInUserDM(bot, user.discord_id, issues.users, sending, data)
        return None
