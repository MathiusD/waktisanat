from data import wakdata
from own import own_discord
from utils import Interact, Server, Notify
import constants
from discord.ext import commands
from models import ItemPrice, Price
import cdnwakfu
import asyncio
import discord
import datetime
import time


class Item(Interact):

    @classmethod
    async def display(cls, ctx: commands.Context, items: list, lang: str, prices: ItemPrice, with_react: bool = True):
        data = await cls.select(ctx, items, lang)
        if data:
            return await cls.send(ctx, data, lang, prices, with_react)
        return None

    @classmethod
    async def select(cls, ctx: commands.Context, items: list, lang: str):
        itemsWithTitle = []
        for item in items:
            if item.title:
                itemsWithTitle.append(item)
        if len(itemsWithTitle) > 0:
            if len(itemsWithTitle) > 1:
                await cls.sendChoices(ctx, itemsWithTitle, lang)
                error_msg = cdnwakfu.Text({
                    "fr": "Entrer un nombre (1 à %i) [Ou %s pour annuler la recherche]" % (len(itemsWithTitle), constants.cancel.fr),
                    "en": "Please enter number (1 to %i) [Or %s for cancel search]" % (len(itemsWithTitle), constants.cancel.en),
                    "es": "Por favor entre en el número (1 a %i) [O %s para anulan la búsqueda]" % (len(itemsWithTitle), constants.cancel.es),
                    "pt": "Por favor entre no número (1 a %i) [Ou %s para cancelam a pesquisa]" % (len(itemsWithTitle), constants.cancel.pt)
                })
                dit = {
                    "cancel": constants.cancel.getByShortLang(lang),
                    "max": len(itemsWithTitle) + 1,
                    "min": 0,
                    "error_msg": error_msg.getByShortLang(lang)
                }
                value = await own_discord.wait_input(ctx, own_discord.int_check, dit, (len(own_discord.embed_create(cls.createList(itemsWithTitle, lang))) * 60.0))
                await ctx.typing()
                if not value or value == -1:
                    await own_discord.reponse_send(ctx, constants.canceled.getByShortLang(lang))
                else:
                    return itemsWithTitle[value - 1]
            else:
                return itemsWithTitle[0]
        else:
            await own_discord.reponse_send(ctx, "%s (%s)" % (constants.not_found.getByShortLang(lang), constants.lang_used.getByShortLang(lang)))
        return None

    @classmethod
    async def send(cls, ctx: commands.Context, item: cdnwakfu.BaseItem, lang: str, prices: ItemPrice, with_react: bool = True, option_msg: str = None, recipes: list = []):
        embd = cls.createMsg(item, lang, prices, option=option_msg)
        if embd:
            msg = await own_discord.embed_send(ctx, embd)
            if msg and not isinstance(msg, list):
                if with_react:
                    await cls.taskManager(ctx, item, msg, lang, prices, option=option_msg, recipes=recipes)
                    asyncio.get_event_loop().create_task(
                        cls.price(ctx, msg, item, lang, prices))
            else:
                await own_discord.reponse_send(ctx, constants.missing_react_partial_item.getByShortLang(lang))
            return msg

    @classmethod
    async def taskManager(cls, ctx: commands.Context, item: cdnwakfu.BaseItem, msg: discord.Message, lang: str, prices: ItemPrice, with_stats: bool = False, with_craft: bool = False, with_price: bool = False, other_tasks: list = [], recipes: list = None, option: str = None):
        for task in other_tasks:
            task.cancel()
        craftTasks = []
        statsTasks = []
        buyTasks = []
        if not recipes:
            recipes = await cdnwakfu.Recipe.asyncFindRecipeByItemResult(wakdata.latest, item)
        if isinstance(item, cdnwakfu.Item):
            stats = asyncio.get_event_loop().create_task(cls.updateStats(
                ctx, item, msg, lang, prices, with_stats, with_craft, with_price, statsTasks, recipes, option))
            craftTasks.append(stats)
            buyTasks.append(stats)
        if len(recipes) > 0:
            craft = asyncio.get_event_loop().create_task(cls.updateCraft(
                ctx, item, msg, lang, prices, with_stats, with_craft, with_price, craftTasks, recipes, option))
            statsTasks.append(craft)
            buyTasks.append(craft)
        buy = asyncio.get_event_loop().create_task(cls.updateBuy(
            ctx, item, msg, lang, prices, with_stats, with_craft, with_price, craftTasks, recipes, option))
        statsTasks.append(buy)
        craftTasks.append(buy)

    @classmethod
    async def updateMsg(cls, react: str, initialState: bool, ctx: commands.Context, item: cdnwakfu.BaseItem, msg: discord.Message, lang: str, prices: ItemPrice, with_stats: bool = False, with_craft: bool = False, with_price: bool = False, other_tasks: list = [], recipes: list = None, option: str = None):
        try:
            if initialState is False:
                result = await own_discord.wait_react(ctx, msg, react)
            else:
                result = await own_discord.wait_react_remove(ctx, msg, react)
            if result is True:
                embd = cls.createMsg(item, lang, prices, with_recipe=with_craft, with_stats=with_stats,
                                     recipe=recipes if with_craft else None, option=option, with_price=with_price)
                if embd:
                    await own_discord.embed_update(ctx, msg, embd)
                    await cls.taskManager(ctx, item, msg, lang, prices, with_stats, with_craft, with_price, other_tasks, recipes, option)
        except Exception as exc:
            await ctx.bot.on_command_error(ctx, exc)

    @classmethod
    async def updateBuy(cls, ctx: commands.Context, item: cdnwakfu.BaseItem, msg: discord.Message, lang: str, prices: ItemPrice, with_stats: bool = False, with_craft: bool = False, with_price: bool = False, other_tasks: list = [], recipes: list = None, option: str = None):
        await cls.updateMsg("💰", with_price, ctx, item, msg, lang, prices, with_stats, with_craft, not with_price, other_tasks, recipes, option)

    @classmethod
    async def updateCraft(cls, ctx: commands.Context, item: cdnwakfu.BaseItem, msg: discord.Message, lang: str, prices: ItemPrice, with_stats: bool = False, with_craft: bool = False, with_price: bool = False, other_tasks: list = [], recipes: list = None, option: str = None):
        await cls.updateMsg("🛠️", with_craft, ctx, item, msg, lang, prices, with_stats, not with_craft, with_price, other_tasks, recipes, option)

    @classmethod
    async def updateStats(cls, ctx: commands.Context, item: cdnwakfu.BaseItem, msg: discord.Message, lang: str, prices: ItemPrice, with_stats: bool = False, with_craft: bool = False, with_price: bool = False, other_tasks: list = [], recipes: list = None, option: str = None):
        await cls.updateMsg("🗡️", with_stats, ctx, item, msg, lang, prices, not with_stats, with_craft, with_price, other_tasks, recipes, option)

    @staticmethod
    def colorFromRarity(rarity: cdnwakfu.Rarity):
        if rarity:
            if cdnwakfu.Rarity.Inhabituel.value == rarity:
                return int("ffffff", 16)
            if cdnwakfu.Rarity.Commun.value == rarity:
                return int("000000", 16)
            elif cdnwakfu.Rarity.Rare.value == rarity:
                return int("00ff00", 16)
            elif cdnwakfu.Rarity.Mythique.value == rarity:
                return int("ffaa00", 16)
            elif cdnwakfu.Rarity.Légendaire.value == rarity:
                return int("ffff00", 16)
            elif cdnwakfu.Rarity.Epique.value == rarity:
                return int("ff00dd", 16)
            elif cdnwakfu.Rarity.Souvenir.value == rarity:
                return int("00d0ff", 16)
            elif cdnwakfu.Rarity.Relique.value == rarity:
                return int("aa00ff", 16)
        return None

    @classmethod
    def createMsg(cls, item: cdnwakfu.BaseItem, lang: str, prices: ItemPrice, recipe=None, option: str = None, with_recipe: bool = False, with_stats: bool = False, with_price: bool = False):
        if item.title or (item._comparedFrom or item._comparedTo):
            item.fetchData(wakdata.latest)
            out = {
                "item": item.sheet(lang, without_stats=not with_stats) if isinstance(item, cdnwakfu.Item) else item.sheet(lang),
                "title": item.title.getByShortLang(lang) if item.title else None,
                "color": None
            }
            if option:
                out["item"] = "%s%s" % (out["item"], option)
            if with_price is True:
                priceResult = prices.fetchLastPriceForAllServer(item)
                for priceData in priceResult:
                    price = priceData["price"]
                    line = "For server %s" % priceData["server"].name
                    if price:
                        line = "%s:\n\t%s" % (
                            line, constants.average_price.getByShortLang(lang))
                        line = "%s : %s\n" % (line, price.average)
                        if price.min:
                            line = "%s\t%s : %s\n" % (
                                line, constants.min_price.getByShortLang(lang), price.min)
                        if price.max:
                            line = "%s\t%s : %s\n" % (
                                line, constants.max_price.getByShortLang(lang), price.max)
                        timeDate = datetime.datetime.utcfromtimestamp(
                            price.timestamp)
                        line = "%s\t - %s" % (line, timeDate.__str__())
                        user = prices.users.fetchUserById(price.owner)
                        if user:
                            line = "%s By <@%i>" % (line, user.discord_id)
                    else:
                        line = "%s:\n\t%s" % (
                            line, constants.no_price.getByShortLang(lang))
                    out["item"] = "%s%s\n" % (out["item"], line)
            if with_recipe is True:
                if not recipe:
                    recipes = cdnwakfu.Recipe.findRecipeByItemResultId(
                        wakdata.latest, item._id)
                    if len(recipes) > 0:
                        out["item"] = "%s%s :\n" % (
                            out["item"], constants.recipes.getByShortLang(lang))
                        for recipe in recipes:
                            recipe.fetchData(wakdata.latest)
                            out["item"] = "%s%s" % (
                                out["item"], recipe.sheet(lang, "\t"))
                else:
                    out["item"] = "%s%s :\n" % (
                        out["item"], constants.recipe.getByShortLang(lang))
                    if isinstance(recipe, cdnwakfu.Recipe):
                        recipe.fetchData(wakdata.latest)
                        out["item"] = "%s%s" % (
                            out["item"], recipe.sheet(lang, "\t"))
                    elif isinstance(recipe, list):
                        for recip in recipe:
                            recip.fetchData(wakdata.latest)
                            out["item"] = "%s%s" % (
                                out["item"], recip.sheet(lang, "\t"))
            if not item._comparedFrom and not item._comparedTo:
                if isinstance(item, cdnwakfu.Item):
                    if item.definition.item.graphicParameters:
                        out["uri"] = item.definition.item.graphicParameters.vertyloUri
                    out["color"] = cls.colorFromRarity(
                        item.definition.item.baseParameters.rarity)
                else:
                    if item.definition.graphicParameters:
                        out["uri"] = item.definition.graphicParameters.vertyloUri
            out["item"] = out["item"].replace("*", "x")
            embed = own_discord.embed_create(
                out["item"], out["title"], out["uri"] if "uri" in out.keys() else None, out["color"])
            return embed
        return None

    @staticmethod
    def createList(items: list, lang: str):
        msg = ""
        compt = 1
        for item in items:
            if item.title:
                msg = "%s%i - %s" % (msg, compt,
                                     item.title.getByShortLang(lang))
                if isinstance(item, cdnwakfu.Item):
                    item.fetchData(wakdata.latest, 3)
                    msg = "%s (%s)" % (msg, cdnwakfu.Rarity.RaritytoText(
                        item.definition.item.baseParameters.rarity).getByShortLang(lang))
                    if item.definition.item.equipmentItemType:
                        msg = "%s [%s]" % (
                            msg, item.definition.item.equipmentItemType.title.getByShortLang(lang))
                msg = "%s\n" % msg
            compt += 1
        return msg

    @staticmethod
    async def getItems(name: str, lang: str):
        return await cdnwakfu.SearchItem.asyncGetItemsByName(wakdata.latest, name, lang)

    @staticmethod
    async def searchItemWithPattern(name: str, lang: str):
        items = []
        recipes = []
        raw_items = await Item.getItems(name, lang)
        for raw_item in raw_items:
            raw_recipes = cdnwakfu.Recipe.findRecipeByItemResultId(
                wakdata.latest, raw_item._id)
            if len(raw_recipes) > 0:
                recipes.append(raw_recipes)
                items.append(raw_item)
        return {"items": items, "recipes": recipes}

    @staticmethod
    async def searchItemWithLoot(name: str, lang: str):
        items = []
        raw_items = await Item.getItems(name, lang)
        for raw_item in raw_items:
            raw_item.fetchData(wakdata.latest)
            if len(raw_item.loots) > 1:
                items.append(raw_item)
        return items

    @staticmethod
    def getItemById(id: int):
        return cdnwakfu.SearchItem.getItem(wakdata.latest, id)

    @staticmethod
    async def waitingPrice(ctx: commands.Context, item: cdnwakfu.BaseItem, lang: str):
        max_price = 1000000000
        bypass = cdnwakfu.Text({
            "fr": "Aucun",
            "en": "None",
            "es": "Nada",
            "pt": "Nada"
        })
        error_msg_byPass = cdnwakfu.Text({
            "fr": "Entrer un nombre (1 à %i)[`%s` pour ne pas en spécifier]" % (max_price, bypass.fr),
            "en": "Please enter number (1 to %i)[`%s` for not specify]" % (max_price, bypass.en),
            "es": "Por favor entre en el número (1 a %i)[`%s` para no especifican]" % (max_price, bypass.es),
            "pt": "Por favor entre no número (1 a %i)[`%s` para não especificam]" % (max_price, bypass.pt)
        })
        error_msg = cdnwakfu.Text({
            "fr": "Entrer un nombre (1 à %i)[Ou %s pour annuler la recherche]" % (max_price, constants.cancel.en),
            "en": "Please enter number (1 to %i)[Or %s for cancel search]" % (max_price, constants.cancel.fr),
            "es": "Por favor entre en el número (1 a %i)[O %s para anulan la búsqueda]" % (max_price, constants.cancel.es),
            "pt": "Por favor entre no número (1 a %i)[Ou %s para cancelam a pesquisa]" % (max_price, constants.cancel.pt)
        })
        dit = {
            "cancel": constants.cancel.getByShortLang(lang),
            "max": max_price,
            "min": 0,
            "error_msg": error_msg.getByShortLang(lang)
        }
        ditByPass = {
            "cancel": bypass.getByShortLang(lang),
            "max": max_price,
            "min": 0,
            "error_msg": error_msg_byPass.getByShortLang(lang)
        }
        moy_val = cdnwakfu.Text({
            "fr": "Entrez le %s :" % constants.average_price.fr,
            "en": "Enter %s here :" % constants.average_price.en,
            "es": "Entre %s aquí :" % constants.average_price.es,
            "pt": "Entre %s aqui :" % constants.average_price.pt
        })
        min_val = cdnwakfu.Text({
            "fr": "Entrez le %s : (`%s` pour ne pas en spécifier)" % (constants.min_price.fr, bypass.fr),
            "en": "Enter %s here : (`%s` for not specify)" % (constants.min_price.en, bypass.en),
            "es": "Entre %s aquí : (`%s` para no especifican)" % (constants.min_price.es, bypass.es),
            "pt": "Entre %s aqui : (`%s` para não especificam)" % (constants.min_price.pt, bypass.pt)
        })
        max_val = cdnwakfu.Text({
            "fr": "Entrez le %s : (`%s` pour ne pas en spécifier)" % (constants.max_price.fr, bypass.fr),
            "en": "Enter %s here : (`%s` for not specify)" % (constants.max_price.en, bypass.en),
            "es": "Entre %s aquí : (`%s` para no especifican)" % (constants.max_price.es, bypass.es),
            "pt": "Entre %s aqui : (`%s` para não especificam)" % (constants.max_price.pt, bypass.pt)
        })
        await own_discord.reponse_send(ctx, moy_val.getByShortLang(lang))
        averagePrice = await own_discord.wait_input(ctx, own_discord.int_check, dit, 120.0)
        await ctx.typing()
        if not averagePrice or averagePrice == -1:
            await own_discord.reponse_send(ctx, constants.canceled.getByShortLang(lang))
            return None
        else:
            await own_discord.reponse_send(ctx, min_val.getByShortLang(lang))
            minPrice = await own_discord.wait_input(ctx, own_discord.int_check, ditByPass, 120.0)
            await ctx.typing()
            if not minPrice or minPrice == -1:
                return Price(item, averagePrice)
            else:
                await own_discord.reponse_send(ctx, max_val.getByShortLang(lang))
                maxPrice = await own_discord.wait_input(ctx, own_discord.int_check, ditByPass, 120.0)
                await ctx.typing()
                if not maxPrice or maxPrice == -1:
                    return Price(item, averagePrice, minPrice)
                else:
                    return Price(item, averagePrice, minPrice, maxPrice)

    @classmethod
    async def price(cls, ctx: commands.Context, msg: discord.Message, item: cdnwakfu.BaseItem, lang: str, prices: ItemPrice):
        try:
            user = prices.users.fetchUserByDiscordId(ctx.author.id)
            if user is None:
                user = prices.users.addUser(
                    ctx.author.name, ctx.author.id, lang, Notify.default)
            if user:
                if await own_discord.wait_react(ctx, msg, "🏷️") is True:
                    server = await Server.select(ctx, lang)
                    if server:
                        server = Server.indexOfServerElement(server)
                        price = await cls.waitingPrice(ctx, item, lang)
                        if price:
                            if prices.addPriceOfItem(ctx.author.id, server, price):
                                await own_discord.reponse_send(ctx, constants.price_submit.getByShortLang(lang))
                            else:
                                await own_discord.reponse_send(ctx, constants.error_occured.getByShortLang(lang))
        except Exception as exc:
            await ctx.bot.on_command_error(ctx, exc)
