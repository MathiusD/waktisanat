from own import own_discord
import cdnwakfu
import asyncio
import discord
import models
from data import wakdata
from utils import Item, Recipe, Avatar
from utils.requests_basic import RequestInteract, CraftRequest, ComponentRequest, BuyRequest
import constants
from discord.ext import commands


class ItemInteract(Item):

    @classmethod
    async def display(cls, ctx: commands.Context, items: list, lang: str, requests: models.Request, prices: models.ItemPrice, with_react: bool = True):
        data = await cls.select(ctx, items, lang)
        if data:
            return await cls.send(ctx, data, lang, requests, prices, with_react)
        return None

    @classmethod
    async def send(cls, ctx: commands.Context, item: cdnwakfu.BaseItem, lang: str, requests: models.Request, prices: models.ItemPrice, with_react: bool = True):
        msg = await super().send(ctx, item, lang, prices, False)
        if msg and with_react and not isinstance(msg, list):
            await cls.bindReact(ctx, msg, item, lang, requests, prices)
        return msg

    @classmethod
    async def bindReact(cls, ctx: commands.Context, msg: discord.Message, item: cdnwakfu.BaseItem, lang: str, requests: models.Request, prices: models.ItemPrice):
        recipes = await cdnwakfu.Recipe.asyncFindRecipeByItemResult(wakdata.latest, item)
        await cls.taskManager(ctx, item, msg, lang, prices, recipes=recipes)
        asyncio.get_event_loop().create_task(
            cls.call(ctx, msg, item, recipes, lang, requests))
        asyncio.get_event_loop().create_task(
            cls.price(ctx, msg, item, lang, prices))
        if isinstance(item, cdnwakfu.Item):
            asyncio.get_event_loop().create_task(
                cls.compareToItem(ctx, msg, item, lang, prices))
        if len(recipes) > 0:
            asyncio.get_event_loop().create_task(cls.showCanCraft(
                ctx, msg, recipes, requests.avatars, lang))
            asyncio.get_event_loop().create_task(
                cls.xp(ctx, msg, recipes, requests.avatars, lang))
            asyncio.get_event_loop().create_task(
                cls.components(ctx, msg, recipes, lang))

    @classmethod
    async def compareToItem(cls, ctx: commands.Context, msg: discord.Message, item: cdnwakfu.Item, lang: str, prices: models.ItemPrice):
        try:
            if await own_discord.wait_react(ctx, msg, "📏") is True:
                request = cdnwakfu.Text({
                    "fr": "Entrez le nom de l'objet à comparer ci-dessous :",
                    "en": "Enter the name of the object to be compared below :",
                    "es": "Escriba el nombre del objeto que se va a comparar a continuación :",
                    "pt": "Digitar o nome do objeto a comparar-se abaixo :"
                })
                await own_discord.reponse_send(ctx, request.getByShortLang(lang))
                dit = {
                    "cancel": constants.cancel.getByShortLang(lang)
                }
                name = await own_discord.wait_input(ctx, own_discord.str_check, dit, 120.0)
                await ctx.typing()
                if not name or name == -1:
                    await own_discord.reponse_send(ctx, constants.canceled.getByShortLang(lang))
                    return None
                items = await cls.getItems(name, lang)
                newItem = await cls.select(ctx, items, lang)
                if newItem:
                    newItem.fetchData(wakdata.latest)
                    compareItem = item.compareTo(newItem)
                    if compareItem:
                        compareRecipe = None
                        recipes = await cdnwakfu.Recipe.asyncFindRecipeByItemResult(wakdata.latest, item)
                        if len(recipes) == 1:
                            newRecipes = await cdnwakfu.Recipe.asyncFindRecipeByItemResult(wakdata.latest, newItem)
                            if len(newRecipes) == 1:
                                compareRecipe = recipes[0].compareTo(
                                    newRecipes[0])
                        embd = cls.createMsg(compareItem, lang, prices, with_stats=True,
                                             recipe=compareRecipe if compareRecipe else None, with_recipe=True if compareRecipe else False)
                        if embd:
                            msg = await own_discord.embed_send(ctx, embd)
        except Exception as exc:
            await ctx.bot.on_command_error(ctx, exc)

    @staticmethod
    async def showCanCraft(ctx: commands.Context, msg: discord.Message, recipes: list, avatars: models.Avatar, lang: str):
        try:
            if await own_discord.wait_react(ctx, msg, "📒") is True:
                await ctx.typing()
                recipe = await Recipe.select(ctx, recipes, lang)
                if recipe:
                    await Avatar.showAvatarCanCraftRecipe(ctx, recipe, avatars, lang)
        except Exception as exc:
            await ctx.bot.on_command_error(ctx, exc)

    @staticmethod
    async def call(ctx: commands.Context, msg: discord.Message, item: cdnwakfu.BaseItem, recipes: list, lang: str, requests: models.Request):
        try:
            if await own_discord.wait_react(ctx, msg, "🔎") is True:
                await ctx.typing()
                avatars = requests.avatars.fetchAvatarsByDiscordId(
                    ctx.author.id)
                if avatars:
                    avatar = await Avatar.select(ctx, avatars, lang, requests.avatars.users)
                    if avatar:
                        await ctx.typing()
                        tempReq = []
                        if len(recipes) > 0:
                            tempReq.append(CraftRequest)
                        if len(item.loots) > 0:
                            tempReq.append(ComponentRequest)
                        tempReq.append(BuyRequest)
                        requestManager = await RequestInteract().select(ctx, lang, tempReq)
                        if requestManager == CraftRequest:
                            recipe = await Recipe.select(ctx, recipes, lang)
                            if recipe:
                                asyncio.get_event_loop().create_task(
                                    requestManager.addRequest(ctx, avatar, lang, requests, recipe))
                        else:
                            asyncio.get_event_loop().create_task(
                                requestManager.addRequest(ctx, avatar, lang, requests, item))
                else:
                    await own_discord.reponse_send(ctx, constants.account_required.getByShortLang(lang))
        except Exception as exc:
            await ctx.bot.on_command_error(ctx, exc)

    @staticmethod
    async def components(ctx: commands.Context, msg: discord.Message, recipes: list, lang: str):
        try:
            if await own_discord.wait_react(ctx, msg, "📃") is True:
                recipe = await Recipe.select(ctx, recipes, lang)
                if recipe:
                    asyncio.get_event_loop().create_task(
                        Recipe.sendAllComponent(recipe, lang, ctx))
        except Exception as exc:
            await ctx.bot.on_command_error(ctx, exc)

    @staticmethod
    async def xp(ctx: commands.Context, msg: discord.Message, recipes: list, avatars: models.Avatar, lang: str):
        try:
            if await own_discord.wait_react(ctx, msg, "❕") is True:
                await ctx.typing()
                recipe = await Recipe.select(ctx, recipes, lang)
                if recipe:
                    avatar = await Avatar.extractAvatar(ctx, avatars, None, lang)
                    if avatar:
                        asyncio.get_event_loop().create_task(
                            Recipe.sendXP(recipe, avatar, lang, ctx, avatars))
                    else:
                        await own_discord.reponse_send(ctx, constants.account_required.getByShortLang(lang))
        except Exception as exc:
            await ctx.bot.on_command_error(ctx, exc)
