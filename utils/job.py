from cdnwakfu import RecipeCategorie, Text
from data import wakdata
from own import own_discord
from discord.ext import commands
import constants


class Job:

    @staticmethod
    def checkJob(job: str, lang: str):
        return RecipeCategorie.findRecipeCategoriesByName(wakdata.latest['recipeCategories'], job, lang)

    @staticmethod
    def extractJob(id: int, lang: str):
        search = RecipeCategorie.findRecipeCategorie(
            wakdata.latest['recipeCategories'], id)
        return search.title.getByShortLang(lang) if search else None

    @staticmethod
    async def sendJobs(ctx: commands.Context, lang: str):
        msg = "%s\n" % constants.job_retry.getByShortLang(lang)
        for cat in wakdata.latest['recipeCategories']:
            cate = RecipeCategorie(cat)
            msg = "%s\t%s\n" % (msg, cate.title.getByShortLang(lang))
        return await own_discord.embed_send(ctx, own_discord.embed_create(msg))

    @staticmethod
    async def sendChoices(ctx: commands.Context, lang: str):
        msg = "%s :\n" % constants.select.getByShortLang(lang)
        compt = 1
        for cat in wakdata.latest['recipeCategories']:
            cate = RecipeCategorie(cat)
            msg = "%s%i - %s\n" % (msg, compt, cate.title.getByShortLang(lang))
            compt += 1
        return await own_discord.embed_send(ctx, own_discord.embed_create(msg))

    @staticmethod
    def extractJobs():
        cats = []
        for cat in wakdata.latest['recipeCategories']:
            cats.append(RecipeCategorie(cat))
        return cats

    @classmethod
    async def selectJobs(cls, ctx: commands.Context, lang: str):
        await cls.sendChoices(ctx, lang)
        error_msg = Text({
            "en": "Entrer un nombre (1 à %i) [Ou %s pour annuler la recherche]" % (len(wakdata.latest['recipeCategories']), constants.cancel.en),
            "fr": "Please enter number (1 to %i) [Or %s for cancel search]" % (len(wakdata.latest['recipeCategories']), constants.cancel.fr),
            "es": "Por favor entre en el número (1 a %i) [O %s para anulan la búsqueda]" % (len(wakdata.latest['recipeCategories']), constants.cancel.es),
            "pt": "Por favor entre no número (1 a %i) [Ou %s para cancelam a pesquisa]" % (len(wakdata.latest['recipeCategories']), constants.cancel.pt)
        })
        dit = {
            "cancel": constants.cancel.getByShortLang(lang),
            "max": len(wakdata.latest['recipeCategories']) + 1,
            "min": 0,
            "error_msg": error_msg.getByShortLang(lang)
        }
        value = await own_discord.wait_input(ctx, own_discord.int_check, dit)
        await ctx.typing()
        if not value or value == -1:
            await own_discord.reponse_send(ctx, constants.canceled.getByShortLang(lang))
        else:
            return RecipeCategorie(wakdata.latest['recipeCategories'][value - 1])
        return None
