from config import settings
from discord.ext import commands
from cdnwakfu import Text, Lang
from own import own_discord
from models import User, Guild
from utils import Interact

class Lang(Interact):


    @staticmethod
    def createMsg(langData:str, lang:Lang):
        return langData.nameAttr

    @staticmethod
    def createList(langs:list, lang:str):
        msg = ""
        compt = 1
        for tempLang in langs:
            msg = "%s%i - %s\n" % (msg, compt, tempLang.nameAttr)
            compt += 1
        return msg

    @classmethod
    async def sendChoices(cls, ctx:commands.Context, objs:list, lang:str):
        msg = "%s\n%s" % (
            Text({
                "fr":"Choisissez la langue d'utilisation de la commande courante",
                "en":"Choose the language for the current command",
                "es":"Elija el idioma para el comando actual",
                "pt":"Escolha o idioma para o comando atual"
            }),
            cls.createList(objs, lang)
        )
        return await own_discord.embed_send(ctx, own_discord.embed_create(msg))
    
    @classmethod
    async def select(cls, ctx:commands.Context, lang:str):
        return await super().select(ctx, Text.langs, lang)

    @classmethod
    async def display(cls, ctx:commands.Context, lang:str):
        return await super().display(ctx, Text.langs, lang)

    @classmethod
    async def checkLangWithSet(cls, ctx:commands.Context, lang:str, users:User, guilds:Guild):
        foundLang = cls.checkLang(lang)
        if foundLang is None:
            newLang = cls.getLang(ctx, users, guilds)
            ms = Text({
                "fr":"Langue non disponible (Langue définie : %s)\nLangues disponibles :\n" % newLang,
                "en":"Lang isn't available (Lang defined : %s)\nLangs available :\n" % newLang,
                "es":"Lang no está disponible (Lang definió : %s)\n Langs disponible:\n" % newLang,
                "pt":"Lang não está disponível (Lang definiu : %s)\n Langs disponível:\n" % newLang
            })
            msg = ms.getByShortLang(newLang)
            await own_discord.reponse_send(ctx, msg)
            return newLang
        return foundLang

    @staticmethod
    def checkLang(lang:str):
        lang = lang.lower().strip()
        if not (lang in Text.shortLangsAccepted()):
            return None
        return lang

    @staticmethod
    def getLangofGuild(guild_id:int, guilds:Guild, withoutDefault:bool = False):
        lang = guilds.fetchLangOfServer(guild_id)
        if not lang:
            lang = settings.DISCORD_LANG if withoutDefault is False else None
        return lang

    @classmethod
    def getLang(cls, ctx:commands.Context, users:User, guilds:Guild, withoutDefault:bool = False):
        lang = users.fetchLangOfUser(ctx.author.id)
        if not lang:
            if ctx.guild:
                return cls.getLangofGuild(ctx.guild.id, guilds, withoutDefault)
            else:
                return settings.DISCORD_LANG if withoutDefault is False else None
        return lang
    
    @staticmethod
    def getLangByUserOnly(user:int, users:User):
        return users.fetchLangOfUser(user)