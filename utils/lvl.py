class Lvl:

    @staticmethod
    def checkAvatarLvl(lvl:int):
        if 215 >= lvl > 0:
            return lvl
        return None

    @staticmethod
    def checkJobLvl(lvl:int):
        if 170 >= lvl > 0:
            return lvl
        return None