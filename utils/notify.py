from discord.ext import commands
from cdnwakfu import Text
from own import own_discord
from utils import Interact
import models_utils

class Notify(Interact, models_utils.Notify):

    @classmethod
    async def checkNotify(cls, ctx:commands.Context, notify:str, lang:str):
        notify = notify.lower().strip()
        for notif in cls.notify:
            if notif.getByShortLang(lang) == notify:
                return cls.notify.index(notif)
        msg = Text({
            "fr":"Notification non disponible \nNotifications Disponibles :\n",
            "en":"Notify isn't available \nNotifys availables :\n",
            "es":"Notifique no está disponible \nNotifys availables :\n",
            "pt":"Notifique não está disponível \nNotifys availables :\n"
        })
        await own_discord.reponse_send(ctx, msg.getByShortLang(lang))
        notify = await cls.select(ctx, lang)
        for notif in cls.notify:
            if notif == notify:
                return cls.notify.index(notif)
        default = Text({
            "fr":"Notification Définie : %s" % cls.notify[cls.default].getByShortLang("fr"),
            "en":"Notify defined : %s" % cls.notify[cls.default].getByShortLang("en"),
            "es":"Notifique definido: %s" % cls.notify[cls.default].getByShortLang("es"),
            "pt":"Notifique definido: %s" % cls.notify[cls.default].getByShortLang("pt")
        })
        await own_discord.reponse_send(ctx, default.getByShortLang(lang))
        return cls.default

    @classmethod
    def extractNotify(cls, notify:int, lang:str):
        if len(cls.notify) > notify >= 0:
            return cls.notify[notify].getByShortLang(lang)
        return None

    @staticmethod
    def createMsg(notify:Text, lang:str):
        return notify.getByShortLang(lang)

    @staticmethod
    def createList(notifys:list, lang:str):
        msg = ""
        compt = 1
        for notify in notifys:
            msg = "%s%i - %s\n" % (msg, compt, notify.getByShortLang(lang))
            compt += 1
        return msg

    @classmethod
    async def select(cls, ctx:commands.Context, lang:str):
        return await super().select(ctx, cls.notify, lang)

    @classmethod
    async def display(cls, ctx:commands.Context, lang:str):
        return await super().display(ctx, cls.notify, lang)