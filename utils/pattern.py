from cdnwakfu import RecipePattern
from data import wakdata
from models import Avatar

class Pattern:

    @staticmethod
    def extractPatterns():
        patterns = []
        for pattern in wakdata.latest['recipePattern']:
            patterns.append(RecipePattern(pattern))
        return patterns

    @classmethod
    def extractRawRecipeAvatar(cls, id:int, avatars:Avatar, missing:bool = False):
        patterns = []
        for pattern in cls.extractPatterns():
            pat = avatars.fetchPatternsByIdAndPattern(id ,pattern.id)
            if missing is True:
                if not pat:
                    patterns.append(pattern)
            else:
                if pat:
                    patterns.append(pattern)
        return patterns

    @staticmethod
    def prepMsgPattern(patterns:list, lang:str, prefix:str = ""):
        sortByObtention = {}
        for pattern in patterns:
            if pattern.obtention.__str__() not in sortByObtention.keys():
                sortByObtention[pattern.obtention.__str__()] = {
                    "patterns":[pattern],
                    "text":pattern.obtention
                }
            else:
                sortByObtention[pattern.obtention.__str__()]["patterns"].append(pattern)
        msg = ""
        for obt in sortByObtention.keys():
            msg = "%s%sIn %s:\n" % (msg, prefix, sortByObtention[obt]['text'].getByShortLang(lang))
            for pat in sortByObtention[obt]["patterns"]:
                msg = "%s%s\t%s\n" % (msg, prefix, pat.name.getByShortLang(lang))
        return msg

    @classmethod
    def extractMissingRecipeAvatar(cls, id:int, avatars:Avatar, lang:str, prefix:str = ""):
        return cls.prepMsgPattern(cls.extractRawRecipeAvatar(id, avatars, True), lang, prefix)

    @classmethod
    def extractRecipeAvatar(cls, id:int, avatars:Avatar, lang:str, prefix:str = ""):
        return cls.prepMsgPattern(cls.extractRawRecipeAvatar(id, avatars), lang, prefix)