from data import wakdata
from utils import Interact
from models import Avatar
from own.own_sqlite.models import ModelData
import constants
from own import own_discord
from discord.ext import commands
from concurrent.futures.thread import ThreadPoolExecutor
import cdnwakfu
import asyncio


class Recipe(Interact):

    @staticmethod
    def createList(recipes: list, lang: str):
        compt = 1
        msg = ""
        for recipe in recipes:
            recipe.fetchData(wakdata.latest, 2)
            if recipe.recipeCategorie:
                ms = cdnwakfu.Text({
                    "fr": "%s%i - avec %s (%s:%i) composé par :\n" % (msg, compt, recipe.recipeCategorie.title.getByShortLang(lang), constants.lvl.getByShortLang(lang), recipe.level),
                    "en": "%s%i - with %s (%s:%i) composed by :\n" % (msg, compt, recipe.recipeCategorie.title.getByShortLang(lang), constants.lvl.getByShortLang(lang), recipe.level),
                    "es": "%s%i - con %s (%s: % i) formado por :\n" % (msg, compt, recipe.recipeCategorie.title.getByShortLang(lang), constants.lvl.getByShortLang(lang), recipe.level),
                    "pt": "%s%i - com %s (%s: % i) composto por :\n" % (msg, compt, recipe.recipeCategorie.title.getByShortLang(lang), constants.lvl.getByShortLang(lang), recipe.level)
                })
                msg = ms.getByShortLang(lang)
                for ingredient in recipe.recipeIngredients:
                    msg = "%s\t%i x %s\n" % (
                        msg, ingredient.quantity, ingredient.item.title.getByShortLang(lang))
                compt += 1
        return msg

    @staticmethod
    def requestIsAvailableForAvatar(recip: cdnwakfu.Recipe, avatar: int, avatars: Avatar):
        return avatars.recipeIsCraftableByAvatar(recip, avatar)

    @classmethod
    async def showAllComponents(cls, recipe: cdnwakfu.Recipe, lang: str):
        ms = cdnwakfu.Text({
            "fr": "Composé par :\n",
            "en": "Composed by :\n",
            "es": "Formado por :\n",
            "pt": "Composto por :\n"
        })
        msg = ms.getByShortLang(lang)
        components = await asyncio.get_event_loop().run_in_executor(ThreadPoolExecutor(20), recipe.getAllComponents, wakdata.latest)
        for component in components:
            component.fetchData(wakdata.latest, 1)
            msg = "%s\t%i x %s\n" % (
                msg, component.quantity, component.item.title.getByShortLang(lang))
        return msg

    @classmethod
    async def sendAllComponent(cls, recipe: cdnwakfu.Recipe, lang: str, ctx: commands.Context):
        msg = await own_discord.reponse_send(ctx, constants.processing.getByShortLang(lang))
        await ctx.typing()
        content = await Recipe.showAllComponents(recipe, lang)
        await own_discord.embed_update(ctx, msg, own_discord.embed_create(content))
        await msg.edit(content="")

    @staticmethod
    def getXP(recipe: cdnwakfu.Recipe, avatar: ModelData, avatars: Avatar):
        recipe.fetchData(wakdata.latest, 1)
        job = avatars.fetchJobLvlByAvatarAndJob(
            avatar.id, recipe.recipeCategorie.id)
        lvl = job.lvl if job else 1
        return recipe.getXP(lvl, wakdata.latest)

    @staticmethod
    def getRate(recipe: cdnwakfu.Recipe, avatar: ModelData, avatars: Avatar):
        recipe.fetchData(wakdata.latest, 1)
        job = avatars.fetchJobLvlByAvatarAndJob(
            avatar.id, recipe.recipeCategorie.id)
        lvl = job.lvl if job else 1
        return recipe.craftRate(lvl)

    @classmethod
    async def sendXP(cls, recipe: cdnwakfu.Recipe, avatar: ModelData, lang: str, ctx: commands.Context, avatars: Avatar):
        xp = cls.getXP(recipe, avatar, avatars)
        rate = cls.getRate(recipe, avatar, avatars)
        if rate > 0:
            if xp > 0:
                craft_rate = cdnwakfu.Text({
                    "fr": "(Avec un taux de réussite de %i.%i)" % (rate.real, int(rate*100)),
                    "en": "(With craft rate of %i.%i)" % (rate.real, int(rate*100)),
                    "es": "(Con precio del arte de %i.%i)" % (rate.real, int(rate*100)),
                    "pt": "(Com tarifa de ofício de %i.%i)" % (rate.real, int(rate*100))
                })
                xp_earned = cdnwakfu.Text({
                    "fr": "Cette recette vous rapportera %i XP" % xp,
                    "en": "This recipe will bring back %i XP to you" % xp,
                    "es": "Esta receta devolverá %i XP a usted" % xp,
                    "pt": "Esta receita devolverá %i XP a você" % xp
                })
                msg = xp_earned.getByShortLang(lang)
                if rate < 1:
                    msg = "%s %s" % (msg, craft_rate.getByShortLang(lang))
                await own_discord.reponse_send(ctx, msg)
            else:
                await own_discord.reponse_send(ctx, constants.no_xp_earned.getByShortLang(lang))
        else:
            await own_discord.reponse_send(ctx, constants.craft_fail.getByShortLang(lang))
