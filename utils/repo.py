from utils import Interact
from discord.ext import commands
import constants
from config import settings
import copy

class Repo:

    def __init__(self, repo:str, protocol:str = settings.GITLAB_PROTOCOL, provider:str = settings.GITLAB_PROVIDER, author:str = settings.GITLAB_AUTHOR, token:str = settings.GITLAB_TOKEN):
        self.protocol = protocol
        self.provider = provider
        self.author = author
        self.repo = repo
        self.token = token

    def createMsg(self, lang:str):
        return self.createRow(lang)

    def createRow(self, lang:str):
        if self.repo in constants.repos:
            return constants.repos[self.repo].getByShortLang(lang)
        else:
            return self.repo

    @staticmethod
    def foundRepoObjectOfTarget(target:str, repos:list):
        for repo in repos:
            if repo.repo == target:
                return repo
        return None

default_repo = Repo(settings.GITLAB_REPO)
repos = [
    default_repo,
    Repo(settings.GITLAB_WEB_REPO),
    Repo(settings.GITLAB_DATA_REPO),
    Repo(settings.GITLAB_MODELS_REPO)
]
all_repos = Repo("*")
read_all_repos = copy.deepcopy(repos)
read_all_repos.insert(0, all_repos)

class RepoInteract(Interact):

    @staticmethod
    def createMsg(repo:Repo, lang:str):
        return repo.createMsg(lang)

    @staticmethod
    def createList(repos:list, lang:str):
        compt = 1
        msg = ""
        for repo in repos:
            msg = "%s%i - %s\n" % (msg, compt, repo.createRow(lang))
            compt += 1
        return msg

    @classmethod
    async def select(cls, ctx:commands.Context, lang:str, repos:list = repos):
        return await super().select(ctx, repos, lang)

    @classmethod
    async def display(cls, ctx:commands.Context, lang:str, repos:list = repos):
        return await super().display(ctx, repos, lang)