from utils import BaseIssue
from cdnwakfu import Text

class Report(BaseIssue):

    label = Text({
        "en":"Bug Report",
        "fr":"Rapport de Bug",
        "es":"Informe del error de programación",
        "pt":"Relatório de defeito"
    })