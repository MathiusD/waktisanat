from own.own_sqlite.models import ModelData
from own import own_discord
from discord.ext import commands
from ..requests import ItemRequest
from ..requests_basic import ComponentRequest as ComponentRequestBase
import models
from utils import ItemInteract
import constants
from models import Request


class ComponentRequest(ItemRequest, ComponentRequestBase):

    @classmethod
    async def addRequestWithSearch(cls, ctx: commands.Context, avatar: ModelData, lang: str, requests: Request, search: str):
        msg = await own_discord.reponse_send(ctx, constants.processing.getByShortLang(lang))
        await ctx.typing()
        items = await ItemInteract.searchItemWithLoot(search, lang)
        await msg.delete()
        item = await ItemInteract.select(ctx, items, lang)
        if item:
            int_data = await cls.waitingQuantity(ctx, lang)
            if int_data:
                request = cls.getSpecificRequest(requests).addRequest(
                    ctx.author.id, avatar.name, item._id, int_data)
                if request:
                    await own_discord.reponse_send(ctx, constants.request_submit.getByShortLang(lang))
                else:
                    await own_discord.reponse_send(ctx, constants.request_already_submit.getByShortLang(lang))
