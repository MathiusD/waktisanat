import cdnwakfu
import models
from data import wakdata
from own import own_discord
from utils import Server, Lang, Avatar, Recipe, ItemInteract
from discord.ext import commands
from own.own_sqlite.models import ModelData
from models import Request, ItemPrice
import constants
import utils.requests_basic as basic


class CraftRequest(basic.CraftRequest):

    @classmethod
    async def requestsAvailable(cls, ctx: commands.Context, avatar: ModelData, lang: str, requests: Request, prices: ItemPrice):
        await ctx.typing()
        data = cls.findRequestAvailableForAvatar(avatar, requests)
        if data:
            request = await cls.select(ctx, data, lang)
            if request:
                await cls.send(ctx, lang, avatar, request, requests, prices)
        else:
            await own_discord.reponse_send(ctx, constants.no_request_available.getByShortLang(lang))

    @classmethod
    async def requestsAvatar(cls, ctx: commands.Context, avatar: ModelData, lang: str, requests: Request, prices: ItemPrice):
        await ctx.typing()
        data = cls.getSpecificRequest(requests).fetchRequestByAvatar(
            ctx.author.id, avatar.name)
        if data:
            request = await cls.select(ctx, data, lang)
            if request:
                msg = await cls.send(ctx, lang, avatar, request, requests, prices)
                return msg
        else:
            await own_discord.reponse_send(ctx, constants.no_request.getByShortLang(lang))
        return None

    @classmethod
    async def addRequestWithSearch(cls, ctx: commands.Context, avatar: ModelData, lang: str, requests: Request, search: str):
        msg = await own_discord.reponse_send(ctx, constants.processing.getByShortLang(lang))
        await ctx.typing()
        data = await ItemInteract.searchItemWithPattern(search, lang)
        items = data["items"]
        glob_recipes = data["recipes"]
        await msg.delete()
        item = await ItemInteract.select(ctx, items, lang)
        if item:
            recipes = glob_recipes[items.index(item)]
            await ctx.typing()
            recipe = await Recipe.select(ctx, recipes, lang)
            int_data = await cls.waitingQuantity(ctx, lang)
            if int_data:
                request = cls.getSpecificRequest(requests).addRequest(
                    ctx.author.id, avatar.name, recipe.id)
                if request:
                    await own_discord.reponse_send(ctx, constants.request_submit.getByShortLang(lang))
                else:
                    await own_discord.reponse_send(ctx, constants.request_already_submit.getByShortLang(lang))
