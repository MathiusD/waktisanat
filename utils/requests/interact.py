from discord.ext import commands
from ..requests import requests
import utils.requests_basic as basic

class RequestInteract(basic.RequestInteract):

    @classmethod
    async def select(cls, ctx:commands.Context, lang:str, requests:list = requests):
        return await super().select(ctx, lang, requests)

    @classmethod
    async def display(cls, ctx:commands.Context, lang:str, requests:list = requests):
        return await super().display(ctx, lang, requests)