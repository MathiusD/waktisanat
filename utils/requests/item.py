import models
import discord
from own import own_discord
from utils import Lang, Avatar, Recipe, ItemInteract
from discord.ext import commands
from models import Request, ItemPrice
from own.own_sqlite.models import ModelData
import constants
import utils.requests_basic as basic


class ItemRequest(basic.ItemRequest):

    @classmethod
    async def requestsAvailable(cls, ctx: commands.Context, avatar: ModelData, lang: str, requests: Request, prices: ItemPrice):
        await ctx.typing()
        data = cls.findRequestAvailableForAvatar(avatar, requests)
        if data:
            request = await cls.select(ctx, data, lang)
            if request:
                await cls.send(ctx, lang, avatar, request, requests, prices)
        else:
            await own_discord.reponse_send(ctx, constants.no_request_available.getByShortLang(lang))

    @classmethod
    async def requestsAvatar(cls, ctx: commands.Context, avatar: ModelData, lang: str, requests: Request, prices: ItemPrice):
        await ctx.typing()
        data = cls.getSpecificRequest(requests).fetchRequestByAvatar(
            ctx.author.id, avatar.name)
        if data:
            request = await cls.select(ctx, data, lang)
            if request:
                msg = await cls.send(ctx, lang, avatar, request, requests, prices)
                return msg
        else:
            await own_discord.reponse_send(ctx, constants.no_request.getByShortLang(lang))
        return None
