from .base import BaseRequest
from .craft import CraftRequest
from .item import ItemRequest
from .component import ComponentRequest
from .buy import BuyRequest
requests = [
    CraftRequest,
    ComponentRequest,
    BuyRequest
]
from .interact import RequestInteract