from utils import Interact, Avatar, Server
import constants
from models import User, Request, ItemPrice
from own import own_discord
from discord.ext import commands
from own.own_sqlite.models import ModelData
from data.wakdata import latest
import cdnwakfu
import abc
import models
import discord
import utils
import datetime


class BaseRequest(Interact):

    name = "Default"
    requestAttr = "Default"

    @staticmethod
    @abc.abstractmethod
    def prepList(requests: list):
        pass

    @classmethod
    def createList(cls, requests: list, lang: str):
        msg = ""
        compt = 1
        for request in cls.prepList(requests):
            item = request["item"]
            quantity = request["quantity"]
            if item.title:
                msg = "%s%i - %i X %s" % (msg, compt,
                                          quantity, item.title.getByShortLang(lang))
                if isinstance(item, cdnwakfu.Item):
                    item.fetchData(latest, 3)
                    msg = "%s (%s)" % (msg, cdnwakfu.Rarity.RaritytoText(
                        item.definition.item.baseParameters.rarity))
                    if item.definition.item.equipmentItemType:
                        msg = "%s [%s]" % (
                            msg, item.definition.item.equipmentItemType.title.getByShortLang(lang))
                msg = "%s\n" % msg
            compt += 1
        return msg

    @classmethod
    def getSpecificRequest(cls, requests: Request):
        return getattr(requests, cls.requestAttr)

    @classmethod
    def createOption(cls, lang: str, avatar: ModelData, request: ModelData, requests: Request):
        users = requests.avatars.users
        return "%s\n%s\n%s\n" % (
            cls.name.getByShortLang(lang),
            cls.createNeeding(
                lang,
                cls.listOfNeeding(request, requests),
                users
            ),
            cls.resolveBy(
                lang,
                cls.listOfResolve(avatar, request, requests.avatars),
                users
            )
        )

    @staticmethod
    def resolveBy(lang: str, avatars: list = None, users: User = None):
        msg = ""
        if users and avatars:
            msg = "%s%s :\n" % (
                msg, constants.request_can_be_resolve_by.getByShortLang(lang))
            for avatar in avatars:
                msg = "%s\t%s" % (msg, Avatar.rowAvatar(avatar, users, lang))
        return msg

    @staticmethod
    def createNeeding(lang: str, request_list: list = None, users: User = None):
        msg = ""
        if users and request_list:
            msg = "%s%s :\n" % (msg, constants.needing_by.getByShortLang(lang))
            for request in request_list:
                raw = "%i X %s (%s)\n" % (
                    request["quantity"],
                    Avatar.rowAvatar(request["avatar"], users, lang)[1:-2],
                    datetime.datetime.utcfromtimestamp(request["since"])
                )
                msg = "%s\t%s" % (
                    msg,
                    "%s : %s " % (
                        constants.current.getByShortLang(lang),
                        raw
                    ) if request["current"] else raw
                )
        return msg

    @staticmethod
    def requestInThisSameServer(avatar: ModelData, request: ModelData, avatars: Avatar):
        other = avatars.fetchAvatarsById(request.id_avatar)
        return True if Server.extractServer(other.server) == Server.extractServer(avatar.server) else False

    @classmethod
    @abc.abstractmethod
    def listOfNeeding(cls, request: ModelData, requests: Request):
        pass

    @staticmethod
    @abc.abstractmethod
    def pingLevel(request: ModelData):
        pass

    @classmethod
    @abc.abstractmethod
    async def send(cls, ctx: commands.Context, lang: str, avatar: ModelData, request: ModelData, requests: Request, prices: ItemPrice):
        pass

    @classmethod
    async def sendPingsForRequest(cls, bot: commands.AutoShardedBot, request: ModelData, requests: Request, prices: ItemPrice):
        avatars = requests.avatars
        users = avatars.users.fetchAll()
        ping_lvl = cls.pingLevel(request)
        for user in users:
            if user.notify <= ping_lvl:
                avatars_available = None
                own_request = False
                for avatar in avatars.fetchAvatarsByDiscordId(user.discord_id):
                    if avatar.id == request.id_avatar:
                        own_request = True
                    if avatars_available is None or own_request == False:
                        if cls.requestIsAvailableForAvatar(avatar, request, avatars):
                            avatars_available = avatar
                if avatars_available and own_request == False:
                    data = {
                        "avatars": avatars_available,
                        "request": request,
                        "requests": requests,
                        "prices": prices
                    }

                    async def sending(ctx: commands.Context, lang: str, data: dict = {}):
                        await cls.send(ctx, lang, data["avatars"], data["request"], data["requests"], data["prices"])
                    await utils.User.execCoroInUserDM(bot, user.discord_id, avatars.users, sending, data)

    @classmethod
    def userHaveAvatarForResolveRequest(cls, userDiscordId: int, request: ModelData, avatars: models.Avatar):
        for avatar in avatars.fetchAvatarsByDiscordId(userDiscordId):
            if cls.requestIsAvailableForAvatar(avatar, request, avatars):
                return True
        return False

    @classmethod
    def listOfResolve(cls, avatar: ModelData, request: ModelData, avatars: models.Avatar):
        avts = []
        data = avatars.fetchAll() if avatar.id == request.id_avatar else avatars.fetchAvatarsByDiscordId(
            avatars.users.fetchUserById(avatar.id_user).discord_id)
        for avatar in data:
            if cls.requestIsAvailableForAvatar(avatar, request, avatars):
                avts.append(avatar)
        return avts

    @staticmethod
    @abc.abstractmethod
    def requestIsAvailableForAvatar(avatar: ModelData, request: ModelData, avatars: models.Avatar):
        pass

    @classmethod
    def findRequestAvailableForAvatar(cls, avatar: ModelData, requests: Request):
        req = []
        data = cls.getSpecificRequest(requests).fetchAll()
        for dat in data:
            if cls.requestIsAvailableForAvatar(avatar, dat, requests.avatars) and avatar.id != dat.id_avatar:
                avatarReq = requests.avatars.fetchAvatarsById(dat.id_avatar)
                if avatarReq and avatarReq.id_user != avatar.id_user:
                    req.append(dat)
        return req

    @staticmethod
    @abc.abstractmethod
    async def addRequest(ctx: commands.Context, avatar: Avatar, lang: str, requests: Request, id: str):
        pass

    @classmethod
    async def removeAllRequest(cls, ctx: commands.Context, avatar: models.Avatar, lang: str, requests: Request):
        if cls.getSpecificRequest(requests).removeAllRequest(ctx.author.id, avatar.name):
            await own_discord.reponse_send(ctx, constants.requests_unsbmit.getByShortLang(lang))
        else:
            await own_discord.reponse_send(ctx, constants.error_occured.getByShortLang(lang))

    @staticmethod
    async def waitingQuantity(ctx: commands.Context, lang: str):
        max_stack = 9999
        error_msg = cdnwakfu.Text({
            "fr": "Entrer un nombre (1 à %i) [Ou %s pour annuler la recherche]" % (max_stack, constants.cancel.en),
            "en": "Please enter number (1 to %i) [Or %s for cancel search]" % (max_stack, constants.cancel.fr),
            "es": "Por favor entre en el número (1 a %i) [O %s para anulan la búsqueda]" % (max_stack, constants.cancel.es),
            "pt": "Por favor entre no número (1 a %i) [Ou %s para cancelam a pesquisa]" % (max_stack, constants.cancel.pt)
        })
        dit = {
            "cancel": constants.cancel.getByShortLang(lang),
            "max": max_stack,
            "min": 0,
            "error_msg": error_msg.getByShortLang(lang)
        }
        int_req = cdnwakfu.Text({
            "fr": "Veuillez saisir la quantité souhaitée :",
            "en": "Please enter the desired quantity :",
            "es": "Por favor, introduzca la cantidad deseada",
            "pt": "Por favor entre na quantidade desejada"
        })
        await own_discord.reponse_send(ctx, int_req.getByShortLang(lang))
        int_data = await own_discord.wait_input(ctx, own_discord.int_check, dit, 120.0)
        await ctx.typing()
        if not int_data or int_data == -1:
            await own_discord.reponse_send(ctx, constants.canceled.getByShortLang(lang))
            return None
        else:
            return int_data

    @classmethod
    async def removeRequest(cls, ctx: commands.Context, msg: discord.Message, avatar: ModelData, request: ModelData, requests: Request, lang: str):
        try:
            if request.id_avatar == avatar.id:
                if await own_discord.wait_react(ctx, msg, "❌") is True:
                    if cls.getSpecificRequest(requests).removeRequest(ctx.author.id, avatar.name, request.id_item):
                        await own_discord.reponse_send(ctx, constants.request_unsbmit.getByShortLang(lang))
                    else:
                        await own_discord.reponse_send(ctx, constants.error_occured.getByShortLang(lang))
        except Exception as exc:
            await ctx.bot.on_command_error(ctx, exc)

    @classmethod
    async def updateRequest(cls, ctx: commands.Context, msg: discord.Message, avatar: ModelData, request: ModelData, requests: Request, lang: str):
        try:
            if request.id_avatar == avatar.id:
                if await own_discord.wait_react(ctx, msg, "📝") is True:
                    quantity = await cls.waitingQuantity(ctx, lang)
                    if quantity:
                        if cls.getSpecificRequest(requests).updateRequest(request.id, quantity):
                            await own_discord.reponse_send(ctx, constants.request_updated.getByShortLang(lang))
                        else:
                            await own_discord.reponse_send(ctx, constants.error_occured.getByShortLang(lang))
        except Exception as exc:
            await ctx.bot.on_command_error(ctx, exc)
