from own.own_sqlite.models import ModelData
from ..requests_basic import ItemRequest
import models_utils, models

class BuyRequest(ItemRequest):

    name = models_utils.Requests.buyRequest
    requestAttr = "buyRequests"

    @classmethod
    def requestIsAvailableForAvatar(cls, avatar:ModelData, request:ModelData, avatars:models.Avatar):
        return cls.requestInThisSameServer(avatar, request, avatars)