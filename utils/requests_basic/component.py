from data import wakdata
from own.own_sqlite.models import ModelData
from ..requests_basic import ItemRequest
import models_utils, models, cdnwakfu

class ComponentRequest(ItemRequest):

    name = models_utils.Requests.componentRequest
    requestAttr = "componentRequests"

    @classmethod
    def requestIsAvailableForAvatar(cls, avatar:ModelData, request:ModelData, avatars:models.Avatar):
        if cls.requestInThisSameServer(avatar, request, avatars):
            item = cdnwakfu.SearchItem.getItem(wakdata.latest, request.id_item)
            item.fetchData(wakdata.latest)
            for lot in item.loots:
                for loot in lot:
                    for resource in loot.collectibleResources:
                        lvl = avatars.fetchJobLvlByAvatarAndJob(avatar.id, resource.skillId)
                        if lvl.lvl >= resource.skillLevelRequired:
                            return True
        return False