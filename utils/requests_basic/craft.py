import cdnwakfu
import models
from data import wakdata
from own import own_discord
from utils import Item, Lang, Avatar, Recipe
from discord.ext import commands
from models import User, Request
from own.own_sqlite.models import ModelData
import constants
from ..requests_basic import BaseRequest
import models_utils
import asyncio


class CraftRequest(BaseRequest):

    name = models_utils.Requests.craftRequest
    requestAttr = "craftRequests"

    @staticmethod
    def prepList(requests: list):
        items = []
        for request in requests:
            recip = cdnwakfu.Recipe.findRecipe(
                wakdata.latest["recipes"], request.id_recipe)
            recip.fetchData(wakdata.latest, 2)
            items.append({"item": Item.getItemById(
                recip.recipeResults[0].item._id), "quantity": request.quantity})
        return items

    @classmethod
    def listOfNeeding(cls, request: ModelData, requests: Request):
        out = []
        data = cls.getSpecificRequest(
            requests).fetchRequestByRecipe(request.id_recipe)
        for dat in data:
            avatar = requests.avatars.fetchAvatarsById(dat.id_avatar)
            if avatar:
                temp = {
                    "avatar": avatar,
                    "quantity": dat.quantity,
                    "current": False,
                    "since": dat.last_edtition
                }
                if dat.id == request.id:
                    temp["current"] = True
                out.append(temp)
        return out

    @staticmethod
    def pingLevel(request: ModelData):
        recipe = cdnwakfu.Recipe.findRecipe(wakdata.latest, request.id_recipe)
        recipe.fetchData(wakdata, 3)
        return 0 if recipe.recipePattern == None else 1

    @classmethod
    async def send(cls, ctx: commands.Context, lang: str, avatar: ModelData, request: ModelData, requests: Request, prices: models.ItemPrice):
        recipe = await cdnwakfu.Recipe.asyncFindRecipe(wakdata.latest["recipes"], request.id_recipe)
        recipe.fetchData(wakdata.latest, 3)
        item = recipe.recipeResults[0].item
        msg = await Item.send(ctx, item, lang, prices, option_msg=cls.createOption(lang, avatar, request, requests), recipes=[recipe])
        if msg:
            asyncio.get_event_loop().create_task(cls.removeRequest(
                ctx, msg, avatar, request, requests, lang))
            asyncio.get_event_loop().create_task(cls.updateRequest(
                ctx, msg, avatar, request, requests, lang))
            return msg

    @classmethod
    def requestIsAvailableForAvatar(cls, avatar: ModelData, request: ModelData, avatars: models.Avatar):
        if cls.requestInThisSameServer(avatar, request, avatars):
            recip = cdnwakfu.Recipe.findRecipe(
                wakdata.latest["recipes"], request.id_recipe)
            recip.fetchData(wakdata.latest, 1)
            if Recipe.requestIsAvailableForAvatar(recip, avatar.id, avatars):
                return True
        return False

    @classmethod
    async def addRequest(cls, ctx: commands.Context, avatar: models.Avatar, lang: str, requests: Request, data: object):
        if isinstance(data, cdnwakfu.Recipe):
            id = data.id
        else:
            id = data
            recipe = await cdnwakfu.Recipe.asyncFindRecipe(wakdata.latest["recipes"], id)
            data = recipe
        if data is not None:
            int_data = await cls.waitingQuantity(ctx, lang)
            if int_data:
                request = cls.getSpecificRequest(requests).addRequest(
                    ctx.author.id, avatar.name, id, int_data)
                if request is not None:
                    await own_discord.reponse_send(ctx, constants.request_submit.getByShortLang(lang))
                else:
                    await own_discord.reponse_send(ctx, constants.request_already_submit.getByShortLang(lang))
        else:
            await own_discord.reponse_send(ctx, constants.error_occured.getByShortLang(lang))
