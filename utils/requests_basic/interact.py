from utils import Interact
from discord.ext import commands
from ..requests_basic import BaseRequest, requests

class RequestInteract(Interact):

    @staticmethod
    def createMsg(request:BaseRequest, lang:str):
        return request.name.getByShortLang(lang)

    @staticmethod
    def createList(requests:list, lang:str):
        msg = ""
        compt = 1
        for request in requests:
            msg = "%s%i - %s\n" % (msg, compt, request.name.getByShortLang(lang))
            compt += 1
        return msg

    @classmethod
    async def select(cls, ctx:commands.Context, lang:str, requests:list = requests):
        return await super().select(ctx, requests, lang)

    @classmethod
    async def display(cls, ctx:commands.Context, lang:str, requests:list = requests):
        return await super().display(ctx, requests, lang)