import cdnwakfu
import models
from data import wakdata
from own import own_discord
from utils import Item, Lang, Avatar
from discord.ext import commands
from models import User, Request
from own.own_sqlite.models import ModelData
import constants
from ..requests_basic import BaseRequest
import asyncio


class ItemRequest(BaseRequest):

    name = "Item"
    requestAttr = "ItemRequest"

    @staticmethod
    def prepList(requests: list):
        items = []
        for request in requests:
            item = cdnwakfu.SearchItem.getItem(wakdata.latest, request.id_item)
            items.append({"item": item, "quantity": request.quantity})
        return items

    @classmethod
    def listOfNeeding(cls, request: ModelData, requests: Request):
        out = []
        data = cls.getSpecificRequest(
            requests).fetchRequestByItem(request.id_item)
        for dat in data:
            avatar = requests.avatars.fetchAvatarsById(dat.id_avatar)
            if avatar:
                temp = {
                    "avatar": avatar,
                    "quantity": dat.quantity,
                    "current": False,
                    "since": dat.last_edtition
                }
                if dat.id == request.id:
                    temp["current"] = True
                out.append(temp)
        return out

    @staticmethod
    def pingLevel(request: ModelData):
        return 0

    @classmethod
    async def send(cls, ctx: commands.Context, lang: str, avatar: ModelData, request: ModelData, requests: Request, prices: models.ItemPrice):
        item = await cdnwakfu.SearchItem.asyncGetItemsById(wakdata.latest, request.id_item)
        msg = await Item.send(ctx, item, lang, prices, option_msg=cls.createOption(lang, avatar, request, requests))
        if msg:
            asyncio.get_event_loop().create_task(cls.removeRequest(
                ctx, msg, avatar, request, requests, lang))
            asyncio.get_event_loop().create_task(cls.updateRequest(
                ctx, msg, avatar, request, requests, lang))
            return msg

    @classmethod
    async def addRequest(cls, ctx: commands.Context, avatar: models.Avatar, lang: str, requests: Request, data: object):
        if isinstance(data, cdnwakfu.BaseItem):
            id = data._id
        else:
            id = data
            recipe = await cdnwakfu.SearchItem.asyncGetItemsById(wakdata.latest, id)
            data = recipe
        if data is not None:
            int_data = await cls.waitingQuantity(ctx, lang)
            if int_data:
                request = cls.getSpecificRequest(requests).addRequest(
                    ctx.author.id, avatar.name, id, int_data)
                if request is not None:
                    await own_discord.reponse_send(ctx, constants.request_submit.getByShortLang(lang))
                else:
                    await own_discord.reponse_send(ctx, constants.request_already_submit.getByShortLang(lang))
        else:
            await own_discord.reponse_send(ctx, constants.error_occured.getByShortLang(lang))
