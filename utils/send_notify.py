from discord.ext import commands
from models import Guild
import discord, logging, utils
from own import own_discord
from cdnwakfu import Text

async def sendAll(bot:commands.AutoShardedBot, guilds:Guild, coro):
    for guild in bot.guilds:
        data = guilds.fetchGuildByDiscordId(guild.id)
        if data and data.notify_chan:
            lang = utils.Lang.getLangofGuild(guild.id, guilds)
            channel = guild.get_channel(data.notify_chan)
            if channel:
                try:
                    msg = await channel.send("Devlog sending...")
                    await coro(await bot.get_context(msg), lang)
                    await msg.delete()
                except discord.Forbidden:
                    chan = await own_discord.getFirstChanWithAllowSendMsg(guild, bot)
                    if chan:
                        warn = Text({
                            "fr":"Le bot <@%i> n'a pas le droit d'envoyer de message dans le salon de notification <#%i>" % (bot.user.id, data.notify_chan),
                            "en":"The bot <@%i> will not be allowed to send a message in the notification lounge <#%i>" % (bot.user.id, data.notify_chan),
                            "es":"El bot <@%i> no podrá enviar un mensaje en la sala de notificación <#%i>" % (bot.user.id, data.notify_chan),
                            "pt":"Não se permitirá que Bot <@%i> envie uma mensagem na ociosidade de notificação <#%i>" % (bot.user.id, data.notify_chan)
                        })
                        await chan.send(warn.getByShortLang(lang))
                    logging.info("In guild : %s (%i), channel : %s (%i) isn't allow send message" % (guild, guild.id, channel, channel.id))