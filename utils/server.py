from discord.ext import commands
import constants
from own import own_discord
from utils import Interact
import cdnwakfu

class Server(cdnwakfu.Server, Interact):

    @classmethod
    async def checkServer(cls, ctx:commands.Context ,server:cdnwakfu.ServerElement or str, lang:str):
        if isinstance(server, cdnwakfu.ServerElement):
            index = cls.indexOfServerElement(server)
        elif isinstance(server, str):
            index = cls.indexOfServer(server)
        else:
            index = None
        if index is None:
            msg= ""
            for server in cls.serversAvailable():
                if server.active:
                    msg = "%s\t %s\n" % (msg, server.name)
            await own_discord.embed_send(ctx, own_discord.embed_create(msg, name=constants.server_not_exist.getByShortLang(lang)))
            return None
        return cls.extractServer(index)

    @staticmethod
    def createMsg(server:str, lang:cdnwakfu.ServerElement):
        return server.name

    @staticmethod
    def createList(servers:list, lang:str):
        msg = ""
        compt = 1
        for server in servers:
            if server.active:
                msg = "%s%i - %s\n" % (msg, compt, server.name)
                compt += 1
        return msg

    @classmethod
    async def select(cls, ctx:commands.Context, lang:str):
        return await super().select(ctx, cls.serversAvailable(), lang)

    @classmethod
    async def display(cls, ctx:commands.Context, lang:str):
        return await super().display(ctx, cls.serversAvailable(), lang)