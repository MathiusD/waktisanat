from utils import BaseIssue
from cdnwakfu import Text

class Suggest(BaseIssue):

    label = Text({
        "en":"Suggest",
        "fr":"Suggestion",
        "es":"Sugerir",
        "pt":"Sugerir"
    })