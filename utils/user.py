from discord.ext import commands
import constants
import discord, models
import logging
from discord.errors import HTTPException
from utils import Lang
from own import own_discord
from cdnwakfu import Text

class User:

    @staticmethod
    async def remove(ctx:commands.Context, lang:str, users:models.User):
        dit = {
            "cancel":constants.cancel.getByShortLang(lang)
        }
        await own_discord.reponse_send(ctx, constants.account_suppress_request.getByShortLang(lang))
        response = await own_discord.wait_input(ctx, own_discord.str_check, dit, 120.0)
        if response and response != -1:
            if response == constants.confirm.getByShortLang(lang):
                allDb = models.AllDb(users.db)
                allDb.removeUserByDiscordID(ctx.author.id)
                await own_discord.reponse_send(ctx, constants.account_deleted.getByShortLang(lang))
            else:
                await own_discord.reponse_send(ctx, constants.bad_argument.getByShortLang(lang))
        else:
            await own_discord.reponse_send(ctx, constants.canceled.getByShortLang(lang))
    @staticmethod
    async def getUser(bot:commands.AutoShardedBot, discord_id:int):
        return await bot.fetch_user(discord_id)

    @classmethod
    async def getLastMessageOfUserDMWithUser(cls, bot:commands.AutoShardedBot, user:discord.User):
        lastMsg = None
        if user:
            lastMsgs = await user.history(limit=1).flatten()
            if len(lastMsgs) != 0:
                lastMsg = lastMsgs[0]
        return lastMsg

    @classmethod
    async def getLastMessageOfUserDM(cls, bot:commands.AutoShardedBot, discord_id:int):
        user = await cls.getUser(bot, discord_id)
        return await cls.getLastMessageOfUserDMWithUser(bot, user)

    @classmethod
    async def execCoroInUserDM(cls, bot:commands.AutoShardedBot, discord_id:int, users:models.User, coro, data:dict = {}):
        out = None
        user = await cls.getUser(bot, discord_id)
        if user:
            lang = Lang.getLangByUserOnly(user.id, users)
            try:
                lastMsg = await cls.getLastMessageOfUserDMWithUser(bot, user)
                sendMsg = False
                if lastMsg is None:
                    sendMsg = True
                    lastMsg = await user.send(constants.processing.getByShortLang(lang))
                ctx = await bot.get_context(lastMsg)
                out = await coro(ctx, lang, data)
                if sendMsg:
                    await lastMsg.delete()
            except HTTPException:
                logging.info("User <@%i> doesn't allow private message" % discord_id)
        return out
